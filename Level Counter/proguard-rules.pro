# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile


# keep everything in this package from being removed or renamed
#-printconfiguration

-keep class com.g_art.** { *; }
-dontwarn com.g_art.munchkinlevelcounter.view.**

# keep everything in this package from being renamed only
-keepnames class com.g_art.** { *; }

#Keep the annotated things annotated
-keepattributes *Annotation*
-keep class javax.inject.Provider

#Keep the dagger annotation classes themselves
-keep @interface dagger.*,javax.inject.*

#Keep the Modules intact
-keep @dagger.Module class *

-keep class dagger.** { *; }

#-Keep the fields annotated with @Inject of any class that is not deleted.
-keepclassmembers class * {
  @javax.inject.* <fields>;
}

#-Keep the names of classes that have fields annotated with @Inject and the fields themselves.
-keepclasseswithmembernames class * {
  @javax.inject.* <fields>;
}

# Keep the generated classes by dagger-compile
-keep class **$$ModuleAdapter
-keep class **$$InjectAdapter
-keep class **$$StaticInjection

-keep class com.afollestad.** { *; }

-keep class kotlin.internal.** { *; }
-keep class kotlin.jvm.** { *; }

-keep class com.bluelinelabs.** { *; }

#Reactive
-keep class io.reactivex.** { *; }
-keep class org.reactivestreams.** { *; }
-dontwarn javax.**

# For Butterknife:
-keep class butterknife.** { *; }
-dontwarn butterknife.internal.**

# Version 7
-keep class **$$ViewBinder { *; }
# Version 8
-keep class **_ViewBinding { *; }

-keepclasseswithmembernames class * { @butterknife.* <fields>; }
-keepclasseswithmembernames class * { @butterknife.* <methods>; }
###---------------End: proguard configuration for ButterKnife  ----------

# For Calligraphy:
-keep class uk.co.chrisjenx.calligraphy.* { *; }
-keep class uk.co.chrisjenx.calligraphy.*$* { *; }

## Google Analytics 3.0 specific rules ##

-keep class com.google.analytics.** { *; }

## MPAndroidChart
-keep class com.github.mikephil.charting.** { *; }

# Kotlin
-keep class kotlin.** { *; }
-dontwarn kotlin.**
-keepclassmembers class **$WhenMappings {
    <fields>;
}
-dontnote kotlin.internal.jdk8.**
-dontnote kotlin.internal.JRE8PlatformImplementations
-dontnote kotlin.internal.JRE7PlatformImplementations
-dontnote kotlin.reflect.jvm.internal.ReflectionFactoryImpl

#Room
-keep class android.arch.persistence.** { *; }


-dontwarn com.google.errorprone.annotations.*

## Google Play Services 4.3.23 specific rules ##
## https://developer.android.com/google/play-services/setup.html#Proguard ##
-keep public class com.google.android.gms.* { public *; }
-dontwarn com.google.android.gms.**

-keep class * extends java.util.ListResourceBundle {
     protected java.lang.Object[][] getContents();
 }

#-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
#    public static final *** NULL;
#}

#-keepnames @com.google.android.gms.common.annotation.KeepName class *
#-keepclassmembernames class * {
#    @com.google.android.gms.common.annotation.KeepName *;
#}

-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}
