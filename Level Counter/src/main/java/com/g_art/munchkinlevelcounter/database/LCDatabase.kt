package com.g_art.munchkinlevelcounter.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.g_art.munchkinlevelcounter.dao.GameDao
import com.g_art.munchkinlevelcounter.dao.PartyDao
import com.g_art.munchkinlevelcounter.dao.PlayerActionDao
import com.g_art.munchkinlevelcounter.dao.PlayerDao
import com.g_art.munchkinlevelcounter.entity.*
import com.g_art.munchkinlevelcounter.util.converter.LCTypeConverters

/**
 * Created by agulia on 2/26/18.
 */

@Database(
        entities = [
            Game::class,
            Party::class,
            Player::class,
            PlayerAction::class,
            PlayerInGame::class,
            PlayerInParty::class,
            PlayerStats::class,
            Monster::class
        ],
        version = 1)
@TypeConverters(LCTypeConverters::class)
abstract class LCDatabase : RoomDatabase() {

    abstract fun playerDao(): PlayerDao
    abstract fun gameDao(): GameDao
    abstract fun partyDao(): PartyDao
    abstract fun playerActionDao(): PlayerActionDao

}