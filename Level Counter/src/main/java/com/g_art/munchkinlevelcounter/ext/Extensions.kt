package com.g_art.munchkinlevelcounter.ext

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator

/**
 * Created by agulia on 3/20/18.
 */

fun RecyclerView.initRecyclerView(layoutManager: RecyclerView.LayoutManager,
                                                               adapter: RecyclerView.Adapter<out RecyclerView.ViewHolder>,
                                                               hasFixedSize: Boolean = true) {
    this.layoutManager = layoutManager
    this.adapter = adapter
    setHasFixedSize(hasFixedSize)
}

fun RecyclerView.disableChangeAnimations() {
    (itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
}

fun MutableList<Any>.getFirst() : Any {
    return this[0]
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}