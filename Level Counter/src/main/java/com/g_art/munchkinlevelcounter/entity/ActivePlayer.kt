package com.g_art.munchkinlevelcounter.entity

import androidx.room.Embedded

class ActivePlayer {

    @Embedded
    lateinit var player: Player

    @Embedded
    lateinit var playerInGame: PlayerInGame

}