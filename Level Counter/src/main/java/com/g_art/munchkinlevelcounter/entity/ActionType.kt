package com.g_art.munchkinlevelcounter.entity

/**
 * Created by agulia on 2/12/18.
 */
enum class ActionType {
    LVL("level"),
    GEAR("gear"),
    POWER("power"),
    TREASURE("treasure"),
    MONSTER("monster"),
    RUN_SUCCESS("run_success"),
    RUN_FAIL("run_fail");

    var type: String

    constructor(type: String) {
        this.type = type
    }

    companion object {
        fun getActionByType(type: String?): ActionType? {
            type?.let {
                for (value in values()) {
                    if (type == value.type) {
                        return value
                    }
                }
            }
            return null
        }
    }
}