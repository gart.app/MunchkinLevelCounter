package com.g_art.munchkinlevelcounter.entity

import androidx.room.*

@Entity(tableName = "monster_in_game",
        foreignKeys = [
            (ForeignKey(entity = Game::class, parentColumns = ["g_id"], childColumns = ["game_id"]))
        ],
        indices = [
            (Index("game_id"))
        ]
)
data class Monster (
        @ColumnInfo(name = "id")
        @PrimaryKey(autoGenerate = true)
        var id: Long = 0,
        @ColumnInfo(name = "game_id") var gameId: Long,
        @ColumnInfo(name = "level") var level: Int = 1,
        @ColumnInfo(name = "mods") var mods: Int = 0,
        @ColumnInfo(name = "treasures") var treasures: Int = 0
)