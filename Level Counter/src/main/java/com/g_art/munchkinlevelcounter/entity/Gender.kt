package com.g_art.munchkinlevelcounter.entity

/**
 * Created by agulia on 2/12/18.
 */
enum class Gender {
    MALE("male"),
    FEMALE("female");

    var gender: String


    constructor(gender: String) {
        this.gender = gender
    }

    companion object {
        fun getGenderByString(str: String?): Gender? {
            str?.let {
                for (value in values()) {
                    if (str == value.gender) {
                        return value
                    }
                }
            }
            return null
        }
    }
}