package com.g_art.munchkinlevelcounter.entity

import androidx.room.Embedded


class PartyAndPlayersAmount (

        @Embedded
        var party: Party,

        var playersAmount: Int = 0
)