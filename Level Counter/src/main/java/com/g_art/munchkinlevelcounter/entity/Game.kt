package com.g_art.munchkinlevelcounter.entity

import androidx.room.*
import java.util.*

/**
 * Created by agulia on 2/12/18.
 */
@Entity(tableName = "game",
        indices = [
            (Index("g_party_id")),
            (Index("g_player_id")),
            (Index("g_id"))
        ])
data class Game(
        @ColumnInfo(name = "g_id")
        @PrimaryKey(autoGenerate = true)
        var id: Long = 0,
        @ColumnInfo(name = "g_player_id") var playerId: Long,
        @ColumnInfo(name = "g_party_id") var partyId: Long,
        @ColumnInfo(name = "start_date") var startDate: Date,
        @ColumnInfo(name = "end_date") var endDate: Date?,
        @ColumnInfo(name = "max_level") var maxLevel: Int
)