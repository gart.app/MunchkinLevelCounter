package com.g_art.munchkinlevelcounter.entity

import androidx.room.*

/**
 * Created by agulia on 2/12/18.
 */

@Entity(tableName = "player_stats",
        foreignKeys = [
            (ForeignKey(entity = Player::class, parentColumns = ["id"], childColumns = ["player_id"], onDelete = ForeignKey.CASCADE))
        ],
        indices = [
            (Index("player_id"))
        ]
)

data class PlayerStats(
        @ColumnInfo(name = "id")
        @PrimaryKey(autoGenerate = true)
        var id: Long = 0,
        @ColumnInfo(name = "games_played") var gamesPlayed: Long,
        @ColumnInfo(name = "games_won") var gamesWon: Long,
        @ColumnInfo(name = "treasures") var treasures: Long,
        @ColumnInfo(name = "monsters") var monsters: Long,
        @ColumnInfo(name = "died") var died: Long,
        @ColumnInfo(name = "max_level") var maxLevel: Long,
        @ColumnInfo(name = "max_power") var maxPower: Long,
        @ColumnInfo(name = "player_id") var playerId: Long
)