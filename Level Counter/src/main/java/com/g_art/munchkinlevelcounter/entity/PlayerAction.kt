package com.g_art.munchkinlevelcounter.entity

import androidx.room.*
import androidx.room.ForeignKey.CASCADE
import java.util.*

/**
 * Created by agulia on 2/12/18.
 */
@Entity(tableName = "player_action",
        foreignKeys = [
            (ForeignKey(entity = Player::class, parentColumns = arrayOf("id"), childColumns = arrayOf("player_id"), onDelete = CASCADE)),
            (ForeignKey(entity = Game::class, parentColumns = arrayOf("g_id"), childColumns = arrayOf("game_id"), onDelete = CASCADE))
        ],
        indices = [
            (Index("game_id")),
            (Index("player_id"))
        ])
data class PlayerAction(
        @ColumnInfo(name = "id")
        @PrimaryKey(autoGenerate = true)
        var id: Long = 0,
        @ColumnInfo(name = "game_id") var gameId: Long,
        @ColumnInfo(name = "player_id") var playerId: Long,
        @ColumnInfo(name = "amount") var amount: Int,
        @ColumnInfo(name = "type") var valueType: ActionType,
        @ColumnInfo(name = "action_date") var actionDate: Date
) {
}