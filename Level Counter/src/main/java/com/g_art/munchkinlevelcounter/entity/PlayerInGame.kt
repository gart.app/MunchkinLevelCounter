package com.g_art.munchkinlevelcounter.entity

import androidx.room.*

/**
 * Created by agulia on 2/12/18.
 */

@Entity(tableName = "player_in_game",
        foreignKeys = [
            (ForeignKey(entity = Player::class, parentColumns = ["id"], childColumns = ["player_id"], onDelete = ForeignKey.CASCADE)),
            (ForeignKey(entity = Game::class, parentColumns = ["g_id"], childColumns = ["game_id"], onDelete = ForeignKey.CASCADE))
        ],
        indices = [
            (Index("game_id")),
            (Index("player_id")),
            (Index(value = ["game_id", "player_id"], unique = true))
        ]
)
data class PlayerInGame(
        @ColumnInfo(name = "pg_id")
        @PrimaryKey(autoGenerate = true)
        var id: Long = 0,
        @ColumnInfo(name = "game_id") var gameId: Long,
        @ColumnInfo(name = "player_id") var playerId: Long,
        @ColumnInfo(name = "level") var level: Int = 1,
        @ColumnInfo(name = "gear") var gear: Int = 0,
        @ColumnInfo(name = "mods") var mods: Int = 0,
        @ColumnInfo(name = "warrior") var warrior: Boolean = false,
        @ColumnInfo(name = "winner") var winner: Boolean = false,
        @ColumnInfo(name = "treasures") var treasures: Int = 0
)