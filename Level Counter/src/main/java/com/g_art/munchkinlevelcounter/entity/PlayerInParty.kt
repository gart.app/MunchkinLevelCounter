package com.g_art.munchkinlevelcounter.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index

/**
 * Created by agulia on 2/12/18.
 */

@Entity(tableName = "player_in_party",
        primaryKeys = ["player_id", "party_id"],
        foreignKeys = [
            (ForeignKey(entity = Player::class, parentColumns = ["id"], childColumns = ["player_id"], deferred = true)),
            (ForeignKey(entity = Party::class, parentColumns = ["id"], childColumns = ["party_id"], deferred = true))
        ],
        indices = [
            (Index("party_id")),
            (Index("player_id"))
        ])
data class PlayerInParty(
        @ColumnInfo(name = "player_id") var playerId: Long,
        @ColumnInfo(name = "party_id") var partyId: Long
) {
}