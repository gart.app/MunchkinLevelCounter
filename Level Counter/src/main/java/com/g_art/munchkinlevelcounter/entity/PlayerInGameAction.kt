package com.g_art.munchkinlevelcounter.entity

import androidx.room.Embedded
import androidx.room.Ignore
import androidx.room.Relation

class PlayerInGameAction(
        @Embedded
        var player: Player? = null,

        @Embedded
        var playerInGame: PlayerInGame? = null,

        @Ignore
        var playerActions: List<PlayerAction> = emptyList()
)