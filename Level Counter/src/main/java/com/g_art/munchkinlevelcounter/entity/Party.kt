package com.g_art.munchkinlevelcounter.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by agulia on 2/12/18.
 */

@Entity(tableName = "party")
data class Party(

        @ColumnInfo(name = "id")
        @PrimaryKey(autoGenerate = true)
        var id: Long = 0,
        @ColumnInfo(name = "name") var partyName: String = ""
)