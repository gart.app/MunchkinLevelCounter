package com.g_art.munchkinlevelcounter.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by agulia on 2/12/18.
 */
@Entity(tableName = "player")
data class Player(
        @ColumnInfo(name = "id")
        @PrimaryKey(autoGenerate = true)
        var id: Long = 0,
        @ColumnInfo(name = "name") var playerName: String,
        @ColumnInfo(name = "gender") var gender: Gender,
        @ColumnInfo(name = "color") var color: Int,
        @ColumnInfo(name = "owner") var owner: Boolean,
        @ColumnInfo(name = "position") var position: Int

)