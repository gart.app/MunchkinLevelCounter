package com.g_art.munchkinlevelcounter.dao

import androidx.room.*
import com.g_art.munchkinlevelcounter.entity.Party
import com.g_art.munchkinlevelcounter.entity.PartyAndPlayersAmount
import com.g_art.munchkinlevelcounter.entity.PlayerInParty
import io.reactivex.Maybe
import io.reactivex.Single

/**
 * Created by agulia on 2/15/18.
 */

@Dao
interface PartyDao : BaseDao<Party> {


    @Query("select * from party")
    fun getAllParties(): Single<List<Party>>

    @SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
    @Query("SELECT party.id, party.name, count(player_in_party.player_id) AS playersAmount FROM party JOIN player_in_party ON party.id = player_in_party.party_id GROUP BY party.id")
    fun getPartiesAndPlayersCount(): Single<List<PartyAndPlayersAmount>>

    @SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
    @Query("SELECT party.id, party.name, count(player_in_party.player_id) AS playersAmount FROM party JOIN player_in_party ON party.id = player_in_party.party_id GROUP BY party.id LIMIT :limit")
    fun getPartiesAndPlayersCount(limit: Int): Single<List<PartyAndPlayersAmount>>

    @Query("select * from party where id = :id")
    fun getPartyById(id: Long): Maybe<Party>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertParty(party: Party): Long

    @Update(onConflict = OnConflictStrategy.IGNORE)
    fun updateParty(party: Party)

    @Query("DELETE FROM party where party.id = :id")
    fun deletePartyById(id: Long)

    @Query("DELETE FROM player_in_party where player_id = :playerId and party_id = :partyId")
    fun removePlayerFromParty(partyId: Long, playerId: Long)

    @Query("DELETE FROM player_in_party where party_id = :partyId")
    fun removePlayersAndParty(partyId: Long)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addPlayersToParty(playersToParty: List<PlayerInParty>)

    @Delete
    fun removePlayersFromParty(playersToRemove: List<PlayerInParty>)

    @Transaction
    fun editPartyWithPlayers(playersToAdd: List<PlayerInParty>, playersToRemove: List<PlayerInParty>) {
        removePlayersFromParty(playersToRemove)
        if (playersToAdd.size == 1) {
            removePartyById(playersToAdd[0].partyId)
        } else {
            addPlayersToParty(playersToAdd)
        }
    }

    @Transaction
    fun removeParty(party: Party) {
        removePartyById(party.id)
    }

    @Transaction
    fun removePartyById(id: Long) {
        removePlayersAndParty(id)
        deletePartyById(id)
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(parties: List<Party>)
}