package com.g_art.munchkinlevelcounter.dao

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.g_art.munchkinlevelcounter.entity.Player
import com.g_art.munchkinlevelcounter.entity.PlayerInParty
import com.g_art.munchkinlevelcounter.model.SelectionPlayerModel
import io.reactivex.Maybe
import io.reactivex.Single

/**
 * Created by agulia on 2/15/18.
 */
@Dao
interface PlayerDao : BaseDao<Player> {

    @Query("select * from player")
    fun getAllPlayers(): Single<List<Player>>

    @Query("select * from player")
    fun getAllPlayersMaybe(): Maybe<List<Player>>

    @Query("select * from player where id = :id")
    fun getPlayerById(id: Long): Maybe<Player>

    @SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
    @Query("SELECT * FROM player INNER JOIN player_in_party ON player.id = player_in_party.player_id WHERE player_in_party.party_id = :id")
    fun getPlayersByPartyId(id: Long): Maybe<List<Player>>

    @SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
    @Query("SELECT player_in_game.*, player.* FROM player_in_game INNER JOIN player ON player.id = player_in_game.player_id WHERE player_in_game.game_id = :id")
    fun getPlayersByGameId(id: Long): Maybe<List<Player>>

    @Insert(onConflict = REPLACE)
    fun addPlayersToParty(partyAndPlayers: List<PlayerInParty>)

    @Insert(onConflict = REPLACE)
    fun insertPlayerAndReturn(player: Player): Long

    @Query("DELETE FROM player where player.id = :id")
    fun deletePlayerById(id: Long)
    @Query("DELETE FROM player_in_party where player_in_party.player_id = :id")
    fun deletePlayerInPartiesById(id: Long)

    @Transaction
    fun deletePlayer(id: Long) {
        deletePlayerById(id)
        deletePlayerInPartiesById(id)
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(players: List<Player>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateAll(players: List<Player>)

    @Query("UPDATE player SET position = :position WHERE id = :playerId")
    fun updatePlayerPositionById(playerId: Long, position: Int)

    @Transaction
    fun updatePlayersPositions(players: List<SelectionPlayerModel>) {
        players.forEach {
            updatePlayerPositionById(it.id, it.position)
        }
    }
}