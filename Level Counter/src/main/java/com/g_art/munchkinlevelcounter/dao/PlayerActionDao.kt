package com.g_art.munchkinlevelcounter.dao

import androidx.room.*
import com.g_art.munchkinlevelcounter.entity.PlayerAction
import com.g_art.munchkinlevelcounter.entity.PlayerInGameAction
import io.reactivex.Maybe
import io.reactivex.Single

/**
 * Created by agulia on 2/19/18.
 */
@Dao
interface PlayerActionDao: BaseDao<PlayerAction> {

    @Query("select * from player_action where id = :id")
    fun getPlayerActionById(id: Long): Maybe<PlayerAction>

    @SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
    @Transaction
    @Query("select * from player p join player_in_game pig on p.id = pig.player_id where pig.game_id = :gameId")
    fun getPlayerInGame(gameId: Long): Single<List<PlayerInGameAction>>

    @SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
    @Transaction
    @Query("select * from player p join player_in_game pig on p.id = pig.player_id where pig.game_id = :gameId and p.id = :playerId")
    fun getGameActionsForPlayer(gameId: Long, playerId: Long): Single<List<PlayerInGameAction>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(actions: List<PlayerAction>)

    @Query("select * from player_action pa where pa.game_id = :gameId")
    fun getGameActions(gameId: Long): Single<List<PlayerAction>>
}