package com.g_art.munchkinlevelcounter.dao

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.g_art.munchkinlevelcounter.entity.PlayerStats

/**
 * Created by agulia on 2/15/18.
 */

@Dao
interface PlayerStatsDao {

    @Query("select * from player_stats")
    fun getAllPlayerStats(): List<PlayerStats>

    @Query("select * from player_stats where id = :id")
    fun getPlayerStatsById(id: Long): PlayerStats

    @Query("select * from player_stats where player_id = :id")
    fun getPlayerStatsByPlayerId(id: Long): PlayerStats

    @Insert(onConflict = REPLACE)
    fun insertPlayerStats(playerStats: PlayerStats)

    @Update(onConflict = REPLACE)
    fun updatePlayerStats(playerStats: PlayerStats)

    @Delete
    fun deletePlayerStats(playerStats: PlayerStats)
}