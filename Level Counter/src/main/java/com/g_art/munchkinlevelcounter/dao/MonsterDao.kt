package com.g_art.munchkinlevelcounter.dao

import androidx.room.Dao
import androidx.room.Query
import com.g_art.munchkinlevelcounter.entity.Monster
import io.reactivex.Maybe

@Dao
interface MonsterDao: BaseDao<Monster> {

    @Query("SELECT * FROM monster_in_game WHERE id = :id")
    fun getMonsterById(id: Long): Maybe<Monster>
}