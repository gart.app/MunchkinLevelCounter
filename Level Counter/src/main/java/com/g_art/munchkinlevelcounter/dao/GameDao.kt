package com.g_art.munchkinlevelcounter.dao

import androidx.room.*
import com.g_art.munchkinlevelcounter.entity.*
import io.reactivex.Maybe
import io.reactivex.Single
import java.util.*

/**
 * Created by agulia on 2/15/18.
 */
@Dao
interface GameDao: BaseDao<Game> {

    @Query("DELETE FROM game where game.g_id = :id")
    fun deleteGameById(id: Long)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertWithResponse(game: Game): Long

    @Query("SELECT DISTINCT g.g_id, g.start_date, g.end_date, g.g_player_id, g.g_party_id, g.max_level FROM game g INNER JOIN player_in_game ON g.g_id = player_in_game.game_id")
    fun getPlayedGames(): Single<List<Game>>

    @Query("select DISTINCT g.g_id, g.start_date, g.end_date, g.g_player_id, g.g_party_id, g.max_level FROM game g INNER JOIN player_in_game ON g.g_id = player_in_game.game_id ORDER BY start_date DESC limit :limit")
    fun getPlayedGames(limit: Int): Single<List<Game>>

    @Query("select * from game where g_id = :id")
    fun getGameById(id: Long): Maybe<Game>

    @Query("select * from game g order by g.start_date DESC")
    fun getLastGame(): Maybe<Game>

    @Query("select * from game where g_id = :id")
    fun getGame(id: Long): Game

    @Query("select * from game where g_party_id = :id")
    fun getGamesByPartyId(id: Long): Single<List<Game>>

    @SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
    @Query("SELECT * FROM game INNER JOIN player_in_game ON game.g_id = player_in_game.game_id WHERE player_in_game.player_id = :id")
    fun getGamesForPlayer(id: Long): Single<List<Game>>

    @SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
    @Query("SELECT * FROM player_in_game WHERE player_in_game.game_id = :gameId")
    fun getPlayersForGame(gameId: Long): Single<List<PlayerInGame>>

    @SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
    @Query("SELECT * FROM player_in_game WHERE player_in_game.game_id = :gameId AND player_in_game.player_id = :playerId")
    fun getPlayerForGame(gameId: Long, playerId: Long): Maybe<PlayerInGame>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(games: List<Game>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPlayers(players: List<PlayerInGame>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updatePlayerInGame(playerInGame: PlayerInGame)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updateMonsterInGame(monster: Monster)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun createAction(action: PlayerAction)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun createAction(action: List<PlayerAction>)

    @Query("DELETE FROM player_action WHERE player_action.game_id = :gameId AND player_action.player_id = :playerId")
    fun removePlayerActions(gameId: Long, playerId: Long)

    @Query("DELETE FROM player_action WHERE player_action.game_id = :gameId")
    fun removeGameActions(gameId: Long)

    @Query("SELECT player_in_game.*, player.* FROM player_in_game INNER JOIN player ON player.id = player_in_game.player_id WHERE player_in_game.game_id = :gameId AND player.id = :playerId")
    fun getActivePlayer(gameId: Long, playerId: Long): Maybe<ActivePlayer>

    @Query("SELECT player_in_game.*, player.* FROM player_in_game INNER JOIN player ON player.id = player_in_game.player_id WHERE player_in_game.game_id = :gameId")
    fun getActivePlayer(gameId: Long): Maybe<ActivePlayer>

    @Query("SELECT player_in_game.*, player.* FROM player_in_game INNER JOIN player ON player.id = player_in_game.player_id WHERE player_in_game.game_id = :gameId")
    fun getActivePlayers(gameId: Long): Maybe<List<ActivePlayer>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addPlayersToTheGame(playersToParty: List<PlayerInGame>)

    @Delete
    fun removePlayersFromTheGame(playersToRemove: List<PlayerInGame>)

    @Query("DELETE FROM player_in_game WHERE player_in_game.game_id = :gameId AND player_in_game.player_id = :playerId")
    fun removePlayerFromTheGame(gameId: Long, playerId: Long)

    @Query("DELETE FROM player_in_game WHERE player_in_game.game_id = :gameId")
    fun removePlayersInGame(gameId: Long)

    @Query("DELETE FROM monster_in_game WHERE monster_in_game.game_id = :gameId")
    fun removeMonstersInGame(gameId: Long)

    @Query("UPDATE game SET g_party_id = :partyId WHERE g_id = :gameId")
    fun updateGameWithParty(gameId: Long, partyId: Long?)

    @Transaction
    fun editPlayers(playersToAdd: List<PlayerInGame>, playersToRemove: List<PlayerInGame>) {
        playersToRemove.forEach { removePlayerActions(it.gameId, it.playerId) }
        playersToRemove.forEach { removePlayerFromTheGame(it.gameId, it.playerId) }
        addPlayersToTheGame(playersToAdd)
    }
    @Transaction
    fun createSingleGame(game: Game): Long {
        val gameId = insertWithResponse(game)
        val playerInGame = PlayerInGame(gameId = gameId, playerId = game.playerId)
        updatePlayerInGame(playerInGame)
        return gameId
    }

    @Transaction
    fun createActionAndUpdatePlayerInGame(playerInGame: PlayerInGame, actions: List<PlayerAction>) {
        updatePlayerInGame(playerInGame)
        createAction(actions)
    }

    @Transaction
    fun createActionAndUpdatePlayerInGame(playerInGame: PlayerInGame, action: PlayerAction) {
        updatePlayerInGame(playerInGame)
        createAction(action)
    }

    @Transaction
    fun removeGameAndActionsById(gameId: Long) {
        removeGameActions(gameId)
        removePlayersInGame(gameId)
        removeMonstersInGame(gameId)
        deleteGameById(gameId)
    }

    @Query("UPDATE game SET end_date = :finishDate WHERE game.g_id = :gameId")
    fun finishTheGame(gameId: Long, finishDate: Date)
}