package com.g_art.munchkinlevelcounter.dao

import androidx.room.*

@Dao
interface BaseDao<T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg obj: T)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(vararg obj: T)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrUpdate(vararg obj: T)

    @Delete
    fun delete(vararg obj: T)
}