package com.g_art.munchkinlevelcounter.model

import java.util.*

/**
 * Created by agulia on 3/20/18.
 */
data class GameModel(
        var id: Long = 0,
        var playerId: Long = 0,
        var partyId: Long = 0,
        var startDate: Date,
        var endDate: Date? = null,
        var maxLevel: Int = 10)