package com.g_art.munchkinlevelcounter.model

data class PartyAndPlayersModel(
        var partyModel: PartyModel,
        var players: MutableList<PlayerModel>
)