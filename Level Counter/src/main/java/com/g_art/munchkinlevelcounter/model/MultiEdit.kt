package com.g_art.munchkinlevelcounter.model

data class MultiEdit(val value: Int = 0,
                private val display: String = "0") {

    override fun toString(): String {
        return display
    }
}

