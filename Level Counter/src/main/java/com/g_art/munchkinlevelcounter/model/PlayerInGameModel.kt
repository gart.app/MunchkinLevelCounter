package com.g_art.munchkinlevelcounter.model

/**
 * Created by agulia on 3/20/18.
 */
class PlayerInGameModel(
        var id: Long = 0,
        var gameId: Long,
        var playerId: Long = 0,
        var level: Int = 1,
        var gear: Int = 0,
        var mods: Int = 0,
        var warrior: Boolean = false,
        var winner: Boolean = false,
        var treasures: Int = 0) {

    fun getTotalPower() = level + gear + mods
}