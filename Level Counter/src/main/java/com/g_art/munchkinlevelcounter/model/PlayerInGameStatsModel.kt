package com.g_art.munchkinlevelcounter.model

import com.g_art.munchkinlevelcounter.entity.ActionType

class PlayerInGameActionsModel (
        var player: PlayerModel,
        var playerInGame: PlayerInGameModel,
        var playerActions: Map<ActionType, List<PlayerActionModel>>
)