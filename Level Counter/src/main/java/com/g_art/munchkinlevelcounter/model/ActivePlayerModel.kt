package com.g_art.munchkinlevelcounter.model

data class ActivePlayerModel(var player: SelectionPlayerModel,
                             var playerInGame: PlayerInGameModel) {

    fun getCompanionInfo(template: String): String {
        return String.format(template, playerInGame.getTotalPower(), player.name, playerInGame.level)
    }
}