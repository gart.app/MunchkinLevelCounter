package com.g_art.munchkinlevelcounter.model


/**
 * Created by agulia on 3/20/18.
 */
data class PlayerInPartyModel(
        var playerId: Long,
        var partyId: Long
)