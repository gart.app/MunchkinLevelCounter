package com.g_art.munchkinlevelcounter.model

import android.graphics.Color
import com.g_art.munchkinlevelcounter.entity.Gender
import java.util.*

/**
 * Created by agulia on 3/20/18.
 */
open class PlayerModel(
        val id: Long = 0,
        var name: String = "",
        var gender: Gender = Gender.MALE,
        var color: Int = 0,
        var owner: Boolean = false,
        var position: Int = 0) {


    fun isValidForAdd() = name.trim().length > 1

    fun generateColor(): Int {
        val rnd = Random()
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))
    }

    fun generateAndSignColor(): Int {
        val rnd = Random()
        this.color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))
        return color
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PlayerModel

        if (id != other.id) return false
        if (name != other.name) return false
        if (gender != other.gender) return false
        if (color != other.color) return false
        if (owner != other.owner) return false
        if (position != other.position) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + gender.hashCode()
        result = 31 * result + color
        result = 31 * result + owner.hashCode()
        result = 31 * result + position
        return result
    }

}