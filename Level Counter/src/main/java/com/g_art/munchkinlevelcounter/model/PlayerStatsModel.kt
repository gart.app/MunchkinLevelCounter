package com.g_art.munchkinlevelcounter.model

/**
 * Created by agulia on 3/20/18.
 */
data class PlayerStatsModel(
        var id: Long = 0,
        var gamesPlayed: Long,
        var gamesWon: Long,
        var treasures: Long,
        var monsters: Long,
        var died: Long,
        var maxLevel: Long,
        var maxPower: Long,
        var playerId: Long)