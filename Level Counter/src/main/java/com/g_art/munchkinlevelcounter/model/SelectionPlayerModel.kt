package com.g_art.munchkinlevelcounter.model

import com.g_art.munchkinlevelcounter.entity.Gender

class SelectionPlayerModel(
        id: Long = 0,
        name: String = "",
        gender: Gender = Gender.MALE,
        color: Int = 0,
        owner: Boolean = false,
        position: Int = 0,
        var selected: Boolean = false
): PlayerModel(id, name, gender, color, owner, position) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        if (!super.equals(other)) return false

        other as SelectionPlayerModel

        if (selected != other.selected) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + selected.hashCode()
        return result
    }
}