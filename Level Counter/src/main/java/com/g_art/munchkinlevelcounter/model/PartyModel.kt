package com.g_art.munchkinlevelcounter.model

/**
 * Created by agulia on 3/20/18.
 */
data class PartyModel(val id: Long = 0,
                      var name: String = "Party name",
                      var playersAmount:Int = 0) {

    fun isValidForAdd() = name.isNotEmpty() && name.isNotBlank()
}