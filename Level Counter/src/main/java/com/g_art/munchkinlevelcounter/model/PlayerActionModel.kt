package com.g_art.munchkinlevelcounter.model

import com.g_art.munchkinlevelcounter.entity.ActionType
import java.util.*

/**
 * Created by agulia on 3/20/18.
 */
data class PlayerActionModel(
        var id: Long = 0,
        var gameId: Long,
        var playerId: Long,
        var amount: Int,
        var valueType: ActionType,
        var actionDate: Date)