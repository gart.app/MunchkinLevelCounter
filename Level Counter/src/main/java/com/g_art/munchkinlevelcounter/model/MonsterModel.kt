package com.g_art.munchkinlevelcounter.model

data class MonsterModel(
        var id: Long = 0,
        var gameId: Long,
        var level: Int = 1,
        var mods: Int = 0,
        var treasures: Int = 0
) {
    fun getPower(): Int = level + mods
}