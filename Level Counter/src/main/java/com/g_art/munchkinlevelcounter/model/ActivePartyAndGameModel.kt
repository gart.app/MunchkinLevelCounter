package com.g_art.munchkinlevelcounter.model

class ActivePartyAndGameModel {
    lateinit var game: GameModel

    lateinit var activeParty: ActivePartyModel

    fun getSelectedPosition(): Int {
        for (i in 0 until activeParty.activePlayers.size) {
            if (activeParty.activePlayers[i].player.selected) {
                return i
            }
        }
        return 0
    }

    fun getPlayerOnPosition(position:Int) : ActivePlayerModel? {
        return if (activeParty.activePlayers.size > position && position >= 0) {
            activeParty.activePlayers[position]
        } else {
            null
        }
    }

    fun selectPlayer(position: Int) {
        activeParty.activePlayers[position].player.selected = true
    }

    fun resetSelection() {
        activeParty.activePlayers.forEach { it.player.selected = false }
    }

    fun nextPlayerPosition(currentPosition: Int): Int {
        val playersAmount = activeParty.activePlayers.size
        return if (currentPosition == playersAmount -1) {
            0
        } else {
            1 + currentPosition
        }
    }
    fun prevPlayerPosition(currentPosition: Int): Int {
        val playersAmount = activeParty.activePlayers.size
        return if (currentPosition == 0) {
            playersAmount - 1
        } else {
            currentPosition - 1
        }
    }
}