package com.g_art.munchkinlevelcounter.view.game.details

import com.g_art.munchkinlevelcounter.model.PlayersAndGameModel
import com.g_art.munchkinlevelcounter.mvp.BasePresenter
import com.g_art.munchkinlevelcounter.usecases.GameUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class GameDetailsPresenter
@Inject constructor(private val useCase: GameUseCase):
        BasePresenter<GameDetailsContract.View> (),
        GameDetailsContract.Presenter {

    private lateinit var gameDetailsModel: PlayersAndGameModel

    override fun loadGame(gameId: Long) {
        disposables.add(
                useCase.loadGameAndPlayers(gameId)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                {
                                    gameDetailsModel = it
                                    view?.onLoadGameSuccess(it.gameModel, it.players)
                                },
                                {
                                    view?.onLoadGameError(it)
                                }
                        )
        )
    }

    override fun replayTheGame() {
        disposables.add(
                useCase.replayTheGame(gameDetailsModel)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                {
                                    view?.openTheGame(it, gameDetailsModel.players.size == 1)
                                },
                                {
                                    view?.onStartGameError(it)
                                }
                        )
        )
    }

    override fun continueTheGame() {
        disposables.add(
                useCase.unmarkTheWinner(gameDetailsModel.players)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                {
                                    view?.openTheGame(gameDetailsModel.gameModel.id, gameDetailsModel.players.size == 1)
                                },
                                {
                                    view?.onStartGameError(it)
                                }
                        )
        )
    }

    override fun openStats() {
        view?.openStats(gameId = gameDetailsModel.gameModel.id)
    }
}