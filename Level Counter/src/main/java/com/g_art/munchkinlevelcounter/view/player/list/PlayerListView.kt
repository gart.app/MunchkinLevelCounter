package com.g_art.munchkinlevelcounter.view.player.list

import android.os.Bundle
import android.view.View
import androidx.appcompat.view.ActionMode
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.OnClick
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler
import com.bluelinelabs.conductor.changehandler.HorizontalChangeHandler
import com.g_art.munchkinlevelcounter.LCApplication
import com.g_art.munchkinlevelcounter.R
import com.g_art.munchkinlevelcounter.ext.initRecyclerView
import com.g_art.munchkinlevelcounter.model.SelectionPlayerModel
import com.g_art.munchkinlevelcounter.mvp.BaseView
import com.g_art.munchkinlevelcounter.mvp.MVPPresenter
import com.g_art.munchkinlevelcounter.util.*
import com.g_art.munchkinlevelcounter.view.game.active.master.MasterGameView
import com.g_art.munchkinlevelcounter.view.game.active.single.SingleGameView
import com.g_art.munchkinlevelcounter.view.player.details.PlayerDetailsView
import com.g_art.munchkinlevelcounter.view.player.list.adapter.PlayerListAdapter
import com.google.firebase.analytics.FirebaseAnalytics
import javax.inject.Inject


/**
 * Created by agulia on 3/12/18.
 */
class PlayerListView : BaseView(), PlayerListContract.View {

    private val mActionModeCallback = PlayersActionMode(this)

    private var mActionMode: ActionMode? = null

    @Inject
    lateinit var presenter: PlayerListPresenter

    @BindView(R.id.rv_players_list)
    lateinit var recyclerView: RecyclerView

    private val clickListener: (Long, Int) -> Unit = this::playerRowClicked

    private val playerAdapter = PlayerListAdapter(clickListener)

    var action: Long = DEFAULT

    override fun onAttach(view: View) {
        super.onAttach(view)

        val bundle = Bundle()
        recyclerView.initRecyclerView(LinearLayoutManager(view.context), playerAdapter)
        with(presenter) {
            start(this@PlayerListView)
            action = args.getLong(ACTION_ID)
            when (action) {
                DEFAULT        ->  {
                    bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, DEFAULT_USE)
                    loadAllPlayers()
                }
                FROM_THE_PARTY -> {
                    bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, FROM_THE_PARTY_USE)
                    loadPlayersForParty(args.getLong(PARTY_ID))
                }
                FROM_THE_GAME  -> {
                    bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, FROM_THE_GAME_USE)
                    loadPlayersFromTheGame(args.getLong(PARTY_ID), args.getLong(GAME_ID))
                }
            }
        }
        logEvent(VIEW_OPEN, bundle)
    }

    private fun playerRowClicked(playerId: Long, clickType: Int) {
        //Depending on click type invoke proper method (ugly solution)
        when(clickType) {
            1 -> onPlayerSelectClick(playerId)
            2 -> onPlayerEditClick(playerId)
            3 -> onPlayerRemoveClick(playerId)
        }
    }

    @OnClick(R.id.add_player_fab)
    fun onAddPlayerClick() {
        resetAction()
        router.pushController(RouterTransaction.with(PlayerDetailsView())
                .pushChangeHandler(FadeChangeHandler())
                .popChangeHandler(FadeChangeHandler()))
    }

    override fun onLoadPlayersSuccess(players: List<SelectionPlayerModel>) {
        playerAdapter.fullPlayersUpdate(players)
    }

    override fun onLoadPlayersError(throwable: Throwable) {
        showMessage(R.string.error_players_load)
    }

    override fun onLoadPartyPlayersSuccess(players: List<SelectionPlayerModel>) {
        playerAdapter.fullPlayersUpdate(players)
    }

    override fun onLoadPartyPlayersError(throwable: Throwable) {
        showMessage(R.string.error_party_load)
    }

    override fun onSavePlayersToPartySuccess() {
        router.popCurrentController()
    }

    override fun onSavePlayersToTheGameSuccess(gameId: Long, isMasterGame: Boolean, wasMasterGame: Boolean) {
        if (isMasterGame) {
            if (wasMasterGame) {
                router.popCurrentController()
            } else {
                router.setBackstack( router.backstack.filter { it.tag() != gameId.toString() }, null)
                openMasterGame(gameId)
            }
        } else {
            if (!wasMasterGame) {
                router.popCurrentController()
            } else {
                router.setBackstack( router.backstack.filter { it.tag() != gameId.toString() }, null)
                openSingleGame(gameId)
            }
        }
    }

    override fun onSavePartyAndPlayersToTheGameSuccess(partyId: Long, gameId: Long) {
        router.popCurrentController()
    }

    override fun onSavePlayersToPartyError(throwable: Throwable) {
        showMessage(R.string.error_players_saving_to_party)
    }

    override fun onStartGameSuccess(gameId: Long) {
        startGame(gameId)
    }

    override fun onStartGameError(throwable: Throwable) {
    }

    override fun onPlayerSelectSuccess(position: Int) {
        playerAdapter.tapOnPlayerByPosition(position)
        updateActionMode()
    }

    private fun updateActionMode() {
        val selected = playerAdapter.selectedPlayers()
        if (selected > 0 ) {
            if (mActionMode == null) {
                mActionMode = getStartActivity().startSupportActionMode(mActionModeCallback)
            }
            mActionMode?.title = resources?.getString(R.string.title_selected_players, playerAdapter.selectedPlayers())
        } else {
            mActionMode?.finish()
            resetAction()
        }
    }

    override fun onPlayerSelectError(throwable: Throwable) {
        showMessage(R.string.error_player_select)
    }

    override fun onDeletePlayerSuccess(position: Int) {
        playerAdapter.removePlayer(position)
        updateActionMode()
    }

    override fun onDeletePlayerError(throwable: Throwable) {
        showMessage(R.string.error_player_delete)
    }

    private fun onPlayerEditClick(playerId: Long) {
        val playerDetailsView = PlayerDetailsView().apply {
            args.putLong(PLAYER_ID, playerId)
        }
        router.pushController(RouterTransaction.with(playerDetailsView)
                .pushChangeHandler(FadeChangeHandler())
                .popChangeHandler(FadeChangeHandler()))
    }

    private fun onPlayerRemoveClick(playerId: Long) {
        presenter.removePlayerById(playerId)
    }

    private fun openSingleGame(gameId: Long) {
        val singleGameView = SingleGameView().apply {
            args.putLong(GAME_ID, gameId)
            args.putString(TAG_KEY, gameId.toString())
        }
        router.replaceTopController(RouterTransaction.with(singleGameView)
                .pushChangeHandler(HorizontalChangeHandler())
                .popChangeHandler(HorizontalChangeHandler())
                .tag(gameId.toString()))
    }

    private fun openMasterGame(gameId: Long) {
        val masterGameView = MasterGameView().apply {
            args.putLong(GAME_ID, gameId)
            args.putString(TAG_KEY, gameId.toString())
        }
        router.replaceTopController(RouterTransaction.with(masterGameView)
                .pushChangeHandler(HorizontalChangeHandler())
                .popChangeHandler(HorizontalChangeHandler())
                .tag(gameId.toString()))
    }


    /**
     * Depends on original request
     * Single player game mode -> Game screen
     * Party players selection -> Add player to party
     */
    private fun onPlayerSelectClick(playerId: Long) {
        when (action) {
            FROM_THE_PARTY -> {
                presenter.editPlayersInParty(playerId = playerId)
            }
            FROM_THE_GAME -> {
                presenter.editPlayersInParty(playerId = playerId)
            }
            else -> presenter.startTheGame(playerId)
        }
    }

    private fun startGame(gameId: Long) {
        val singleGame = SingleGameView().apply {
            args.putLong(GAME_ID, gameId)
            args.putString(TAG_KEY, gameId.toString())
        }
        router.pushController(RouterTransaction.with(singleGame)
                .pushChangeHandler(FadeChangeHandler())
                .popChangeHandler(FadeChangeHandler())
                .tag(gameId.toString()))
    }

    override fun handleBack(): Boolean {
        resetAction()
        return super.handleBack()
    }

    override fun resetAction() {
        playerAdapter.resetSelection()
        mActionMode?.finish()
        mActionMode = null
    }

    override fun onOkActionClick(): Boolean {
        mActionMode?.finish()
        when (action) {
            FROM_THE_PARTY -> {
                presenter.savePlayersToParty(args.getLong(PARTY_ID))
            }
            FROM_THE_GAME -> {
                presenter.savePlayersToPartyAndGame(args.getLong(PARTY_ID), args.getLong(GAME_ID))
            }
        }
        return true
    }

    override fun getLayoutId() = R.layout.players_list

    override fun getToolbarTitleId() = R.string.title_activity_players

    override fun injectDependencies() {
        DaggerPlayerListComponent.builder()
                .lCAppComponent(LCApplication.component)
                .playerListModule(PlayerListModule())
                .build()
                .inject(this)
    }

    override fun getPresenter(): MVPPresenter = presenter

}