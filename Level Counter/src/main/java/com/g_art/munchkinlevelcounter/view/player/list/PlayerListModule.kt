package com.g_art.munchkinlevelcounter.view.player.list

import com.g_art.munchkinlevelcounter.di.PerScreen
import com.g_art.munchkinlevelcounter.repository.GameRepository
import com.g_art.munchkinlevelcounter.repository.PartyRepository
import com.g_art.munchkinlevelcounter.repository.PlayerRepository
import com.g_art.munchkinlevelcounter.usecases.PlayersUseCase
import dagger.Module
import dagger.Provides

/**
 * Created by G_Artem on 3/24/18.
 */
@Module
class PlayerListModule {

    @PerScreen
    @Provides
    fun providePlayerUseCase(playerRepository: PlayerRepository,
                             partyRepository: PartyRepository,
                             gameRepository: GameRepository) =
            PlayersUseCase(playerRepository, partyRepository, gameRepository)

    @PerScreen
    @Provides
    fun providePresenter(playersUseCase: PlayersUseCase) = PlayerListPresenter(playersUseCase)

}