package com.g_art.munchkinlevelcounter.view.stats

import com.g_art.munchkinlevelcounter.di.PerScreen
import com.g_art.munchkinlevelcounter.repository.ActionsRepository
import com.g_art.munchkinlevelcounter.usecases.ActionsUseCase
import dagger.Module
import dagger.Provides

@Module
class StatsModule {

    @PerScreen
    @Provides
    fun provideActionsUseCase(actionsRepository: ActionsRepository) = ActionsUseCase(actionsRepository)

    @PerScreen
    @Provides
    fun providePresenter(actionsUseCase: ActionsUseCase) = StatsPresenter(actionsUseCase)
}