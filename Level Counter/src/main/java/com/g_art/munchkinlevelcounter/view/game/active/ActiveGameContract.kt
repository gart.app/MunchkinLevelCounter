package com.g_art.munchkinlevelcounter.view.game.active

import com.g_art.munchkinlevelcounter.model.ActivePlayerModel
import com.g_art.munchkinlevelcounter.model.GameModel
import com.g_art.munchkinlevelcounter.mvp.MPVView
import com.g_art.munchkinlevelcounter.view.helper.move.ItemTouchHelperViewCallback

interface ActiveGameContract {

    interface View: MPVView {
        fun onLoadGameSuccess(game: GameModel)
        fun onLoadGameError(throwable: Throwable)

        fun onPlayerUpdateSuccess(activePlayer: ActivePlayerModel)
        fun onPlayerUpdateError(throwable: Throwable)
        fun onPlayerUpdateFailed(ArgumentException: IllegalArgumentException)

        fun onToggleGenderSuccess(activePlayer: ActivePlayerModel)

        fun onUpdateGameSuccess(game: GameModel)
        fun onUpdateGameError(throwable: Throwable)

        fun editPlayers(gameId: Long, partyId: Long)
        fun setGameMaxLevel(maxLevel: Int)
        fun showMaxLevelChangeDialog(maxLevel: Int)

        fun openStatsView(gameId: Long)
        fun openBattleView(gameId: Long, attackerId: Long)

        fun onError()
        fun onFinishGameError(throwable: Throwable)
    }

    interface SinglePlayerView: View {
        fun onColorChangeSuccess(color: Int)
        fun onStatsChangeSuccess(lvl: Int, gear: Int)
        fun onPlayerWinTheGame(activePlayer: ActivePlayerModel)

        fun startBattleForPlayer(gameId: Long, playerId: Long)
    }

    interface PartyView : View, ItemTouchHelperViewCallback  {
        fun onPlayersLoadSuccess(players: List<ActivePlayerModel>)
        fun onPlayerUpdateSuccess(position: Int, activePlayer: ActivePlayerModel)
        fun scrollToPosition(position:Int)
        fun onStatsChangeSuccess(position:Int, lvl: Int, gear: Int)
        fun onColorChangeSuccess(position:Int, activePlayer: ActivePlayerModel)

        fun onPlayerWinTheGame(position:Int, activePlayer: ActivePlayerModel)

        fun onReorderPlayerSuccess(fromPosition: Int, toPosition: Int)
        fun onReorderPlayerError(throwable: Throwable)
    }

    interface Presenter {
        fun loadGame(gameId: Long)
        fun toggleGender()

        fun lvlUp()
        fun lvlDwn()
        fun gearUp(amount: Int)
        fun gearDwn(amount: Int)
        fun changeColor(selectedColor: Int)

        fun markTheWinner(player: ActivePlayerModel)
        fun setGameMaxLevel(maxLevel: Int)
        fun finishTheGame()
        fun finishTheGameAndOpenStats()
        fun finishTheGame(activePlayer: ActivePlayerModel)
        fun openGameMaxLevelChange()
        fun startBattle()
        fun editPlayers()
    }

    interface PlayerGamePresenter: Presenter

    interface PartyPresenter : Presenter {
        fun selectPlayer(playerId: Long)
        fun selectNextPlayer()
        fun selectPrevPlayer()

        fun switchPlayersOrder(fromPosition: Int, toPosition: Int)

        fun savePlayersOrder()
    }
}