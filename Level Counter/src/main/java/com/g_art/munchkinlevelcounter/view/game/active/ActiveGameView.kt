package com.g_art.munchkinlevelcounter.view.game.active

import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.OvalShape
import android.os.Bundle
import android.view.View
import android.widget.*
import butterknife.BindView
import butterknife.OnClick
import butterknife.Optional
import com.afollestad.materialdialogs.DialogAction
import com.afollestad.materialdialogs.GravityEnum
import com.afollestad.materialdialogs.MaterialDialog
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler
import com.g_art.munchkinlevelcounter.R
import com.g_art.munchkinlevelcounter.entity.Gender
import com.g_art.munchkinlevelcounter.model.ActivePlayerModel
import com.g_art.munchkinlevelcounter.mvp.BaseView
import com.g_art.munchkinlevelcounter.util.*
import com.g_art.munchkinlevelcounter.view.player.list.PlayerListView
import com.google.firebase.analytics.FirebaseAnalytics

abstract class ActiveGameView : BaseView(), ActiveGameContract.View {

    @BindView(R.id.txt_player_name)
    lateinit var playerName: TextView

    @BindView(R.id.total_value)
    lateinit var playerTotal: TextView

    @BindView(R.id.txt_player_lvl_value)
    lateinit var playerLvl: TextView

    @BindView(R.id.txt_player_gear)
    lateinit var playerGear: TextView

    @JvmField
    @BindView(R.id.im_player_gender)
    var playerGender: ImageView? = null

    @JvmField
    @BindView(R.id.player_color)
    var playerColor: View? = null

    var iconColor = ShapeDrawable(OvalShape())


    override fun onPlayerUpdateSuccess(activePlayer: ActivePlayerModel) {
        playerName.text = activePlayer.player.name

        initPlayerStats(activePlayer.playerInGame.level, activePlayer.playerInGame.gear)

        initGender(activePlayer.player.gender)

        initColor(activePlayer.player.color)
    }

    open fun initPlayerStats(lvl: Int, gear: Int) {
        playerTotal.text = (lvl + gear).toString()
        playerLvl.text = lvl.toString()
        playerGear.text = gear.toString()
    }

    fun initGender(gender: Gender) {
        when (gender) {
            Gender.MALE -> playerGender?.setImageResource(R.drawable.ic_gender_man)
            Gender.FEMALE -> playerGender?.setImageResource(R.drawable.ic_gender_woman)
        }
    }

    fun initColor(color: Int) {
        iconColor = ShapeDrawable(OvalShape())
        iconColor.intrinsicHeight = 24
        iconColor.intrinsicWidth = 24

        iconColor.paint.color = color
        playerColor?.background = iconColor
    }

    fun showMultiEditDialog() {
        MaterialDialog.Builder(getStartActivity())
                .items(UtilityData.multiEditValues)
                .itemsGravity(GravityEnum.CENTER)
                .itemsCallback { _: MaterialDialog, _: View, position: Int, _: CharSequence ->
                    val value = UtilityData.multiEditValues[position].value
                    if (value > 0) {
                        gearUp(value)
                    } else {
                        gearDwn(-value)
                    }
                }
                .show()
    }

    fun showWinDialogForPlayer(activePlayer: ActivePlayerModel) {
        MaterialDialog.Builder(getStartActivity())
                .title(R.string.winner)
                .content(R.string.message_for_dialog_cont)
                .positiveText(R.string.ok_btn_for_dialog_cont)
                .negativeText(R.string.cancel_btn_for_dialog_cont)
                .canceledOnTouchOutside(false)
                .onAny { _, which ->
                    when (which) {
                        DialogAction.NEGATIVE -> finishTheGame(activePlayer)
                        else -> {
                            requestToChangeMaxLevel()
                        }
                    }
                }
                .show()
    }

    abstract fun requestToChangeMaxLevel()

    override fun handleBack(): Boolean {
        MaterialDialog.Builder(activity!!)
                .title(R.string.title_dialog_confirm)
                .content(R.string.message_for_dialog_confirm)
                .positiveText(R.string.ok_btn_for_dialog_confirm)
                .negativeText(R.string.cancel_btn_for_dialog_confirm)
                .onNegative { _, _ -> leaveGame() }
                .show()
        return true
    }

    private fun leaveGame() {
        finishTheGame()
        router.popCurrentController()
    }

    abstract fun finishTheGame()
    abstract fun finishTheGame(activePlayer: ActivePlayerModel)

    override fun editPlayers(gameId: Long, partyId: Long) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, EDIT_PLAYERS_CLICK)
        logEvent(BTN_CLICK, bundle)

        val playerListView = PlayerListView().apply {
            args.putLong(ACTION_ID, FROM_THE_GAME)
            args.putLong(GAME_ID, gameId)
            args.putLong(PARTY_ID, partyId)
            args.putString(TAG_KEY, gameId.toString())
        }

        router.pushController(RouterTransaction.with(playerListView)
                .pushChangeHandler(FadeChangeHandler())
                .popChangeHandler(FadeChangeHandler())
                .tag(PlayerListView::class.java.toString()))
    }

    override fun showMaxLevelChangeDialog(maxLevel: Int) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, MAX_LVL_CLICK)
        logEvent(BTN_CLICK, bundle)

        val dialog = MaterialDialog.Builder(activity!!)
                .title(R.string.txt_max_lvl)
                .customView(R.layout.maxlvl_dialog, false)
                .positiveText(R.string.dialog_ok_btn)
                .negativeText(R.string.dialog_cancel_btn)
                .onAny { dialog, which ->
                    when (which) {
                        DialogAction.POSITIVE -> {
                            val editLvl = dialog.customView?.findViewById(R.id.maxLvL) as EditText
                            val lvl = editLvl.text.toString()
                            if (lvl.isNotEmpty()) {
                                try {
                                    val mLvl = Integer.parseInt(lvl)
                                    if (mLvl <= 1) {
                                        Toast.makeText(dialog.context,
                                                R.string.error_max_level_one,
                                                Toast.LENGTH_SHORT
                                        ).show()
                                        editLvl.setText(maxLevel.toString(), TextView.BufferType.EDITABLE)
                                    } else {
                                        setGameMaxLevel(mLvl)
                                    }
                                } catch (ex: NumberFormatException) {
                                    Toast.makeText(dialog.context,
                                            R.string.error_max_lvl_settings,
                                            Toast.LENGTH_SHORT
                                    ).show()
                                    setGameMaxLevel(maxLevel)
                                    dialog.dismiss()

                                    val error = Bundle()
                                    error.putString(FirebaseAnalytics.Param.VALUE, lvl)
                                    logEvent(GAME_ERROR, error)
                                }
                            } else {
                                setGameMaxLevel(maxLevel)
                                dialog.dismiss()
                            }
                        }
                        else -> {
                            setGameMaxLevel(maxLevel)
                            dialog.dismiss()
                        }
                    }
                }
                .build()
        val maxLvlEditText = dialog.customView?.findViewById(R.id.maxLvL) as EditText
        maxLvlEditText.setText(maxLevel.toString(), TextView.BufferType.EDITABLE)
        val position = maxLvlEditText.length()
        maxLvlEditText.setSelection(position)
        dialog.show()
    }

    fun diceClick() {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, DICE_CLICK)
        logEvent(BTN_CLICK, bundle)

        val min = 1
        val max = 6
        val dice = min + (Math.random() * (max - min + 1)).toInt()
        val v = View.inflate(applicationContext, R.layout.dice_dialog, null)
        val diceView = v.findViewById<ImageView>(R.id.imgDice)

        when (dice) {
            1 -> diceView.setImageResource(R.drawable.ic_dice_1)
            2 -> diceView.setImageResource(R.drawable.ic_dice_2)
            3 -> diceView.setImageResource(R.drawable.ic_dice_3)
            4 -> diceView.setImageResource(R.drawable.ic_dice_4)
            5 -> diceView.setImageResource(R.drawable.ic_dice_5)
            6 -> diceView.setImageResource(R.drawable.ic_dice_6)
        }

        val dialog = MaterialDialog.Builder(activity!!)
                .customView(diceView, false)
                .contentGravity(GravityEnum.CENTER)
                .title(R.string.dice)
                .build()

        diceView.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }

    @Optional
    @OnClick(R.id.im_player_gender)
    abstract fun toggleGenderClick()

    @OnClick(R.id.btn_gear_up, R.id.btn_gear_dwn, R.id.btn_gear_multi_edit)
    abstract fun gearChangeClick(view: View)

    @OnClick(R.id.btn_lvl_up, R.id.btn_lvl_dwn)
    abstract fun lvlChangeClick(view: View)

    abstract fun gearUp(value: Int)
    abstract fun gearDwn(value: Int)

}