package com.g_art.munchkinlevelcounter.view.stats

import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import android.view.MenuItem
import android.view.View
import butterknife.BindView
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.g_art.munchkinlevelcounter.LCApplication
import com.g_art.munchkinlevelcounter.R
import com.g_art.munchkinlevelcounter.entity.ActionType
import com.g_art.munchkinlevelcounter.mvp.BaseView
import com.g_art.munchkinlevelcounter.mvp.MVPPresenter
import com.g_art.munchkinlevelcounter.util.RouterPagerAdapter
import com.g_art.munchkinlevelcounter.util.STATS_SCREEN
import com.g_art.munchkinlevelcounter.util.VIEW_OPEN
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.google.firebase.analytics.FirebaseAnalytics
import javax.inject.Inject

class StatsView : BaseView(), StatsContract.View {

    private var pages = arrayOf(ActionType.LVL, ActionType.GEAR, ActionType.POWER, ActionType.TREASURE)

    @Inject
    lateinit var presenter: StatsPresenter

    @BindView(R.id.stats_pager)
    lateinit var viewPager: ViewPager

    @BindView(R.id.tab_layout)
    lateinit var tabLayout: TabLayout

    private lateinit var pagerAdapter: RouterPagerAdapter

    private var source: Long = DEFAULT

    override fun onAttach(view: View) {
        super.onAttach(view)

        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, STATS_SCREEN)
        logEvent(VIEW_OPEN, bundle)

        val gameID = args.getLong(GAME_ID)
        source = args.getLong(ACTION_ID, DEFAULT)

        pagerAdapter = object : RouterPagerAdapter(this) {
            override fun getCount(): Int = pages.size

            override fun configureRouter(router: Router, position: Int) {
                if (!router.hasRootController()) {
                    val statsController = LineChartController(presenter, pages[position], gameID)
                    router.setRoot(RouterTransaction
                            .with(statsController)
                            .tag(pages[position].name))

                    //Dirty hack for the first page initialisation
                    if (pages[position] == ActionType.LVL) {
                        statsController.requestStats()
                    }
                }
            }

            override fun getPageTitle(position: Int): CharSequence {
                return pages[position].name
            }
        }

        viewPager.adapter = pagerAdapter
        tabLayout.setupWithViewPager(viewPager)
        viewPager.offscreenPageLimit = tabLayout.tabCount - 1

        viewPager.addOnPageChangeListener(object: ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {
            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
            }

            override fun onPageSelected(position: Int) {
                pagerAdapter.getRouter(position)?.getControllerWithTag(pages[position].name)?.let {
                    if (it is StatsContract.ChildView) {
                        it.requestStats()
                    }
                }
            }
        })

        presenter.cacheStatsForGame(gameID)

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                return if (source == DEFAULT) {
                    handleBack()
                } else {
                    handleMenuClick()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStatLoadSuccess(stats: Map<ActionType, List<ILineDataSet>>) {
        if (stats.isEmpty()) {
            showMessage(R.string.error_players_load)
        }
    }

    override fun onStatLoadError(throwable: Throwable) {
        showMessage(R.string.error_stats_loading)
    }

    override fun onDestroyView(view: View) {
        if (!activity!!.isChangingConfigurations) {
            viewPager.adapter = null
        }
        tabLayout.setupWithViewPager(null)
        super.onDestroyView(view)
    }

    override fun handleBack(): Boolean {
        if (source == DEFAULT) {
            router.popToRoot()
            return true
        }
        return super.handleBack()
    }

    private fun handleMenuClick(): Boolean {
        return router.popCurrentController()
    }

    override fun getLayoutId(): Int = R.layout.stats_view

    override fun getToolbarTitleId(): Int = R.string.title_stats

    override fun injectDependencies() {
        DaggerStatsComponent.builder()
                .lCAppComponent(LCApplication.component)
                .statsModule(StatsModule())
                .build()
                .inject(this)
    }

    override fun getPresenter(): MVPPresenter = presenter
}