package com.g_art.munchkinlevelcounter.view.settings

import android.annotation.SuppressLint
import android.content.res.Configuration
import android.os.Bundle
import android.preference.PreferenceActivity
import androidx.annotation.LayoutRes
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup

@SuppressLint("Registered")
open class AppCompatPreferenceActivity : PreferenceActivity() {

    var mDelegate: AppCompatDelegate? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        getDelegate()?.installViewFactory()
        getDelegate()?.onCreate(savedInstanceState)
        super.onCreate(savedInstanceState)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        getDelegate()?.onPostCreate(savedInstanceState)
    }

    fun getSupportActionBar(): ActionBar? {
        return getDelegate()?.supportActionBar
    }

    fun setSupportActionBar(toolbar: Toolbar?) {
        getDelegate()?.setSupportActionBar(toolbar)
    }

    override fun getMenuInflater(): MenuInflater {
        return getDelegate()?.menuInflater!!
    }

    override fun setContentView(@LayoutRes layoutResID: Int) {
        getDelegate()?.setContentView(layoutResID)
    }

    override fun setContentView(view: View) {
        getDelegate()?.setContentView(view)
    }

    override fun setContentView(view: View, params: ViewGroup.LayoutParams) {
        getDelegate()?.setContentView(view, params)
    }

    override fun addContentView(view: View, params: ViewGroup.LayoutParams) {
        getDelegate()?.addContentView(view, params)
    }

    override fun onPostResume() {
        super.onPostResume()
        getDelegate()?.onPostResume()
    }

    override fun onTitleChanged(title: CharSequence, color: Int) {
        super.onTitleChanged(title, color)
        getDelegate()?.setTitle(title)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        getDelegate()?.onConfigurationChanged(newConfig)
    }

    override fun onStop() {
        super.onStop()
        getDelegate()?.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        getDelegate()?.onDestroy()
    }

    override fun invalidateOptionsMenu() {
        getDelegate()?.invalidateOptionsMenu()
    }

    private fun getDelegate(): AppCompatDelegate? {
        if (mDelegate == null) {
            mDelegate = AppCompatDelegate.create(this, null)
        }
        return mDelegate
    }
}