package com.g_art.munchkinlevelcounter.view.player.list.adapter

import android.os.Bundle
import androidx.recyclerview.widget.DiffUtil
import com.g_art.munchkinlevelcounter.model.SelectionPlayerModel
import com.g_art.munchkinlevelcounter.util.LCDiffUtil

class PlayerListDiffUtil (
        var oldList: List<SelectionPlayerModel>,
        var newList: List<SelectionPlayerModel>) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return false
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        if (oldList.size <= oldItemPosition) {
            return null
        }

        if (newList.size <= newItemPosition) {
            return null
        }

        val oldPlayer = oldList[oldItemPosition]
        val newPlayer = newList[newItemPosition]
        val diffBundle = Bundle()

        if (oldPlayer.id != newPlayer.id) {
            return null
        }

        if (oldPlayer.name != newPlayer.name) {
            diffBundle.putString(LCDiffUtil.PLAYER_NAME_KEY, newPlayer.name)
        }

        if (oldPlayer.color != newPlayer.color) {
            diffBundle.putInt(LCDiffUtil.PLAYER_COLOR_KEY, newPlayer.color)
        }

        return diffBundle
    }
}