package com.g_art.munchkinlevelcounter.view.game.active

import com.g_art.munchkinlevelcounter.di.PerScreen
import com.g_art.munchkinlevelcounter.repository.GameRepository
import com.g_art.munchkinlevelcounter.repository.PlayerRepository
import com.g_art.munchkinlevelcounter.usecases.GameUseCase
import com.g_art.munchkinlevelcounter.view.game.active.master.MasterGamePresenter
import com.g_art.munchkinlevelcounter.view.game.active.single.SingleGamePresenter
import dagger.Module
import dagger.Provides

@Module
class ActiveGameModule {

    @PerScreen
    @Provides
    fun provideUseCase(gameRepository: GameRepository, playerRepository: PlayerRepository) =
            GameUseCase(gameRepository, playerRepository)

    @PerScreen
    @Provides
    fun provideSingleGamePresenter(gameUseCase: GameUseCase) = SingleGamePresenter(gameUseCase)

    @PerScreen
    @Provides
    fun provideGameMasterPresenter(gameUseCase: GameUseCase) = MasterGamePresenter(gameUseCase)
}