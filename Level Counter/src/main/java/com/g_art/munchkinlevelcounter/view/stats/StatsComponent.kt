package com.g_art.munchkinlevelcounter.view.stats

import com.g_art.munchkinlevelcounter.di.LCAppComponent
import com.g_art.munchkinlevelcounter.di.PerScreen
import dagger.Component


@PerScreen
@Component(modules = [(StatsModule::class)],
        dependencies = [(LCAppComponent::class)])
interface StatsComponent {
    fun inject(view: StatsView)
}