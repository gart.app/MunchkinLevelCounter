package com.g_art.munchkinlevelcounter.view.battle

import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat
import androidx.appcompat.widget.AppCompatCheckBox
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.OnCheckedChanged
import butterknife.OnClick
import com.afollestad.materialdialogs.MaterialDialog
import com.g_art.munchkinlevelcounter.LCApplication
import com.g_art.munchkinlevelcounter.R
import com.g_art.munchkinlevelcounter.ext.invisible
import com.g_art.munchkinlevelcounter.model.ActivePlayerModel
import com.g_art.munchkinlevelcounter.model.GameModel
import com.g_art.munchkinlevelcounter.model.MonsterModel
import com.g_art.munchkinlevelcounter.mvp.BaseView
import com.g_art.munchkinlevelcounter.mvp.MVPPresenter
import com.g_art.munchkinlevelcounter.util.*
import com.google.firebase.analytics.FirebaseAnalytics
import javax.inject.Inject

/**
 * Created by agulia on 3/12/18.
 */

class BattleView: BaseView(), BattleContract.View {

    @Inject
    lateinit var presenter: BattlePresenter

    @BindView(R.id.txt_battle_player_name)
    lateinit var playerName: TextView

    @BindView(R.id.txt_battle_lvl_value)
    lateinit var playerLvl: TextView

    @BindView(R.id.txt_battle_mods_value)
    lateinit var playerMods: TextView

    @BindView(R.id.txt_battle_gear_value)
    lateinit var playerGear: TextView

    @BindView(R.id.txt_battle_player_power)
    lateinit var playerPower: TextView

    @BindView(R.id.txt_battle_m_lvl_value)
    lateinit var monsterLvl: TextView

    @BindView(R.id.txt_battle_m_mods_value)
    lateinit var monsterMods: TextView

    @BindView(R.id.txt_battle_m_power)
    lateinit var monsterPower: TextView

    @BindView(R.id.txt_battle_m_tr_value)
    lateinit var monsterTreasures: TextView

    @BindView(R.id.txt_battle_helper_power_value)
    lateinit var companionPower: TextView

    @BindView(R.id.fab_battle_companion)
    lateinit var companionFab: FloatingActionButton

    @BindView(R.id.chb_battle_warrior)
    lateinit var warrior: AppCompatCheckBox

    @BindView(R.id.diagonal_divider)
    lateinit var mDivider: View

    override fun onAttach(view: View) {
        super.onAttach(view)

        initDivider()

        with(presenter) {
            start(this@BattleView)
            val gameId = args.getLong(GAME_ID)
            val playerId = args.getLong(PLAYER_ID)
            loadBattleGame(gameId, playerId)
        }

        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, BATTLE_SCREEN)
        logEvent(VIEW_OPEN, bundle)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> closeBattle()
        }
        return true
    }

    override fun onLoadGameSuccess(game: GameModel) {
    }

    override fun onPlayerUpdateSuccess(activePlayer: ActivePlayerModel) {
        playerName.text = activePlayer.player.name
        playerLvl.text  = activePlayer.playerInGame.level.toString()
        playerGear.text = activePlayer.playerInGame.gear.toString()
        playerMods.text = activePlayer.playerInGame.mods.toString()

        warrior.isSelected = activePlayer.playerInGame.warrior
    }

    override fun onPowerUpdate(playerPower: Int) {
        this.playerPower.text = playerPower.toString()
    }

    override fun displayCompanions(players: List<ActivePlayerModel>, selectedPosition: Int?) {

        val playersList: ArrayList<String> = ArrayList(players.size)

        for (player in players) {
            playersList.add(player.getCompanionInfo(activity!!.getString(R.string.companion_info_template)))
        }

        val dialogBuilder = MaterialDialog.Builder(activity!!)
                .items(playersList)
                .positiveText(R.string.title_dialog_choose_helper_ok)
                .onPositive { dialog, _ -> companionSelected(dialog.selectedIndex) }
                .neutralText(R.string.title_dialog_choose_helper_cancel)
                .negativeText(R.string.title_dialog_choose_helper_remove)
                .onNegative { _, _ -> companionSelected(-1) }
                .autoDismiss(true)

        dialogBuilder.itemsCallbackSingleChoice(selectedPosition ?: 0) { _, _, _, _ -> true }

        dialogBuilder.build().show()
    }

    private fun companionSelected(selectedPosition: Int) {
        presenter.selectCompanion(selectedPosition)
    }

    override fun onSelectedCompanion(activePlayer: ActivePlayerModel, selectedPosition: Int) {
        companionPower.text = String.format(activity!!.resources.getString(R.string.helper_power_value), activePlayer.playerInGame.getTotalPower())
        companionBtnImage(R.drawable.ic_autorenew_helper_24dp)
    }

    override fun onRemoveCompanion() {
        companionPower.text = ""
        companionBtnImage(R.drawable.ic_person_add_24dp)
    }

    private fun companionBtnImage(resId: Int) {
        val icon: Drawable? = if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            VectorDrawableCompat.create(resources!!, resId, activity!!.theme)
        } else {
            this.resources!!.getDrawable(resId, activity!!.theme)
        }
        companionFab.setImageDrawable(icon)
    }

    override fun onMonsterUpdateSuccess(monster: MonsterModel) {
        monsterLvl.text  = monster.level.toString()
        monsterMods.text = monster.mods.toString()
        monsterTreasures.text = monster.treasures.toString()
        monsterPower.text = monster.getPower().toString()
    }

    @OnClick(R.id.fab_battle_lvl_up, R.id.fab_battle_lvl_dwn,
            R.id.fab_battle_gear_up, R.id.fab_battle_gear_dwn,
            R.id.fab_battle_mods_up, R.id.fab_battle_mods_dwn)
    fun playerBtnClick(view: View) {
        when (view.id) {
            R.id.fab_battle_lvl_up -> presenter.lvlUp(true)
            R.id.fab_battle_lvl_dwn -> presenter.lvlDwn(true)
            R.id.fab_battle_gear_up -> presenter.gearUp(1)
            R.id.fab_battle_gear_dwn -> presenter.gearDwn(1)
            R.id.fab_battle_mods_up -> presenter.modifierUp(1, true)
            R.id.fab_battle_mods_dwn -> presenter.modifierDwn(1, true)
        }

    }

    @OnClick(R.id.fab_battle_m_lvl_up, R.id.fab_battle_m_lvl_dwn,
            R.id.fab_battle_m_mods_up, R.id.fab_battle_m_mods_dwn,
            R.id.fab_battle_m_tr_up, R.id.fab_battle_m_tr_dwn)
    fun monsterBtnClick(view: View) {
        when (view.id) {
            R.id.fab_battle_m_lvl_up -> presenter.lvlUp(false)
            R.id.fab_battle_m_lvl_dwn -> presenter.lvlDwn(false)
            R.id.fab_battle_m_mods_up -> presenter.modifierUp(1, false)
            R.id.fab_battle_m_mods_dwn -> presenter.modifierDwn(1, false)
            R.id.fab_battle_m_tr_up -> presenter.treasuresUp()
            R.id.fab_battle_m_tr_dwn -> presenter.treasuresDwn()
        }

    }

    @OnCheckedChanged(R.id.chb_battle_warrior)
    fun warriorClick(checkBox: AppCompatCheckBox, checked: Boolean) {
        presenter.toggleWarrior(checked)
    }

    @OnClick(R.id.fab_battle_companion)
    fun companionClick() {
        presenter.loadCompanions()
    }

    @OnClick(R.id.fab_battle_fight)
    fun fight() {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, FIGHT)
        logEvent(BTN_CLICK, bundle)

        presenter.fight()
    }

    override fun winBattleMessage() {
        MaterialDialog.Builder(activity!!)
                .title(R.string.battle_dialog_title_win)
                .content(R.string.battle_dialog_msg)
                .positiveText(R.string.ok_btn_dialog_battle_continue)
                .negativeText(R.string.cancel_btn_for_dialog_battle)
                .onPositive { _, _ -> router.popCurrentController() }
                .autoDismiss(true)
                .show()
    }

    override fun loseBattleMessage() {
        MaterialDialog.Builder(activity!!)
                .title(R.string.battle_dialog_title_lose)
                .content(R.string.battle_dialog_msg_lose)
                .positiveText(R.string.battle_dialog_lose_run_away)
                .onPositive { _, _ ->
                    runAway()
                }
                .negativeText(R.string.battle_dialog_lose_anyway)
                .onNegative { _, _ ->
                    router.popCurrentController()
                }
                .autoDismiss(true)
                .show()
    }

    @OnClick(R.id.fab_battle_run_away)
    fun runAway() {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, RUN_AWAY)
        logEvent(BTN_CLICK, bundle)
        runAwayMessage()
    }

    override fun runAwayMessage() {
        MaterialDialog.Builder(activity!!)
                .title(R.string.battle_dialog_title_run)
                .content(R.string.battle_dialog_msg_run)
                .positiveText(R.string.battle_dialog_run_confirm)
                .onPositive { _, _ ->
                    showDice()
                }
                .negativeText(R.string.battle_dialog_run_cancel)
                .autoDismiss(true)
                .show()
    }

    override fun disableCompanionBtn() {
        companionFab.invisible()
    }

    private fun showDice() {
        val min = 1
        val max = 6
        val dice = min + (Math.random() * (max - min + 1)).toInt()
        val v = View.inflate(applicationContext, R.layout.dice_dialog, null)
        val diceView = v.findViewById<ImageView>(R.id.imgDice)

        when (dice) {
            1 -> diceView.setImageResource(R.drawable.ic_dice_1)
            2 -> diceView.setImageResource(R.drawable.ic_dice_2)
            3 -> diceView.setImageResource(R.drawable.ic_dice_3)
            4 -> diceView.setImageResource(R.drawable.ic_dice_4)
            5 -> diceView.setImageResource(R.drawable.ic_dice_5)
            6 -> diceView.setImageResource(R.drawable.ic_dice_6)
        }

        val dialog = MaterialDialog.Builder(activity!!)
                .title(R.string.dice)
                .customView(diceView, false)
                .autoDismiss(true)
                .positiveText(R.string.dialog_ok_btn)
                .onPositive { _, _ ->
                    presenter.run(dice)
                }
                .build()

        diceView.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }

    override fun onPlayerUpdateError(throwable: Throwable) {
        showMessage(R.string.error_player_update)
    }

    override fun handleBack(): Boolean {
        presenter.resetMods()
        return super.handleBack()
    }

    override fun closeBattle() {
        presenter.resetMods()
        router.popCurrentController()
    }

    override fun getLayoutId(): Int = R.layout.battle_screen

    override fun getToolbarTitleId(): Int = R.string.battle_screen

    override fun injectDependencies() {
        DaggerBattleComponent.builder()
                .lCAppComponent(LCApplication.component)
                .battleModule(BattleModule())
                .build()
                .inject(this)
    }

    override fun getPresenter(): MVPPresenter = presenter

    private fun initDivider() {
        val divider: Drawable? = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            VectorDrawableCompat.create(resources!!, R.drawable.ic_test_divider_1, activity!!.theme)
        } else {
            resources!!.getDrawable(R.drawable.ic_test_divider_1, activity!!.theme)
        }
        mDivider.background = divider
    }
}