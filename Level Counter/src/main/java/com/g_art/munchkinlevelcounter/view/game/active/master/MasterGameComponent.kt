package com.g_art.munchkinlevelcounter.view.game.active.master

import com.g_art.munchkinlevelcounter.di.LCAppComponent
import com.g_art.munchkinlevelcounter.di.PerScreen
import com.g_art.munchkinlevelcounter.view.game.active.ActiveGameModule
import dagger.Component

@PerScreen
@Component(modules = [(ActiveGameModule::class)],
        dependencies = [(LCAppComponent::class)])
interface MasterGameComponent {
    fun inject(view: MasterGameView)
}