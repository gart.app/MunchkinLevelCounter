package com.g_art.munchkinlevelcounter.view.player.list

import com.g_art.munchkinlevelcounter.model.SelectionPlayerModel
import com.g_art.munchkinlevelcounter.mvp.MPVView

/**
 * Created by G_Artem on 3/24/18.
 */
interface PlayerListContract {
    interface View : MPVView {
        fun onLoadPlayersSuccess(players: List<SelectionPlayerModel>)
        fun onLoadPlayersError(throwable: Throwable)

        fun onDeletePlayerSuccess(position: Int)
        fun onDeletePlayerError(throwable: Throwable)

        fun onPlayerSelectSuccess(position: Int)
        fun onPlayerSelectError(throwable: Throwable)

        fun onLoadPartyPlayersSuccess(players: List<SelectionPlayerModel>)
        fun onLoadPartyPlayersError(throwable: Throwable)

        fun onSavePlayersToPartySuccess()
        fun onSavePlayersToTheGameSuccess(gameId: Long, isMasterGame: Boolean, wasMasterGame: Boolean)
        fun onSavePartyAndPlayersToTheGameSuccess(partyId: Long, gameId: Long)
        fun onSavePlayersToPartyError(throwable: Throwable)

        fun onStartGameSuccess(gameId: Long)
        fun onStartGameError(throwable: Throwable)

        //ActionCallback
        fun resetAction()
        fun onOkActionClick(): Boolean
    }

    interface Presenter {
        fun loadAllPlayers()

        fun loadPlayersForParty(partyId: Long)

        fun loadPlayersFromTheGame(partyId:Long, gameId: Long)

        fun removePlayerById(id: Long)

        fun editPlayersInParty(playerId: Long)

        fun savePlayersToParty(partyId: Long)

        fun savePlayersToPartyAndGame(partyId: Long, gameId: Long)

        fun startTheGame(playerId: Long)
    }
}