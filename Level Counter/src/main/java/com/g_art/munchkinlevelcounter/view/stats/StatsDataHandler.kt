package com.g_art.munchkinlevelcounter.view.stats

import android.content.Context
import android.util.SparseArray
import com.g_art.munchkinlevelcounter.model.PlayerActionModel
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import java.util.*

class StatsDataHandler {

    val LVL_STATS = 0
    val GEAR_STATS = 1
    val POWER_STATS = 2
    val LINE_WIDTH = 3f
    private val CLEAR_STRING_BUILDER = 0

    fun getStats(actions: ArrayList<PlayerActionModel>?, chartView: LineChart?, context: Context, stats: Int): SparseArray<String>? {
        var MAX_VALUE = 0
        var MIN_VALUE = 0
        val playersColorSparse = SparseArray<String>()

        var set: LineDataSet
        val dataSets = ArrayList<ILineDataSet>()

        var dataSet: MutableList<Entry>

//        if (actions != null && chartView != null) {
//            for (action in actions) {
//                dataSet = ArrayList()
//
//                when (stats) {
//                    LVL_STATS -> {
//                        MIN_VALUE = 1
//                        for (i in 0 until player.getLvlStats().size()) {
//                            val lvlValue = Integer.parseInt(player.getLvlStats().get(i))
//
//                            dataSet.add(Entry(i.toFloat(), lvlValue.toFloat()))
//
//                            if (MAX_VALUE < lvlValue) {
//                                MAX_VALUE = lvlValue
//                            }
//                        }
//                    }
//                    GEAR_STATS -> {
//                        var gearChanged = false
//                        MIN_VALUE = 0
//                        for (i in 0 until player.getGearStats().size()) {
//                            val gearValue = Integer.parseInt(player.getGearStats().get(i))
//
//                            dataSet.add(Entry(i.toFloat(), gearValue.toFloat()))
//
//                            if (MAX_VALUE < gearValue) {
//                                MAX_VALUE = gearValue
//                            }
//                            if (gearValue != 0) {
//                                gearChanged = true
//                            }
//                            if (MIN_VALUE > gearValue) {
//                                MIN_VALUE = gearValue
//                            }
//                        }
//                        if (!gearChanged) {
//                            MAX_VALUE = GEAR_STATS
//                        }
//                    }
//                    POWER_STATS -> {
//                        MIN_VALUE = 0
//                        for (i in 0 until player.getPowerStats().size()) {
//                            val powerValue = Integer.parseInt(player.getPowerStats().get(i))
//
//                            dataSet.add(Entry(i.toFloat(), powerValue.toFloat()))
//
//                            if (MAX_VALUE < powerValue) {
//                                MAX_VALUE = powerValue
//                            }
//                            if (MIN_VALUE > powerValue) {
//                                MIN_VALUE = powerValue
//                            }
//                        }
//                    }
//                }
//
//                var color = player.getColor()
//                if (color == 0) {
//                    player.generateColor()
//                    color = player.getColor()
//                }
//
//                set = LineDataSet(dataSet, player.getName() + " - " + context.getString(if (player.isWinner()) R.string.winner else R.string.loser))
//                set.setDrawCircles(false)
//                set.color = color
//                set.lineWidth = LINE_WIDTH
//                set.cubicIntensity = 0.1f
//                set.mode = LineDataSet.Mode.CUBIC_BEZIER
//                set.setDrawValues(false)
//                set.isHighlightEnabled = false
//
//                dataSets.add(set)
//            }
//
//            chartView.data = LineData(dataSets)
//
//            return playersColorSparse
//        }

        return null
    }
}