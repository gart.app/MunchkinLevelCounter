package com.g_art.munchkinlevelcounter.view.stats

import com.g_art.munchkinlevelcounter.entity.ActionType
import com.g_art.munchkinlevelcounter.mvp.MPVView
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet

interface StatsContract {
    interface View : MPVView {
        fun onStatLoadSuccess(stats: Map<ActionType, List<ILineDataSet>>)
        fun onStatLoadError(throwable: Throwable)
    }

    interface ChildView : MPVView {
        fun requestStats()
        fun onStatLoadSuccess(stats: List<ILineDataSet>?)
        fun onStatLoadError(throwable: Throwable)
    }

    interface Presenter {
        fun cacheStatsForGame(gameId: Long)
        fun cacheStatsForPlayer(gameId: Long, playerId: Long)
        fun loadStatsForType(gameId: Long, statsType: ActionType, childView: ChildView)
    }

}