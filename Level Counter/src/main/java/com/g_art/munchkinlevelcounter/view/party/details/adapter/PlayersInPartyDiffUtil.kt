package com.g_art.munchkinlevelcounter.view.party.details.adapter

import android.os.Bundle
import androidx.recyclerview.widget.DiffUtil
import com.g_art.munchkinlevelcounter.model.PlayerModel
import com.g_art.munchkinlevelcounter.util.LCDiffUtil.Companion.GENDER_KEY
import com.g_art.munchkinlevelcounter.util.LCDiffUtil.Companion.PLAYER_COLOR_KEY
import com.g_art.munchkinlevelcounter.util.LCDiffUtil.Companion.PLAYER_NAME_KEY

class PlayersInPartyDiffUtil(
        var oldList: List<PlayerModel>,
        var newList: List<PlayerModel>) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
//        return oldList[oldItemPosition].equals(newList[newItemPosition])
        return false
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        if (oldList.size <= oldItemPosition) {
            return null
        }

        if (newList.size <= newItemPosition) {
            return null
        }

        val oldPlayer = oldList[oldItemPosition]
        val newPlayer = newList[newItemPosition]
        val diffBundle = Bundle()

        if (oldPlayer.id != newPlayer.id) {
            return null
        }

        if (oldPlayer.name != newPlayer.name) {
            diffBundle.putString(PLAYER_NAME_KEY, newPlayer.name)
        }

        if (oldPlayer.color != newPlayer.color) {
            diffBundle.putInt(PLAYER_COLOR_KEY, newPlayer.color)
        }

        if (oldPlayer.gender != newPlayer.gender) {
            diffBundle.putString(GENDER_KEY, newPlayer.gender.name)
        }

        return diffBundle
    }
}