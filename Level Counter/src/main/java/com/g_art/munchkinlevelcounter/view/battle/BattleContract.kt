package com.g_art.munchkinlevelcounter.view.battle

import com.g_art.munchkinlevelcounter.model.ActivePlayerModel
import com.g_art.munchkinlevelcounter.model.GameModel
import com.g_art.munchkinlevelcounter.model.MonsterModel
import com.g_art.munchkinlevelcounter.mvp.MPVView

interface BattleContract {

    interface View : MPVView {
        fun onLoadGameSuccess(game: GameModel)
        fun onPlayerUpdateSuccess(activePlayer: ActivePlayerModel)
        fun onMonsterUpdateSuccess(monster: MonsterModel)

        fun onPowerUpdate(playerPower: Int)
        fun onPlayerUpdateError(throwable: Throwable)

        fun displayCompanions(players: List<ActivePlayerModel>, selectedPosition: Int?)
        fun onSelectedCompanion(activePlayer: ActivePlayerModel, selectedPosition: Int)
        fun onRemoveCompanion()

        fun winBattleMessage()
        fun loseBattleMessage()

        fun runAwayMessage()
        fun disableCompanionBtn()

        fun closeBattle()

    }

    interface Presenter {
        fun loadBattleGame(gameId: Long, attackerId: Long)
        fun loadCompanions()
        fun selectCompanion(positionId: Int)
        fun toggleWarrior(checked: Boolean)

        fun fight()
        fun run(diceResult: Int)

        fun lvlUp(isPlayer: Boolean)
        fun lvlDwn(isPlayer: Boolean)
        fun gearUp(amount: Int)
        fun gearDwn(amount: Int)
        fun modifierUp(amount: Int, isPlayer: Boolean)
        fun modifierDwn(amount: Int, isPlayer: Boolean)
        fun resetMods()
        fun treasuresUp()
        fun treasuresDwn()

    }

}