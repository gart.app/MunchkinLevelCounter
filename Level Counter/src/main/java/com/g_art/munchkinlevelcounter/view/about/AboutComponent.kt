package com.g_art.munchkinlevelcounter.view.about

import com.g_art.munchkinlevelcounter.di.LCAppComponent
import com.g_art.munchkinlevelcounter.di.PerScreen
import dagger.Component


@PerScreen
@Component(modules = [(AboutModule::class)],
        dependencies = [(LCAppComponent::class)])
interface AboutComponent {
    fun inject(view: AboutView)
}