package com.g_art.munchkinlevelcounter.view.game.history

import com.g_art.munchkinlevelcounter.di.LCAppComponent
import com.g_art.munchkinlevelcounter.di.PerScreen
import dagger.Component


@PerScreen
@Component(modules = [(GameListModule::class)],
        dependencies = [(LCAppComponent::class)])
interface GameListComponent {
    fun inject(view: GameListView)
}