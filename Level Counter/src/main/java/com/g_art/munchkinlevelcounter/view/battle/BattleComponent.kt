package com.g_art.munchkinlevelcounter.view.battle

import com.g_art.munchkinlevelcounter.di.LCAppComponent
import com.g_art.munchkinlevelcounter.di.PerScreen
import dagger.Component

@PerScreen
@Component(modules = [(BattleModule::class)],
        dependencies = [(LCAppComponent::class)])
interface BattleComponent {
    fun inject(view: BattleView)
}