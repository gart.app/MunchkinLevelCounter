package com.g_art.munchkinlevelcounter.view.game.details

import com.g_art.munchkinlevelcounter.di.LCAppComponent
import com.g_art.munchkinlevelcounter.di.PerScreen
import dagger.Component


@PerScreen
@Component(modules = [(GameDetailsModule::class)],
        dependencies = [(LCAppComponent::class)])
interface GameDetailsComponent {
    fun inject(view: GameDetailsView)
}