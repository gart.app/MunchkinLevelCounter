package com.g_art.munchkinlevelcounter.view.helper.move

import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView


class SimpleItemTouchHelperCallback(private val mView: ItemTouchHelperViewCallback) : ItemTouchHelper.Callback() {

    private var longPressEnabled = true
    private var itemViewSwipeEnabled = true

    override fun isLongPressDragEnabled(): Boolean {
        return longPressEnabled
    }

    override fun isItemViewSwipeEnabled(): Boolean {
        return itemViewSwipeEnabled
    }

    override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
        val dragFlags = ItemTouchHelper.UP or ItemTouchHelper.DOWN
        val swipeFlags = ItemTouchHelper.START or ItemTouchHelper.END
        return makeMovementFlags(dragFlags, swipeFlags)
    }

    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder,
                        target: RecyclerView.ViewHolder): Boolean {
        mView.onItemMove(viewHolder.adapterPosition, target.adapterPosition)
        return true
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        mView.onItemDismiss(viewHolder.adapterPosition)
    }

    fun disableSwipeToRemove() {
        itemViewSwipeEnabled = false
    }

    fun disableLongPress() {
        longPressEnabled = false
    }

}