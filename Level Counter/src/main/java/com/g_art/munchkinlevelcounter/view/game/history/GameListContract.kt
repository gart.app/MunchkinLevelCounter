package com.g_art.munchkinlevelcounter.view.game.history

import com.g_art.munchkinlevelcounter.model.GameModel
import com.g_art.munchkinlevelcounter.mvp.MPVView

interface GameListContract {

    interface View : MPVView {
        fun onLoadGamesSuccess(games: List<GameModel>)
        fun onLoadGamesError(throwable: Throwable)

        fun onGameRemoveSuccess(position: Int)
        fun onGameRemoveError(throwable: Throwable)
    }

    interface Presenter {
        fun loadGames()

        fun removeGameById(gameId: Long)
    }
}