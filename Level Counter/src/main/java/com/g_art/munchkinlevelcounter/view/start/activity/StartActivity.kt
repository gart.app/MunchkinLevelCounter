package com.g_art.munchkinlevelcounter.view.start.activity

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.ViewGroup
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.afollestad.materialdialogs.color.ColorChooserDialog
import com.bluelinelabs.conductor.Conductor
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.g_art.munchkinlevelcounter.R
import com.g_art.munchkinlevelcounter.mvp.BaseView
import com.g_art.munchkinlevelcounter.util.AppType
import com.g_art.munchkinlevelcounter.util.ads.AdsUtility
import com.g_art.munchkinlevelcounter.view.start.StartView
import com.google.firebase.analytics.FirebaseAnalytics
import io.github.inflationx.viewpump.ViewPumpContextWrapper


class StartActivity : AppCompatActivity(), ColorChooserDialog.ColorCallback {
    internal var toolbar: Toolbar? = null
    internal var container: ViewGroup? = null
    var view: BaseView? = null

    private var router: Router? = null
    private var firebaseAnalytics: FirebaseAnalytics? = null
    private var adsUtil: AdsUtility? = null

    private var msp: SharedPreferences? = null
    private var gameShown = false;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        firebaseAnalytics = FirebaseAnalytics.getInstance(this)

        setContentView(R.layout.activity_main)

        toolbar = findViewById(R.id.toolbar)
        container = findViewById(R.id.controller_container)

        setSupportActionBar(toolbar)

        router = Conductor.attachRouter(this, container!!, savedInstanceState)
        if (!router!!.hasRootController()) {
            router!!.setRoot(RouterTransaction.with(StartView()).tag(StartView::class.java.toString()))
        }

        msp = PreferenceManager.getDefaultSharedPreferences(baseContext)

        checkIfPaid()

        val alwaysOn = msp?.getBoolean("pref_screen_on", false)
        alwaysOn?.let {
            if (it) {
                makeScreenOn()
            } else {
                makeScreenOff()
            }
        }

        adsUtil = AdsUtility(this)
    }

    private fun checkIfPaid() {
        val isPaid = packageName.contains(".paid", true)
        msp?.edit()?.putBoolean("paid", isPaid)?.apply()
    }

    override fun onResume() {
        val alwaysOn = msp?.getBoolean("pref_screen_on", false)
        alwaysOn?.let {
            if (it) {
                makeScreenOn()
            } else {
                makeScreenOff()
            }
        }
        super.onResume()
    }

    fun setViewForColorCallback(view: BaseView) {
        this.view = view
    }

    override fun onColorSelection(dialog: ColorChooserDialog, selectedColor: Int) {
        view?.onColorChoose(dialog, selectedColor)
        dialog.dismiss()
    }

    override fun onColorChooserDismissed(dialog: ColorChooserDialog) {
    }

    private fun makeScreenOn() {
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

    private fun makeScreenOff() {
        window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

    fun wasInfoShown(): Boolean {
        return msp?.getBoolean("info_shown", false)?:false
    }

    fun wasGameShown(): Boolean {
        return gameShown
    }

    fun gameShown() {
        gameShown = true
    }

    fun saveVersion(ads: Boolean) {
        msp?.edit()?.let {
            it.putBoolean("ads_enabled", ads)
            it.putBoolean("info_shown", true)
            it.apply()
        }
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase))
    }

    override fun onBackPressed() {
        if (!router!!.handleBack()) {
            super.onBackPressed()
        }
    }

    override fun onDestroy() {
        makeScreenOff()
        super.onDestroy()

    }

    fun getGameType(): Int {
        val paidType = msp?.getBoolean("paid", false)?:false
        if (paidType) {
            return AppType.PAID
        }
        val boolType = msp?.getBoolean("ads_enabled", true)?:false
        return if (boolType) {
            AppType.ADS
        } else {
            AppType.ADS_FREE
        }
    }

    fun getAnalytics(): FirebaseAnalytics? {
        return firebaseAnalytics
    }

    fun showAdIfPossible() {
        adsUtil?.showAd()
    }
}
