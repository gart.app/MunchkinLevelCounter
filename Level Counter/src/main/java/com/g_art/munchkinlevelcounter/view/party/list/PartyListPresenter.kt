package com.g_art.munchkinlevelcounter.view.party.list

import com.g_art.munchkinlevelcounter.model.PartyModel
import com.g_art.munchkinlevelcounter.mvp.BasePresenter
import com.g_art.munchkinlevelcounter.usecases.PartiesUseCase
import com.g_art.munchkinlevelcounter.util.AppType
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

/**
 * Created by agulia on 3/20/18.
 */
class PartyListPresenter
@Inject constructor(private val partiesUseCase: PartiesUseCase) :
        BasePresenter<PartyListContract.View>(),
        PartyListContract.Presenter {

    private var partiesAmount: Int? = 0
    private var parties = ArrayList<PartyModel>()

    override fun loadParties() {
        disposables.add(
                if (view?.getGameState() == AppType.ADS_FREE) {
                    partiesUseCase.loadAllParties(3)
                } else {
                    partiesUseCase.loadAllParties()
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (view?.getGameState() == AppType.ADS_FREE && it.size >= 3) {
                        view?.makeButtonsGone()
                    } else {
                        view?.makeButtonsVisible()
                    }
                    parties.clear()
                    parties.addAll(it)
                    partiesAmount = it.size
                    view?.onLoadPartiesSuccess(it)

                }, { view?.onLoadPartiesError(it) })
        )
    }

    override fun removePartyById(partyId: Long) {
        getPartyPosition(partyId)?.let { position ->
            disposables.add(partiesUseCase.removePartyById(partyId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe( {
                        parties.removeAt(position)
                        partiesAmount = partiesAmount?.dec()
                        partiesAmount?.let {
                            if (view?.getGameState() == AppType.ADS_FREE && it >= 3) {
                                view?.makeButtonsGone()
                            } else {
                                view?.makeButtonsVisible()
                            }
                        }
                        view?.onDeletePartySuccess(position)
                    }, { view?.onDeletePartyError(it) })
            )
        }
    }

    override fun startTheGame(partyId: Long) {
        disposables.add(partiesUseCase.startTheGame(partyId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( { view?.onStartTheGameSuccess(it) }, { view?.onStartTheGameError(it) } )
        )
    }

    private fun getPartyPosition(partyId: Long): Int? {
        parties.forEachIndexed { index, partyModel ->
            if (partyModel.id == partyId) {
                return index
            }
        }
        return null
    }

    private fun getPartyById(partyId: Long): PartyModel? {
        return parties.find { party-> party.id == partyId }
    }
}