package com.g_art.munchkinlevelcounter.view.start

/**
 * Created by G_Artem on 3/18/18.
 */
interface StartContract {
    interface View {
        fun onLoneWolfClick()
        fun onGameModeClick()

        fun onSettingsClick()
        fun onGameHistoryClick()
        fun onAboutClick()

        fun showUnfinishedGameMessage(gameId: Long, single: Boolean)
        fun onError(throwable: Throwable)
    }

    interface Presenter {
        fun checkUnfinishedGame()
        fun finishTheGame(gameId: Long)
    }
}