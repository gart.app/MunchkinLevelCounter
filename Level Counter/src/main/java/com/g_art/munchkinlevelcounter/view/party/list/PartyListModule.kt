package com.g_art.munchkinlevelcounter.view.party.list

import com.g_art.munchkinlevelcounter.di.PerScreen
import com.g_art.munchkinlevelcounter.repository.GameRepository
import com.g_art.munchkinlevelcounter.repository.PartyRepository
import com.g_art.munchkinlevelcounter.usecases.PartiesUseCase
import dagger.Module
import dagger.Provides

/**
 * Created by agulia on 3/20/18.
 */
@Module
class PartyListModule {

    @PerScreen
    @Provides
    fun providePartyUseCase(partyRepository: PartyRepository, gameRepository: GameRepository) =
            PartiesUseCase(partyRepository, gameRepository)

    @PerScreen
    @Provides
    fun providePresenter(partiesUseCase: PartiesUseCase) = PartyListPresenter(partiesUseCase)
}