package com.g_art.munchkinlevelcounter.view.party.details

import com.g_art.munchkinlevelcounter.di.LCAppComponent
import com.g_art.munchkinlevelcounter.di.PerScreen
import dagger.Component

/**
 * Created by agulia on 4/3/18.
 */

@PerScreen
@Component(modules = [(PartyDetailsModule::class)],
        dependencies = [(LCAppComponent::class)])
interface PartyDetailsComponent {
    fun inject(view: PartyDetailsView)
}