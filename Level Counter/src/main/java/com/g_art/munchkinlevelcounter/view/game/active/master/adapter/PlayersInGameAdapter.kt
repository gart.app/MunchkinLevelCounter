package com.g_art.munchkinlevelcounter.view.game.active.master.adapter

import android.graphics.Color
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.OvalShape
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.g_art.munchkinlevelcounter.R
import com.g_art.munchkinlevelcounter.databinding.InGamePlayersRow2Binding
import com.g_art.munchkinlevelcounter.ext.getFirst
import com.g_art.munchkinlevelcounter.model.ActivePlayerModel
import com.g_art.munchkinlevelcounter.model.SelectionPlayerModel
import com.g_art.munchkinlevelcounter.util.LCDiffUtil.Companion.HIGHLIGHT_KEY
import com.g_art.munchkinlevelcounter.util.LCDiffUtil.Companion.PLAYER_COLOR_KEY
import com.g_art.munchkinlevelcounter.util.LCDiffUtil.Companion.PLAYER_LEVEL_KEY
import com.g_art.munchkinlevelcounter.util.LCDiffUtil.Companion.PLAYER_NAME_KEY
import com.g_art.munchkinlevelcounter.util.LCDiffUtil.Companion.PLAYER_TOTAL_KEY
import com.g_art.munchkinlevelcounter.view.helper.move.ItemTouchHelperAdapter
//import kotlinx.android.synthetic.main.in_game_players_row.view.*
import kotlin.collections.ArrayList


class PlayersInGameAdapter(private val clickListener: (Long) -> Unit) :
        RecyclerView.Adapter<PlayersInGameAdapter.ViewHolder>(), ItemTouchHelperAdapter {

    private var players: List<ActivePlayerModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = InGamePlayersRow2Binding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount() = players.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        players[position].apply {
            holder.playerName.text = this.player.name
            val iconColor = ShapeDrawable(OvalShape())
            iconColor.intrinsicHeight = 24
            iconColor.intrinsicWidth = 24
            iconColor.paint.color = this.player.color
            holder.playerColor.background = iconColor
            holder.playerLevel.text = this.playerInGame.level.toString()
            holder.playerTotal.text = String.format(holder.itemView.context.resources.getString(R.string.total_value_container),
                    this.playerInGame.getTotalPower())
            if (this.player.selected) {
                highlightSelection(holder.itemView)
            } else {
                unhighlightSelection(holder.itemView)
            }
            holder.itemView.setOnClickListener { clickListener(this.player.id) }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads)
        } else {
            val diffBundle = payloads.getFirst() as Bundle
            diffBundle.keySet().forEach {
                when (it) {
                    PLAYER_COLOR_KEY -> {
                        val iconColor = ShapeDrawable(OvalShape())
                        iconColor.intrinsicHeight = 24
                        iconColor.intrinsicWidth = 24
                        iconColor.paint.color = diffBundle.getInt(it)
                        holder.playerColor.background = iconColor
                    }
                    PLAYER_NAME_KEY -> holder.playerName.text = diffBundle.getString(it)
                    PLAYER_LEVEL_KEY -> {
                        holder.playerLevel.text = diffBundle.getInt(it).toString()
                    }
                    PLAYER_TOTAL_KEY -> holder.playerTotal.text = diffBundle.getInt(it).toString()
                    HIGHLIGHT_KEY ->
                        when (diffBundle.getBoolean(HIGHLIGHT_KEY)) {
                            true -> highlightSelection(holder.itemView)
                            false -> unhighlightSelection(holder.itemView)
                        }
                }
            }
        }
    }

    fun getPlayerAt(position: Int): ActivePlayerModel {
        return players[position]
    }

    fun updatePlayer(position: Int, player: ActivePlayerModel) {
        (players as ArrayList)[position] = player
        notifyItemChanged(position)
    }

    fun updatePlayers(players: List<ActivePlayerModel>) {
        val diffResult = DiffUtil.calculateDiff(PlayersInGameDiffUtil(this.players, players), true)
        (this.players as ArrayList).clear()
        (this.players as ArrayList).addAll(players)
        diffResult.dispatchUpdatesTo(this)
    }

    fun removePlayer(position: Int) {
        (players as ArrayList).removeAt(position)
        notifyItemRemoved(position)
    }

    override fun onItemMove(fromPosition: Int, toPosition: Int): Boolean {
        notifyItemMoved(fromPosition, toPosition)
        return true
    }

    override fun onItemDismiss(position: Int) {
        //not supported
    }

    class ViewHolder(view: InGamePlayersRow2Binding) : RecyclerView.ViewHolder(view.root) {
        val playerColor = view.playerColor
        val playerName = view.inGamePlayerName
        val playerLevel = view.inGamePlayerLvlValue
        val playerTotal = view.inGamePlayerTotalValue
    }

    private fun highlightSelection(rowView: View) {
        rowView.setBackgroundColor(rowView.resources.getColor(R.color.list_selected))

        val name = rowView.findViewById<View>(R.id.in_game_player_name) as TextView
        name.setTextColor(rowView.resources.getColor(R.color.light_background))
        val lvl = rowView.findViewById<View>(R.id.in_game_player_lvl) as TextView
        lvl.setTextColor(rowView.resources.getColor(R.color.light_background))
        val lvlValue = rowView.findViewById<View>(R.id.in_game_player_lvl_value) as TextView
        lvlValue.setTextColor(rowView.resources.getColor(R.color.light_background))
        val totalValue = rowView.findViewById<View>(R.id.in_game_player_total_value) as TextView
        totalValue.setTextColor(rowView.resources.getColor(R.color.light_background))
    }

    private fun unhighlightSelection(rowView: View) {
        rowView.setBackgroundColor(Color.TRANSPARENT)

        val name = rowView.findViewById<View>(R.id.in_game_player_name) as TextView
        name.setTextColor(rowView.resources.getColor(R.color.dark_background))
        val lvl = rowView.findViewById<View>(R.id.in_game_player_lvl) as TextView
        lvl.setTextColor(rowView.resources.getColor(R.color.dark_background))
        val lvlValue = rowView.findViewById<View>(R.id.in_game_player_lvl_value) as TextView
        lvlValue.setTextColor(rowView.resources.getColor(R.color.dark_background))
        val totalValue = rowView.findViewById<View>(R.id.in_game_player_total_value) as TextView
        totalValue.setTextColor(rowView.resources.getColor(R.color.dark_background))
    }
}