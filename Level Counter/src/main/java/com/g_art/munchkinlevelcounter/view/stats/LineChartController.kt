package com.g_art.munchkinlevelcounter.view.stats

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import com.bluelinelabs.conductor.Controller
import com.g_art.munchkinlevelcounter.R
import com.g_art.munchkinlevelcounter.entity.ActionType
import com.g_art.munchkinlevelcounter.util.AppType
import com.g_art.munchkinlevelcounter.view.start.activity.StartActivity
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet

class LineChartController(): Controller(), StatsContract.ChildView {

    override fun getGameState(): Int {
        return (activity as? StartActivity)?.getGameType() ?: AppType.PAID
    }

    lateinit var presenter: StatsContract.Presenter
    private var statsType: ActionType? = ActionType.LVL
    var gameId: Long = -1
    private var animated = false

    @BindView(R.id.game_stats)
    lateinit var mChart: LineChart

    constructor(presenter: StatsContract.Presenter, statsType: ActionType, gameId: Long) : this() {
        this.presenter = presenter
        this.statsType = statsType
        this.gameId = gameId
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        val view = inflater.inflate(R.layout.game_stats_line_chart, container, false)
        ButterKnife.bind(this, view)
        setHasOptionsMenu(true)
        return view
    }

    override fun onAttach(view: View) {
        super.onAttach(view)

        if (::mChart.isInitialized) {

            mChart.setDrawGridBackground(false)
            mChart.setBackgroundColor(resources!!.getColor(R.color.light_background))
            // enable touch gestures
            mChart.setTouchEnabled(true)
            mChart.setScaleEnabled(true)
            // if disabled, scaling can be done on x- and y-axis separately
            mChart.setPinchZoom(true)

            // no description text
            mChart.description.isEnabled = false
            mChart.legend.isWordWrapEnabled = true
            resources?.getColor(R.color.primaryColor)?.let {
                mChart.legend.textColor = it
            }
            mChart.legend.textSize = 15f
            mChart.legend.xEntrySpace = 15f
            resources?.let { mChart.setBorderColor(it.getColor(R.color.primaryColor)) }

            val desc = Description()
            resources?.getColor(R.color.primaryColor)?.let {
                desc.textColor = it
            }
            desc.textSize = 15f
            statsType?.let {
                when (it) {
                    ActionType.LVL   -> desc.text = resources!!.getString(R.string.level)
                    ActionType.GEAR  -> desc.text = resources!!.getString(R.string.gear)
                    ActionType.POWER -> desc.text = resources!!.getString(R.string.total_power)
                    else -> {
                        if (getGameState() == AppType.ADS_FREE) {
                            desc.text = resources!!.getString(R.string.only_paid)
                        } else {
                            desc.text = resources!!.getString(R.string.treasures)
                        }
                    }
                }
            }

            mChart.description = desc
        }
    }

    override fun onStatLoadSuccess(stats: List<ILineDataSet>?) {
        stats?.let {
            if (statsType != ActionType.TREASURE) {
                mChart.data = LineData(it)
            } else {
                if (getGameState() != AppType.ADS_FREE) {
                    mChart.data = LineData(it)
                }
            }
        }
        if (!animated) {
            mChart.animateXY(1000, 500)
            animated = true
        } else {
            mChart.invalidate()
        }
    }

    override fun requestStats() {
        if (::presenter.isInitialized) {
            presenter.loadStatsForType(gameId, statsType?:ActionType.LVL, this@LineChartController)
        }
    }

    override fun onStatLoadError(throwable: Throwable) {
        Toast.makeText(this.applicationContext, R.string.error_stats_loading, Toast.LENGTH_SHORT).show()
    }
}