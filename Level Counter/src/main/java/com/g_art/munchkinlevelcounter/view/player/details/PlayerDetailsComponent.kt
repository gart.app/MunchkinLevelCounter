package com.g_art.munchkinlevelcounter.view.player.details

import com.g_art.munchkinlevelcounter.di.LCAppComponent
import com.g_art.munchkinlevelcounter.di.PerScreen
import dagger.Component

/**
 * Created by agulia on 3/28/18.
 */
@PerScreen
@Component(modules = [(PlayerDetailsModule::class)],
        dependencies = [(LCAppComponent::class)])
interface PlayerDetailsComponent {
    fun inject(view: PlayerDetailsView)
}