package com.g_art.munchkinlevelcounter.view

import android.os.Bundle
import android.webkit.WebView
import androidx.appcompat.app.AppCompatActivity
import com.g_art.munchkinlevelcounter.R

class WebViewActivity: AppCompatActivity() {

    val PRIVACY_POLICY_URL = "https://sites.google.com/view/level-counter-privacy-policy/home"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webview)
        val myWebView: WebView = findViewById(R.id.webview)
        myWebView.settings.javaScriptEnabled = true
        myWebView.loadUrl(PRIVACY_POLICY_URL)
    }
}