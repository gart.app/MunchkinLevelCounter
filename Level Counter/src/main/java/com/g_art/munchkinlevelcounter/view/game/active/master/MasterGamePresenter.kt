package com.g_art.munchkinlevelcounter.view.game.active.master

import com.g_art.munchkinlevelcounter.model.ActivePartyAndGameModel
import com.g_art.munchkinlevelcounter.model.ActivePlayerModel
import com.g_art.munchkinlevelcounter.mvp.BasePresenter
import com.g_art.munchkinlevelcounter.usecases.GameUseCase
import com.g_art.munchkinlevelcounter.view.game.active.ActiveGameContract
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

class MasterGamePresenter
@Inject constructor(private val useCase: GameUseCase) :
        BasePresenter<ActiveGameContract.PartyView>(),
        ActiveGameContract.PartyPresenter {

    private var playerAndGame: ActivePartyAndGameModel? = null

    private var selectedPlayerPosition = 0
    private var continueAfterWin: Boolean = false

    override fun loadGame(gameId: Long) {
        disposables.add(useCase.loadGameAndParty(gameId)
                .observeOn(Schedulers.computation())
                .map {
                    it.resetSelection()
                    return@map it
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ game ->
                    playerAndGame = game
                    playerAndGame?.let {
                        if (it.activeParty.activePlayers.isEmpty()) {
                            view?.onLoadGameError(IllegalStateException())
                        } else {
                            selectedPlayerPosition = 0

                            view?.onLoadGameSuccess(it.game)
                            view?.onPlayersLoadSuccess(it.activeParty.activePlayers)

                            it.selectPlayer(selectedPlayerPosition)
                            it.getPlayerOnPosition(selectedPlayerPosition)?.let {player->
                                view?.onPlayerUpdateSuccess(selectedPlayerPosition, player)
                            }

                        }
                    }
                }, { view?.onLoadGameError(it) })
        )
    }

    override fun markTheWinner(player: ActivePlayerModel) {
        disposables.add(
                useCase.markTheWinner(player)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { },
                                {
                                    onPlayerUpdateError(it)
                                }
                        )
        )
    }

    override fun selectPlayer(playerId: Long) {
        getPlayerPosition(playerId)?.let { position ->
            playerAndGame?.let { game ->
                disposables.add(Completable.fromAction {
                    playerAndGame?.resetSelection()
                    selectedPlayerPosition = position
                    playerAndGame?.selectPlayer(selectedPlayerPosition)
                }
                        .subscribeOn(Schedulers.computation())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {
                            game.getPlayerOnPosition(selectedPlayerPosition)?.let {
                                view?.onPlayersLoadSuccess(game.activeParty.activePlayers)
                                view?.onPlayerUpdateSuccess(selectedPlayerPosition, it)
                            }
                            view?.scrollToPosition(selectedPlayerPosition)
                        }
                )
            }
        }
    }

    override fun selectNextPlayer() {
        playerAndGame?.nextPlayerPosition(selectedPlayerPosition)?.let {position ->
            playerAndGame?.getPlayerOnPosition(position)?.let {
                selectPlayer(it.player.id)
            }
        }
    }
    override fun selectPrevPlayer() {
        playerAndGame?.prevPlayerPosition(selectedPlayerPosition)?.let {position ->
            playerAndGame?.getPlayerOnPosition(position)?.let {
                selectPlayer(it.player.id)
            }
        }
    }

    override fun setGameMaxLevel(maxLevel: Int) {
        playerAndGame?.let {
            disposables.add(
                    useCase.setGameMaxLevel(it.game, maxLevel)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    {
                                        continueAfterWin = false
                                        view?.onUpdateGameSuccess(it.game)
                                    },
                                    { error ->
                                        continueAfterWin = false
                                        view?.onUpdateGameError(error)
                                    }
                            )
            )
        }
    }

    override fun switchPlayersOrder(fromPosition: Int, toPosition: Int) {
        playerAndGame?.let {
            disposables.add(Completable.fromAction { Collections.swap(it.activeParty.activePlayers, fromPosition, toPosition) }
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe( {
                        savePlayersOrder()
                    }, { error -> view?.onReorderPlayerError(error) } ))
        }
    }

    override fun savePlayersOrder() {
        playerAndGame?.let {
            val size = it.activeParty.activePlayers.size
            if (size == 0) {
                return
            }
            for (i in 0 until size) {
                it.activeParty.activePlayers[i].player.position = i
            }

            disposables.add(useCase.savePlayersOrder(it.activeParty.activePlayers)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                updateSelectedPosition()
                                view?.onPlayersLoadSuccess(it.activeParty.activePlayers)
                            },
                            { error->view?.onReorderPlayerError(error) } ))
        }
    }

    private fun updateSelectedPosition() {
        playerAndGame?.getSelectedPosition()?.let {
            selectedPlayerPosition = it
        }
    }

    override fun finishTheGame() {
        playerAndGame?.let {
            disposables.add(
                    useCase.finishTheGame(it.game.id)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    {},
                                    { error -> view?.onFinishGameError(error) }
                            )
            )
        }
    }

    override fun finishTheGameAndOpenStats() {
        playerAndGame?.let {party ->
            party.getPlayerOnPosition(selectedPlayerPosition)?.let {
                finishTheGame(it)
            }
        }
    }

    override fun finishTheGame(activePlayer: ActivePlayerModel) {
        playerAndGame?.let {
            disposables.add(
                useCase.finishTheGame(activePlayer, it.game.id)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { view?.openStatsView(it.game.id) },
                                { error-> view?.onFinishGameError(error) }
                        )
        )
        }

    }

    override fun openGameMaxLevelChange() {
        playerAndGame?.let {
            view?.showMaxLevelChangeDialog(it.game.maxLevel)
        }
    }

    override fun lvlUp() {
        playerAndGame?.let {game ->
            disposables.add(useCase.lvlUpPlayer(game.activeParty.activePlayers, selectedPlayerPosition)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                game.getPlayerOnPosition(selectedPlayerPosition)?.let {
                                    val maxGameLevel = playerAndGame!!.game.maxLevel
                                    if (it.playerInGame.level >= maxGameLevel && !continueAfterWin) {
                                        continueAfterWin = true
                                        view?.onPlayerWinTheGame(selectedPlayerPosition, it)
                                    } else {
                                        view?.onPlayerUpdateSuccess(selectedPlayerPosition, it)
                                    }
                                }
                            },
                            { onPlayerUpdateError(it) })
            )
        }
    }

    override fun lvlDwn() {
        playerAndGame?.let { game->
            disposables.add(useCase.lvlDwnPlayer(game.activeParty.activePlayers, selectedPlayerPosition)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                game.getPlayerOnPosition(selectedPlayerPosition)?.let {
                                    view?.onPlayerUpdateSuccess(selectedPlayerPosition, it)
                                }
                            },
                            { onPlayerUpdateError(it) })
            )
        }
    }

    override fun gearUp(amount: Int) {
        playerAndGame?.let {game ->
            disposables.add(useCase.gearUpPlayer(game.activeParty.activePlayers, selectedPlayerPosition, amount)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                game.getPlayerOnPosition(selectedPlayerPosition)?.let {
                                    view?.onPlayerUpdateSuccess(selectedPlayerPosition, it)
                                }
                            },
                            { onPlayerUpdateError(it) })
            )
        }
    }

    override fun gearDwn(amount: Int) {
        playerAndGame?.activeParty?.let {
            disposables.add(useCase.gearDwnPlayer(it.activePlayers, selectedPlayerPosition, amount)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                playerAndGame?.getPlayerOnPosition(selectedPlayerPosition)?.let { player->
                                view?.onPlayerUpdateSuccess(selectedPlayerPosition, player) }
                            },
                            { error -> onPlayerUpdateError(error) })
            )
        }
    }

    override fun toggleGender() {
        playerAndGame?.getPlayerOnPosition(selectedPlayerPosition)?.let {
            disposables.add(useCase.toggleGender(it)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { view?.onToggleGenderSuccess(it) },
                            { error -> onPlayerUpdateError(error) })
            )
        }
    }

    override fun changeColor(selectedColor: Int) {
        playerAndGame?.getPlayerOnPosition(selectedPlayerPosition)?.let {
            disposables.add(
                    useCase.changeColor(selectedColor, it)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    {
                                        view?.onColorChangeSuccess(selectedPlayerPosition, it)
                                    },
                                    { error -> onPlayerUpdateError(error) })
            )
        }

    }

    override fun editPlayers() {
        playerAndGame?.let {
            view?.editPlayers(it.game.id, it.game.partyId)
        }
    }

    override fun startBattle() {
        playerAndGame?.let {game ->
            game.getPlayerOnPosition(selectedPlayerPosition)?.let {
                view?.openBattleView(game.game.id, it.player.id)
            }
        }
    }

    private fun onPlayerUpdateError(throwable: Throwable) {
        when (throwable) {
            is IllegalArgumentException -> view?.onPlayerUpdateFailed(throwable)
            else -> view?.onPlayerUpdateError(throwable)
        }
    }

    private fun getPlayerPosition(playerId: Long): Int? {
        playerAndGame?.let {
            it.activeParty.activePlayers.forEachIndexed { index, playerModel ->
                if (playerModel.player.id == playerId) {
                    return index
                }
            }
        }
        return null
    }

    private fun getPlayerById(playerId: Long): ActivePlayerModel? {
        playerAndGame?.let {
            it.activeParty.activePlayers.find { player-> player.player.id == playerId }
        }
        return null
    }

}