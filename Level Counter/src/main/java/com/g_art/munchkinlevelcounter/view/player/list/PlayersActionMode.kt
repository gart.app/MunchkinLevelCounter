package com.g_art.munchkinlevelcounter.view.player.list

import androidx.appcompat.view.ActionMode
import android.view.Menu
import android.view.MenuItem
import com.g_art.munchkinlevelcounter.R

class PlayersActionMode(var view: PlayerListContract.View?) : ActionMode.Callback {

    override fun onCreateActionMode(mode: ActionMode, menu: Menu): Boolean {
        // Inflate a menu resource providing context menu items
        val inflater = mode.menuInflater
        inflater.inflate(R.menu.menu_player_select_mode, menu)
        return true
    }

    override fun onPrepareActionMode(mode: ActionMode, menu: Menu): Boolean {
        return false // Return false if nothing is done
    }

    override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_confirm -> {
                view?.onOkActionClick() ?: false
            }
            else -> false
        }
    }

    override fun onDestroyActionMode(mode: ActionMode) {
        view?.resetAction()
    }
}