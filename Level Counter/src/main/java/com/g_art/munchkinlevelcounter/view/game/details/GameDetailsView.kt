package com.g_art.munchkinlevelcounter.view.game.details

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.TextView
import butterknife.BindView
import butterknife.OnClick
import com.afollestad.materialdialogs.DialogAction
import com.afollestad.materialdialogs.MaterialDialog
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler
import com.g_art.munchkinlevelcounter.LCApplication
import com.g_art.munchkinlevelcounter.R
import com.g_art.munchkinlevelcounter.ext.initRecyclerView
import com.g_art.munchkinlevelcounter.model.ActivePlayerModel
import com.g_art.munchkinlevelcounter.model.GameModel
import com.g_art.munchkinlevelcounter.mvp.BaseView
import com.g_art.munchkinlevelcounter.mvp.MVPPresenter
import com.g_art.munchkinlevelcounter.util.GAME_DETAILS
import com.g_art.munchkinlevelcounter.util.GAME_MASTER_GAME
import com.g_art.munchkinlevelcounter.util.GAME_STARTED
import com.g_art.munchkinlevelcounter.util.VIEW_OPEN
import com.g_art.munchkinlevelcounter.view.game.active.master.MasterGameView
import com.g_art.munchkinlevelcounter.view.game.active.single.SingleGameView
import com.g_art.munchkinlevelcounter.view.game.details.adapter.PlayersInGameAdapter
import com.g_art.munchkinlevelcounter.view.stats.StatsView
import com.google.firebase.analytics.FirebaseAnalytics
import java.text.DateFormat
import java.text.SimpleDateFormat
import javax.inject.Inject

class GameDetailsView: BaseView(), GameDetailsContract.View {
    private val dateFormat: DateFormat = SimpleDateFormat.getDateInstance()

    @Inject
    lateinit var presenter: GameDetailsPresenter

    @BindView(R.id.game_name_text)
    lateinit var gameName: TextView

    @BindView(R.id.game_date_text)
    lateinit var gameDate: TextView

    @BindView(R.id.rv_players_list)
    lateinit var recyclerView: RecyclerView

    private val gameAdapter = PlayersInGameAdapter()

    override fun onAttach(view: View) {
        super.onAttach(view)

        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, GAME_DETAILS)
        logEvent(VIEW_OPEN, bundle)

        recyclerView.initRecyclerView(LinearLayoutManager(view.context), gameAdapter)

        with(presenter) {
            start(this@GameDetailsView)
            loadGame(args.getLong(GAME_ID))
        }
    }

    override fun onLoadGameSuccess(game: GameModel, players: List<ActivePlayerModel>) {
        gameName.text = String.format(applicationContext!!.resources.getString(R.string.game_name_holder), game.id)

        game.endDate?.let {
            gameDate.text = String.format(applicationContext!!.resources.getString(R.string.game_date_holder), dateFormat.format(it))
        }

        gameAdapter.updatePlayers(players)
    }

    @OnClick(R.id.fab_start_game)
    fun onGameStartClick() {
        //show dialog
        MaterialDialog.Builder(getStartActivity())
                .title(R.string.game_play_title)
                .content(R.string.game_play_body)
                .positiveText(R.string.game_play_positive)
                .negativeText(R.string.game_play_negative)
                .neutralText(R.string.game_play_neutral)
                .canceledOnTouchOutside(false)
                .onAny { _, which ->
                    when (which) {
                        DialogAction.POSITIVE -> presenter.replayTheGame()
                        DialogAction.NEGATIVE -> presenter.continueTheGame()
                        else -> {
                            //do nothing
                        }
                    }
                }
                .show()
    }

    override fun openStats(gameId: Long) {
        val statsView = StatsView().apply {
            args.putLong(GAME_ID, gameId)
            args.putLong(ACTION_ID, FROM_THE_GAME)
        }
        router.pushController(RouterTransaction.with(statsView)
                .pushChangeHandler(FadeChangeHandler())
                .popChangeHandler(FadeChangeHandler()))
    }

    @OnClick(R.id.fab_stats)
    fun onStatsClick() {
        showAdIfPossible()
        presenter.openStats()
    }

    override fun openTheGame(gameId: Long, isSingleGame: Boolean) {
        showAdIfPossible()
        when (isSingleGame) {
            true    -> openSingleGame(gameId)
            false   -> openMasterGame(gameId)
        }
    }

    private fun openMasterGame(gameId: Long) {
        val masterGame = MasterGameView().apply {
            args.putLong(GAME_ID, gameId)
            args.putString(TAG_KEY, gameId.toString())
        }
        router.pushController(RouterTransaction.with(masterGame)
                .pushChangeHandler(FadeChangeHandler())
                .popChangeHandler(FadeChangeHandler())
                .tag(gameId.toString()))
    }

    private fun openSingleGame(gameId: Long) {
        val singleGame = SingleGameView().apply {
            args.putLong(GAME_ID, gameId)
            args.putString(TAG_KEY, gameId.toString())
        }
        router.pushController(RouterTransaction.with(singleGame)
                .pushChangeHandler(FadeChangeHandler())
                .popChangeHandler(FadeChangeHandler())
                .tag(gameId.toString()))
    }

    override fun onLoadGameError(throwable: Throwable) {
        showMessage(R.string.error_game_loading)
    }

    override fun onStartGameError(throwable: Throwable) {
        showMessage(R.string.error_game_loading)
    }

    override fun getLayoutId(): Int = R.layout.game_details

    override fun getToolbarTitleId(): Int = R.string.app_name

    override fun injectDependencies() {
        DaggerGameDetailsComponent.builder()
                .lCAppComponent(LCApplication.component)
                .gameDetailsModule(GameDetailsModule())
                .build()
                .inject(this)
    }

    override fun getPresenter(): MVPPresenter = presenter
}