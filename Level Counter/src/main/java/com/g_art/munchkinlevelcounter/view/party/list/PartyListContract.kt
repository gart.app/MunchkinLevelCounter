package com.g_art.munchkinlevelcounter.view.party.list

import com.g_art.munchkinlevelcounter.model.PartyModel
import com.g_art.munchkinlevelcounter.mvp.MPVView

/**
 * Created by agulia on 3/19/18.
 */
interface PartyListContract {
    interface View : MPVView {
        fun onLoadPartiesSuccess(parties: List<PartyModel>)
        fun onLoadPartiesError(throwable: Throwable)
        fun onDeletePartySuccess(position: Int)
        fun onDeletePartyError(throwable: Throwable)
        fun onCreatePartyClicked()
        fun onStartTheGameSuccess(gameId: Long)
        fun onStartTheGameError(throwable: Throwable)
        fun makeButtonsVisible()
        fun makeButtonsGone()
    }

    interface Presenter {
        fun loadParties()

        fun removePartyById(partyId: Long)

        fun startTheGame(partyId: Long)
    }

}