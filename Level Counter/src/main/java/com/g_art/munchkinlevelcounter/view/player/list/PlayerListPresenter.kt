package com.g_art.munchkinlevelcounter.view.player.list

import com.g_art.munchkinlevelcounter.model.PlayerInGameModel
import com.g_art.munchkinlevelcounter.model.PlayerInPartyModel
import com.g_art.munchkinlevelcounter.model.SelectionPlayerModel
import com.g_art.munchkinlevelcounter.mvp.BasePresenter
import com.g_art.munchkinlevelcounter.mvp.BaseView.Companion.DEFAULT
import com.g_art.munchkinlevelcounter.mvp.BaseView.Companion.FROM_THE_GAME
import com.g_art.munchkinlevelcounter.mvp.BaseView.Companion.FROM_THE_PARTY
import com.g_art.munchkinlevelcounter.usecases.PlayersUseCase
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by G_Artem on 3/24/18.
 */
class PlayerListPresenter
@Inject constructor(private val useCase: PlayersUseCase) :
        BasePresenter<PlayerListContract.View>(),
        PlayerListContract.Presenter {

    var source = DEFAULT
    private var selectedPlayerIds = ArrayList<Long>()
    private var originalPlayerIds = ArrayList<Long>()
    private var playerIdsToRemove = ArrayList<Long>()
    private var partyIdForTheGame: Long = -1
    private var players = ArrayList<SelectionPlayerModel>()

    override fun loadAllPlayers() {
        source = DEFAULT
        disposables.add(useCase.loadAllPlayers()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    players.clear()
                    players.addAll(it)
                    view?.onLoadPlayersSuccess(it)
                }, { view?.onLoadPlayersError(it) })
        )
    }

    override fun loadPlayersForParty(partyId: Long) {
        source = FROM_THE_PARTY
        disposables.add(useCase.loadPlayersFromParty(partyId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ players ->
                    players.forEach {
                        if (it.selected) {
                            selectedPlayerIds.add(it.id)
                        }
                    }
                    this.players.clear()
                    this.players.addAll(players)
                    view?.onLoadPartyPlayersSuccess(players)
                }, { view?.onLoadPartyPlayersError(it) })
        )
    }

    override fun loadPlayersFromTheGame(partyId: Long, gameId: Long) {
        source = FROM_THE_GAME
        disposables.add(useCase.loadPlayersFromGame(gameId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ playersInTheGame ->
                    playersInTheGame.forEach {
                        if (it.selected) {
                            selectedPlayerIds.add(it.id)
                            originalPlayerIds.add(it.id)
                        }
                    }
                    partyIdForTheGame = partyId
                    this.players.clear()
                    this.players.addAll(playersInTheGame)
                    view?.onLoadPartyPlayersSuccess(playersInTheGame)
                }, { view?.onLoadPlayersError(it) })
        )
    }

    override fun removePlayerById(id: Long) {
        getPlayerPosition(id)?.let {position ->
            disposables.add(useCase.removePlayerById(id)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        players.removeAt(position)
                        view?.onDeletePlayerSuccess(position)
                    }, { view?.onDeletePlayerError(it) })
            )
        }
    }

    override fun editPlayersInParty(playerId: Long) {
        getPlayerPosition(playerId)?.let { position ->
            disposables.add(Completable.fromAction {
                if (selectedPlayerIds.contains(playerId)) {
                    selectedPlayerIds.remove(playerId)
                    playerIdsToRemove.add(playerId)
                } else {
                    playerIdsToRemove.remove(playerId)
                    selectedPlayerIds.add(playerId)
                }

            }
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ view?.onPlayerSelectSuccess(position) }, { view?.onPlayerSelectError(it) }))
        }
    }

    override fun savePlayersToParty(partyId: Long) {
        val toInsert = mutableListOf<PlayerInPartyModel>()
        selectedPlayerIds.forEach {
            toInsert.add(PlayerInPartyModel(it, partyId))
        }

        val toRemove = mutableListOf<PlayerInPartyModel>()
        playerIdsToRemove.forEach {
            toRemove.add(PlayerInPartyModel(it, partyId))
        }

        disposables.add(
                useCase.editPlayersInParty(toInsert, toRemove)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ view?.onSavePlayersToPartySuccess() }, { view?.onPlayerSelectError(it) })

        )
    }

    override fun savePlayersToPartyAndGame(partyId: Long, gameId: Long) {
        when (source) {
            FROM_THE_PARTY -> {
                editPlayersForParty(partyId)
            }
            FROM_THE_GAME -> {
                val toInsertToGame = mutableListOf<PlayerInGameModel>()
                val toInsertToParty = mutableListOf<PlayerInGameModel>()
                val toRemoveFromGame = mutableListOf<PlayerInGameModel>()

                //From the single game
                if (partyId == -1L) {
                    //remove players from the party
                    playerIdsToRemove.forEach {
                        toRemoveFromGame.add(PlayerInGameModel(gameId = gameId, playerId = it))
                    }
                    //add players to the party
                    selectedPlayerIds.forEach {
                        if (!originalPlayerIds.contains(it)) {
                            toInsertToGame.add(PlayerInGameModel(gameId = gameId, playerId = it))
                        }
                        toInsertToParty.add(PlayerInGameModel(gameId = gameId, playerId = it))
                    }

                    //create party if needed
                    if (selectedPlayerIds.size > 1) {
                        //create party
                        createPartyAndInsertPlayers(toInsertToParty)
                    }
                }
                //From the master game
                else {
                    val toInsert = mutableListOf<PlayerInPartyModel>()
                    val toRemove = mutableListOf<PlayerInPartyModel>()

                    //remove players from the party
                    playerIdsToRemove.forEach {
                        toRemove.add(PlayerInPartyModel(it, partyId))
                        toRemoveFromGame.add(PlayerInGameModel(gameId = gameId, playerId = it))
                    }

                    //add players to the party
                    selectedPlayerIds.forEach {
                        toInsert.add(PlayerInPartyModel(it, partyId))
                        if (!originalPlayerIds.contains(it)) {
                            toInsertToGame.add(PlayerInGameModel(gameId = gameId, playerId = it))
                        }
                    }

                    //add players to the party
                    //remove party if needed
                    editPlayersInTheParty(toInsert, toRemove)
                }


                //add players to the game if needed
                //remove players from the game actions history if needed
                //remove players from the game if needed
                disposables.add(
                        useCase.editPlayersInTheGame(gameId, partyIdForTheGame, toInsertToGame, toRemoveFromGame)
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(
                                        { view?.onSavePlayersToTheGameSuccess(gameId,
                                                selectedPlayerIds.size > 1,
                                                originalPlayerIds.size > 1) },
                                        { view?.onPlayerSelectError(it) }
                                )

                )
            }
        }

    }

    private fun createPartyAndInsertPlayers(toInsert: List<PlayerInGameModel>) {
        disposables.add(
                useCase.createPartyAndInsertPlayers(toInsert)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe( {
                            partyIdForTheGame = it
                        },
                                { view?.onPlayerSelectError(it) })

        )
    }

    private fun editPlayersForParty(partyId: Long) {
        //add players to the party
        val toInsert = mutableListOf<PlayerInPartyModel>()
        selectedPlayerIds.forEach {
            toInsert.add(PlayerInPartyModel(it, partyId))
        }

        //remove players from the party
        val toRemove = mutableListOf<PlayerInPartyModel>()
        playerIdsToRemove.forEach {
            toRemove.add(PlayerInPartyModel(it, partyId))
        }

        editPlayersInThePartyAndGoBack(toInsert, toRemove)
    }

    private fun editPlayersInTheParty(toInsert: MutableList<PlayerInPartyModel>, toRemove: MutableList<PlayerInPartyModel>) {
        disposables.add(
                useCase.editPlayersInParty(toInsert, toRemove)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe( {
                            if (toInsert.size == 1)
                            {
                                partyIdForTheGame = 0
                            }
                        }, { view?.onPlayerSelectError(it) })

        )
    }

    private fun editPlayersInThePartyAndGoBack(toInsert: MutableList<PlayerInPartyModel>, toRemove: MutableList<PlayerInPartyModel>) {
        disposables.add(
                useCase.editPlayersInParty(toInsert, toRemove)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ view?.onSavePlayersToPartySuccess() }, { view?.onPlayerSelectError(it) })

        )
    }

    override fun startTheGame(playerId: Long) {
        getPlayerById(playerId)?.let {player->
            disposables.add(useCase.startTheGame(player)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ view?.onStartGameSuccess(it) }, { view?.onStartGameError(it) })
            )
        }
    }

    private fun getPlayerPosition(playerId: Long): Int? {
        players.forEachIndexed { index, playerModel ->
            if (playerModel.id == playerId) {
                return index
            }
        }
        return null
    }

    private fun getPlayerById(playerId: Long): SelectionPlayerModel? {
        return players.find { player-> player.id == playerId }
    }
}