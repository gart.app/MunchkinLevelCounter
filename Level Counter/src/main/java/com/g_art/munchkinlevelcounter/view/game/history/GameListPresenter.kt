package com.g_art.munchkinlevelcounter.view.game.history

import com.g_art.munchkinlevelcounter.model.GameModel
import com.g_art.munchkinlevelcounter.mvp.BasePresenter
import com.g_art.munchkinlevelcounter.usecases.GameUseCase
import com.g_art.munchkinlevelcounter.util.AppType
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class GameListPresenter
@Inject constructor(private val gameUseCase: GameUseCase) :
        BasePresenter<GameListContract.View>(),
        GameListContract.Presenter {
    private var games = ArrayList<GameModel>()

    override fun loadGames() {
        disposables.add(
                if (view?.getGameState() == AppType.ADS_FREE) {
                    gameUseCase.loadGames(3)
                } else {
                    gameUseCase.loadGames()
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    games.clear()
                    games.addAll(it)
                    view?.onLoadGamesSuccess(it)
                }, { view?.onLoadGamesError(it) })
        )
    }

    override fun removeGameById(gameId: Long) {
        getGamesPosition(gameId)?.let { position ->
            disposables.add(gameUseCase.removeGameById(gameId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                games.removeAt(position)
                                view?.onGameRemoveSuccess(position)
                            },
                            {
                                view?.onGameRemoveError(it)
                            }
                    )
            )
        }
    }

    private fun getGamesPosition(gameId: Long): Int? {
        games.forEachIndexed { index, gameModel ->
            if (gameModel.id == gameId) {
                return index
            }
        }
        return null
    }

    private fun getGameById(playerId: Long): GameModel? {
        return games.find { game-> game.id == playerId }
    }

}