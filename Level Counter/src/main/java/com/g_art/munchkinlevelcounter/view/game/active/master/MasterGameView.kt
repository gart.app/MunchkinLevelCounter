package com.g_art.munchkinlevelcounter.view.game.active.master

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.OnClick
import com.afollestad.materialdialogs.color.ColorChooserDialog
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler
import com.g_art.munchkinlevelcounter.LCApplication
import com.g_art.munchkinlevelcounter.R
import com.g_art.munchkinlevelcounter.ext.disableChangeAnimations
import com.g_art.munchkinlevelcounter.ext.initRecyclerView
import com.g_art.munchkinlevelcounter.model.ActivePlayerModel
import com.g_art.munchkinlevelcounter.model.GameModel
import com.g_art.munchkinlevelcounter.mvp.MVPPresenter
import com.g_art.munchkinlevelcounter.util.GAME_MASTER_GAME
import com.g_art.munchkinlevelcounter.util.GAME_STARTED
import com.g_art.munchkinlevelcounter.util.VIEW_OPEN
import com.g_art.munchkinlevelcounter.view.battle.BattleView
import com.g_art.munchkinlevelcounter.view.game.active.ActiveGameView
import com.g_art.munchkinlevelcounter.view.game.active.ActiveGameContract
import com.g_art.munchkinlevelcounter.view.game.active.ActiveGameModule
import com.g_art.munchkinlevelcounter.view.game.active.master.adapter.PlayersInGameAdapter
import javax.inject.Inject
import com.g_art.munchkinlevelcounter.view.helper.move.SimpleItemTouchHelperCallback
import com.g_art.munchkinlevelcounter.view.stats.StatsView
import com.google.firebase.analytics.FirebaseAnalytics

class MasterGameView : ActiveGameView(), ActiveGameContract.PartyView {

    @Inject
    lateinit var presenter: MasterGamePresenter

    @BindView(R.id.rv_in_game_players)
    lateinit var recyclerView: RecyclerView

    private val clickListener: (Long) -> Unit = this::playerRowClicked
    private val playerAdapter = PlayersInGameAdapter(clickListener)
    private val callback = SimpleItemTouchHelperCallback(this)
    private val touchHelper = ItemTouchHelper(callback)

    override fun onAttach(view: View) {
        super.onAttach(view)

        recyclerView.initRecyclerView(LinearLayoutManager(view.context), playerAdapter)
        recyclerView.disableChangeAnimations()

        callback.disableSwipeToRemove()
        touchHelper.attachToRecyclerView(recyclerView)

        with(presenter) {
            start(this@MasterGameView)
            val gameId = args.getLong(GAME_ID)
            loadGame(gameId)
        }

        playerColor?.visibility = View.GONE

        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, GAME_MASTER_GAME)
        logEvent(GAME_STARTED, bundle)
        logEvent(VIEW_OPEN, bundle)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.master_game_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_edit_players -> presenter.editPlayers()
            R.id.action_start_battle -> presenter.startBattle()
            R.id.action_settings     -> presenter.openGameMaxLevelChange()
            R.id.action_finish       -> finishTheGame()
            R.id.action_dice         -> diceClick()
            android.R.id.home        -> handleBack()
        }
        return true
    }

    override fun onPlayersLoadSuccess(players: List<ActivePlayerModel>) {
        playerAdapter.updatePlayers(players)
    }

    @OnClick(R.id.btn_next_player, R.id.btn_prev_player)
    fun nextPlayerClick(view: View) {
        when(view.id) {
            R.id.btn_prev_player -> presenter.selectPrevPlayer()
            R.id.btn_next_player -> presenter.selectNextPlayer()
        }
    }

    override fun lvlChangeClick(view: View) {
        when(view.id) {
            R.id.btn_lvl_up -> presenter.lvlUp()
            R.id.btn_lvl_dwn -> presenter.lvlDwn()
        }
    }

    override fun gearChangeClick(view: View) {
        when(view.id) {
            R.id.btn_gear_up -> presenter.gearUp(1)
            R.id.btn_gear_dwn -> presenter.gearDwn(1)
            R.id.btn_gear_multi_edit -> showMultiEditDialog()
        }
    }

    override fun onPlayerWinTheGame(position:Int, activePlayer: ActivePlayerModel) {
        onPlayerUpdateSuccess(position, activePlayer)
        showWinDialogForPlayer(activePlayer)
    }

    override fun onToggleGenderSuccess(activePlayer: ActivePlayerModel) {
        initGender(activePlayer.player.gender)
    }

    override fun onStatsChangeSuccess(position: Int, lvl: Int, gear: Int) {
        initPlayerStats(lvl, gear)
    }

    override fun gearUp(value: Int) {
        presenter.gearUp(value)
    }

    override fun gearDwn(value: Int) {
        presenter.gearDwn(value)
    }

    override fun openStatsView(gameId: Long) {
        showAdIfPossible()
        val statsView = StatsView().apply {
            args.putLong(GAME_ID, gameId)
        }

        router.pushController(RouterTransaction.with(statsView)
                .pushChangeHandler(FadeChangeHandler())
                .popChangeHandler(FadeChangeHandler()))
    }

    override fun openBattleView(gameId: Long, attackerId: Long) {
        val battleView = BattleView().apply {
            args.putLong(GAME_ID, gameId)
            args.putLong(PLAYER_ID, attackerId)
        }

        router.pushController(RouterTransaction.with(battleView)
                .pushChangeHandler(FadeChangeHandler())
                .popChangeHandler(FadeChangeHandler()))
    }

    override fun finishTheGame() {
        presenter.finishTheGameAndOpenStats()
    }

    override fun finishTheGame(activePlayer: ActivePlayerModel) {
        presenter.finishTheGame(activePlayer)
    }

    override fun requestToChangeMaxLevel() {
        presenter.openGameMaxLevelChange()
    }

    override fun setGameMaxLevel(maxLevel: Int) {
        presenter.setGameMaxLevel(maxLevel)
    }

    override fun onUpdateGameSuccess(game: GameModel) {
    }

    override fun onUpdateGameError(throwable: Throwable) {
        showMessage(R.string.error_generic)
    }

    override fun onColorChoose(dialog: ColorChooserDialog, selectedColor: Int) {
        super.onColorChoose(dialog, selectedColor)
        presenter.changeColor(selectedColor)
    }

    override fun onColorChangeSuccess(position: Int, activePlayer: ActivePlayerModel) {
        playerAdapter.updatePlayer(position, activePlayer)
    }

    private fun playerRowClicked(playerId: Long) {
        presenter.selectPlayer(playerId)
    }

    override fun onLoadGameSuccess(game: GameModel) {
    }

    override fun onLoadGameError(throwable: Throwable) {
        showMessage(R.string.error_game_loading)
    }

    override fun onPlayerUpdateError(throwable: Throwable) {
        showMessage(R.string.error_player_update)
    }

    override fun onPlayerUpdateFailed(ArgumentException: IllegalArgumentException) {
        showMessage(R.string.error_player_update)
    }

    override fun onError() {
        showMessage(R.string.error_generic)
    }

    override fun onReorderPlayerSuccess(fromPosition: Int, toPosition: Int) {
    }

    override fun onReorderPlayerError(throwable: Throwable) {
        showMessage(R.string.error_player_update)
    }

    override fun onFinishGameError(throwable: Throwable) {
        showMessage(R.string.error_game_finish)
    }

    override fun onItemMove(fromPosition: Int, toPosition: Int) {
        presenter.switchPlayersOrder(fromPosition, toPosition)
//        playerAdapter.onItemMove(fromPosition, toPosition)
    }

    override fun onItemDismiss(position: Int) {
    }

    override fun onPlayerUpdateSuccess(position: Int, activePlayer: ActivePlayerModel) {
        playerAdapter.updatePlayer(position, activePlayer)
        onPlayerUpdateSuccess(activePlayer)
    }

    override fun initPlayerStats(lvl: Int, gear: Int) {
        playerTotal.text = String.format(activity!!.applicationContext.resources.getString(R.string.total_value_container),
                lvl+gear)
        playerLvl.text = lvl.toString()
        playerGear.text = gear.toString()
    }

    override fun scrollToPosition(position: Int) {
        recyclerView.scrollToPosition(position)
    }

    override fun toggleGenderClick() {
        presenter.toggleGender()
    }

    override fun getLayoutId() = R.layout.game_master_screen

    override fun getToolbarTitleId() = R.string.game_master

    override fun getPresenter(): MVPPresenter = presenter

    override fun injectDependencies() {
        DaggerMasterGameComponent.builder()
                .lCAppComponent(LCApplication.component)
                .activeGameModule(ActiveGameModule())
                .build()
                .inject(this)
    }
}