package com.g_art.munchkinlevelcounter.view.player.list.adapter

import android.graphics.Color
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.OvalShape
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.g_art.munchkinlevelcounter.R
import com.g_art.munchkinlevelcounter.databinding.PlayerRowBinding
import com.g_art.munchkinlevelcounter.model.SelectionPlayerModel

/**
 * Created by G_Artem on 3/24/18.
 */
class PlayerListAdapter(private val onPlayerClick: (Long, Int) -> Unit):
        RecyclerView.Adapter<PlayerListAdapter.ViewHolder>() {

    private var players = ArrayList<SelectionPlayerModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = PlayerRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    fun fullPlayersUpdate(players: List<SelectionPlayerModel>) {
        this.players.clear()
        this.players.addAll(players)
        notifyDataSetChanged()
    }

    fun removePlayer(position: Int) {
        if (players.size > position && position >= 0) {
            players.removeAt(position)
            notifyItemRemoved(position)
        } else {
            //players wasn't updated
            notifyDataSetChanged()
        }
    }

    fun tapOnPlayerByPosition(position: Int) {
        if (players.size > position && position >= 0) {
            val player = players[position]

            player.selected = !player.selected

            notifyItemChanged(position)
        }
    }

    fun selectedPlayers() : Int {
        return players.filter { it.selected }.size
    }

    fun resetSelection() {
        return players.forEach { it.selected = false }
    }

    override fun getItemCount() = players.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        players[position].apply {
            holder.playerName.text = this.name
            val iconColor = ShapeDrawable(OvalShape())
            iconColor.intrinsicHeight = 24
            iconColor.intrinsicWidth = 24
            iconColor.paint.color = this.color
            holder.playerColor.background = iconColor
            holder.playerName.setOnClickListener {
                    onPlayerClick(this.id, 1)
            }
            holder.editPlayer.setOnClickListener {
                    onPlayerClick(this.id, 2)
            }
            holder.removePlayer.setOnClickListener {
                    onPlayerClick(this.id, 3)
            }

            if (this.selected) {
                highlightSelection(holder)
            } else {
                unhighlightSelection(holder)
            }

        }
    }

    private fun highlightSelection(holder: ViewHolder) {
        val resources = holder.playerRow.resources!!
        holder.playerRow.setBackgroundColor(resources.getColor(R.color.list_selected))
        holder.playerName.setTextColor(resources.getColor(R.color.light_background))

        holder.removePlayer.visibility = View.GONE
        holder.editPlayer.setImageResource(R.drawable.ic_edit_light_24dp)

    }

    private fun unhighlightSelection(holder: ViewHolder) {
        val resources = holder.playerRow.resources!!

        holder.playerRow.setBackgroundColor(Color.TRANSPARENT)
        holder.playerName.setTextColor(resources.getColor(R.color.dark_background))
        holder.editPlayer.setImageResource(R.drawable.ic_edit_dark_24dp)

        holder.removePlayer.visibility = View.VISIBLE
    }

    class ViewHolder(view: PlayerRowBinding) : RecyclerView.ViewHolder(view.root) {
        val playerColor = view.playerColor
        val playerName = view.txtPlayerName
        val editPlayer = view.imEditPlayer
        val playerRow = view.playerRowView
        val removePlayer = view.imRemovePlayer
    }
}