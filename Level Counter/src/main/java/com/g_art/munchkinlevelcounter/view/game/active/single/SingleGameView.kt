package com.g_art.munchkinlevelcounter.view.game.active.single

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import butterknife.BindView
import butterknife.OnClick
import com.afollestad.materialdialogs.color.ColorChooserDialog
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler
import com.g_art.munchkinlevelcounter.LCApplication
import com.g_art.munchkinlevelcounter.R
import com.g_art.munchkinlevelcounter.ext.visible
import com.g_art.munchkinlevelcounter.model.ActivePlayerModel
import com.g_art.munchkinlevelcounter.model.GameModel
import com.g_art.munchkinlevelcounter.mvp.MVPPresenter
import com.g_art.munchkinlevelcounter.util.*
import com.g_art.munchkinlevelcounter.view.battle.BattleView
import com.g_art.munchkinlevelcounter.view.game.active.ActiveGameView
import com.g_art.munchkinlevelcounter.view.game.active.ActiveGameContract
import com.g_art.munchkinlevelcounter.view.game.active.ActiveGameModule
import com.g_art.munchkinlevelcounter.view.stats.StatsView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.analytics.FirebaseAnalytics
import javax.inject.Inject


class SingleGameView: ActiveGameView(), ActiveGameContract.SinglePlayerView {

    @Inject
    lateinit var presenter: SingleGamePresenter

    @BindView(R.id.btn_dice)
    lateinit var diceBtn: FloatingActionButton

    @BindView(R.id.btn_start_battle)
    lateinit var battleBtn: FloatingActionButton


    override fun onAttach(view: View) {
        super.onAttach(view)

        with(presenter) {
            start(this@SingleGameView)
            val gameId = args.getLong(GAME_ID)
            loadGame(gameId)
        }
        makeButtonsVisible()

        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, LONE_WOLF_GAME)
        logEvent(GAME_STARTED, bundle)
        logEvent(VIEW_OPEN, bundle)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.single_game_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_edit_players -> presenter.editPlayers()
            R.id.action_settings     -> presenter.openGameMaxLevelChange()
            R.id.action_finish       -> presenter.finishTheGameAndOpenStats()
            android.R.id.home   -> handleBack()
        }
        return true
    }

    private fun makeButtonsVisible() {
        diceBtn.visible()
        battleBtn.visible()
    }

    override fun onPlayerWinTheGame(activePlayer: ActivePlayerModel) {
        onStatsChangeSuccess(activePlayer.playerInGame.level, activePlayer.playerInGame.gear)

        showWinDialogForPlayer(activePlayer)
    }

    override fun onColorChangeSuccess(color: Int) {
        initColor(color)
    }

    override fun onStatsChangeSuccess(lvl: Int, gear: Int) {
        initPlayerStats(lvl, gear)
    }

    override fun onToggleGenderSuccess(activePlayer: ActivePlayerModel) {
        initGender(activePlayer.player.gender)
    }

    override fun startBattleForPlayer(gameId: Long, playerId: Long) {
        val battleView = BattleView().apply {
            args.putLong(GAME_ID, gameId)
            args.putLong(PLAYER_ID, playerId)
        }

        router.pushController(RouterTransaction.with(battleView)
                .pushChangeHandler(FadeChangeHandler())
                .popChangeHandler(FadeChangeHandler()))
    }

    override fun openStatsView(gameId: Long) {
        showAdIfPossible()
        val statsView = StatsView().apply {
            args.putLong(GAME_ID, gameId)
        }

        router.pushController(RouterTransaction.with(statsView)
                .pushChangeHandler(FadeChangeHandler())
                .popChangeHandler(FadeChangeHandler()))
    }

    override fun openBattleView(gameId: Long, attackerId: Long) {
        val battleView = BattleView().apply {
            args.putLong(GAME_ID, gameId)
            args.putLong(PLAYER_ID, attackerId)
        }

        router.pushController(RouterTransaction.with(battleView)
                .pushChangeHandler(FadeChangeHandler())
                .popChangeHandler(FadeChangeHandler()))
    }

    override fun finishTheGame() {
        presenter.finishTheGame()
    }

    override fun finishTheGame(activePlayer: ActivePlayerModel) {
        presenter.finishTheGame(activePlayer)
    }

    override fun requestToChangeMaxLevel() {
        presenter.openGameMaxLevelChange()
    }

    override fun setGameMaxLevel(maxLevel: Int) {
        presenter.setGameMaxLevel(maxLevel)
    }

    @OnClick(R.id.player_color)
    fun colorClick() {
        getStartActivity().setViewForColorCallback(this)
        ColorChooserDialog.Builder(getStartActivity(), R.string.choose_player_color)
                .allowUserColorInputAlpha(false)
                .allowUserColorInput(false)
                .cancelButton(R.string.dialog_cancel_btn)
                .backButton(R.string.dialog_back_btn)
                .doneButton(R.string.dialog_ok_btn)
                .dynamicButtonColor(true)
                .preselect(iconColor.paint.color)
                .show()
    }

    @OnClick(R.id.btn_dice)
    fun diceButtonClick() {
        super.diceClick()
    }

    override fun onColorChoose(dialog: ColorChooserDialog, selectedColor: Int) {
        super.onColorChoose(dialog, selectedColor)
        presenter.changeColor(selectedColor)
    }

    override fun lvlChangeClick(view: View) {
        when(view.id) {
            R.id.btn_lvl_up -> presenter.lvlUp()
            R.id.btn_lvl_dwn -> presenter.lvlDwn()
        }
    }

    override fun gearChangeClick(view: View) {
        when(view.id) {
            R.id.btn_gear_up -> presenter.gearUp(1)
            R.id.btn_gear_dwn -> presenter.gearDwn(1)
            R.id.btn_gear_multi_edit -> showMultiEditDialog()
        }
    }

    override fun gearUp(value: Int) {
        presenter.gearUp(value)
    }

    override fun gearDwn(value: Int) {
        presenter.gearDwn(value)
    }

    override fun toggleGenderClick() {
        presenter.toggleGender()
    }

    @OnClick(R.id.btn_start_battle)
    fun battleClick() {
        presenter.startBattle()
    }

    override fun onLoadGameSuccess(game: GameModel) {
    }

    override fun onLoadGameError(throwable: Throwable) {
        showMessage(R.string.error_game_loading)
    }

    override fun onUpdateGameSuccess(game: GameModel) {
    }

    override fun onUpdateGameError(throwable: Throwable) {
        showMessage(R.string.error_generic)
    }

    override fun onPlayerUpdateError(throwable: Throwable) {
        showMessage(R.string.error_player_update)
    }

    override fun onPlayerUpdateFailed(ArgumentException: IllegalArgumentException) {
        showMessage(R.string.error_player_update)
    }

    override fun onError() {
        showMessage(R.string.error_generic)
    }

    override fun onFinishGameError(throwable: Throwable) {
        showMessage(R.string.error_game_finish)
    }

    override fun getLayoutId() = R.layout.single_player_screen

    override fun getToolbarTitleId() = R.string.single_mode

    override fun getPresenter(): MVPPresenter = presenter

    override fun injectDependencies() {
        DaggerSingleGameComponent.builder()
                .lCAppComponent(LCApplication.component)
                .activeGameModule(ActiveGameModule())
                .build()
                .inject(this)
    }
}