package com.g_art.munchkinlevelcounter.view.game.details

import com.g_art.munchkinlevelcounter.di.PerScreen
import com.g_art.munchkinlevelcounter.repository.GameRepository
import com.g_art.munchkinlevelcounter.repository.PlayerRepository
import com.g_art.munchkinlevelcounter.usecases.GameUseCase
import dagger.Module
import dagger.Provides

@Module
class GameDetailsModule {

    @PerScreen
    @Provides
    fun provideUseCase(gameRepository: GameRepository, playerRepository: PlayerRepository) =
            GameUseCase(gameRepository, playerRepository)


    @PerScreen
    @Provides
    fun provideGameDetailsPresenter(gameUseCase: GameUseCase) = GameDetailsPresenter(gameUseCase)

}