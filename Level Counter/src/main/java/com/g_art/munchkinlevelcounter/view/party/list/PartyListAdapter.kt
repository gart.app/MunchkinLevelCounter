package com.g_art.munchkinlevelcounter.view.party.list

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.g_art.munchkinlevelcounter.R
import com.g_art.munchkinlevelcounter.databinding.PartyRowBinding
import com.g_art.munchkinlevelcounter.model.PartyModel

/**
 * Created by agulia on 3/20/18.
 */
class PartyListAdapter(val onPartyClick: (Long, Int) -> Unit) : RecyclerView.Adapter<PartyListAdapter.ViewHolder>() {

    private var parties = ArrayList<PartyModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = PartyRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount() = parties.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        parties[position].apply {
            holder.partyName.setOnClickListener {
                onPartyClick(this.id, 1)
            }
            holder.editParty.setOnClickListener {
                onPartyClick(this.id, 2)
            }
            holder.removeParty.setOnClickListener {
                onPartyClick(this.id, 3)
            }
            holder.partyName.text = this.name
            holder.playersAmount.text = String.format(holder.playersAmount.context.resources.getString(R.string.party_players_amount), this.playersAmount)
        }
    }

    fun fullPartiesUpdate(parties: List<PartyModel>) {
        this.parties.clear()
        this.parties.addAll(parties)
        notifyDataSetChanged()
    }

    fun addParty(party: PartyModel) {
        parties.add(party)
        notifyItemInserted(parties.size)
    }

    fun removeParty(position: Int) {
        parties.removeAt(position)
        notifyItemRemoved(position)
    }

    fun modifyParty(position: Int, modifiedParty: PartyModel) {
        parties[position] = modifiedParty
        notifyItemChanged(position)
    }

    class ViewHolder(view: PartyRowBinding) : RecyclerView.ViewHolder(view.root) {
        val partyName = view.txtPartyName
        val editParty = view.imEditParty
        val removeParty = view.imRemoveParty
        val playersAmount = view.txtPlayersAmount
    }
}