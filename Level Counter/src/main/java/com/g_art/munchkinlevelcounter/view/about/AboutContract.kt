package com.g_art.munchkinlevelcounter.view.about

import com.g_art.munchkinlevelcounter.mvp.MPVView

interface AboutContract {

    interface View: MPVView {

    }

    interface Presenter {

    }
}