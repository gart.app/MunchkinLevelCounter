package com.g_art.munchkinlevelcounter.view.about

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import butterknife.BindView
import butterknife.OnClick
import com.g_art.munchkinlevelcounter.BuildConfig
import com.g_art.munchkinlevelcounter.LCApplication
import com.g_art.munchkinlevelcounter.R
import com.g_art.munchkinlevelcounter.ext.gone
import com.g_art.munchkinlevelcounter.ext.visible
import com.g_art.munchkinlevelcounter.mvp.BaseView
import com.g_art.munchkinlevelcounter.mvp.MVPPresenter
import com.g_art.munchkinlevelcounter.util.AppType
import com.g_art.munchkinlevelcounter.util.BTN_CLICK
import com.g_art.munchkinlevelcounter.util.POLICY_CLICK
import com.g_art.munchkinlevelcounter.view.WebViewActivity
import com.google.firebase.analytics.FirebaseAnalytics
import javax.inject.Inject

class AboutView : BaseView(), AboutContract.View {
    private val PAID_URI = "https://play.google.com/store/apps/details?id=com.g_art.munchkinlevelcounter.paid"
    private val FREE_URI = "https://play.google.com/store/apps/details?id=com.g_art.munchkinlevelcounter"

    @Inject
    lateinit var presenter: AboutPresenter

    @BindView(R.id.btn_full_version)
    lateinit var paidBtn: Button

    @BindView(R.id.p_policy)
    lateinit var policyBtn: Button

    @BindView(R.id.txt_paid)
    lateinit var txtPaid: TextView

    @BindView(R.id.app_v)
    lateinit var txtAppV: TextView

    override fun onAttach(view: View) {
        super.onAttach(view)
        if (getGameState() != AppType.PAID) {
            paidBtn.visible()
            txtPaid.visible()
        } else {
            paidBtn.gone()
            txtPaid.gone()
        }
        txtAppV.text = String.format(applicationContext!!.resources.getString(R.string.app_v), BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE)
    }

    @OnClick(R.id.imb_rate)
    fun rateClick() {
        if (getGameState() == AppType.PAID) {
            openPlayStore(PAID_URI)
        } else {
            openPlayStore(FREE_URI)
        }
    }

    @OnClick(R.id.imb_contact)
    fun contactClick() {
        val email = Intent(Intent.ACTION_SENDTO)
        email.data = Uri.parse("mailto:")
        email.putExtra(Intent.EXTRA_EMAIL, arrayOf("gart.app@gmail.com"))
        email.putExtra(Intent.EXTRA_SUBJECT, "Feedback:"+
                String.format(applicationContext!!.resources.getString(R.string.app_v), BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE))
        email.putExtra(Intent.EXTRA_TEXT, "Dear Developer, ")
        activity?.packageManager?.let {
            if (email.resolveActivity(it) != null) {
                startActivity(email)
            }
        }
    }

    @OnClick(R.id.btn_full_version)
    fun paidClick() {
        openPlayStore(PAID_URI)
    }

    @OnClick(R.id.p_policy)
    fun privacyPolicyClick() {
        openPrivacyPolicyView()
    }

    private fun openPrivacyPolicyView() {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, POLICY_CLICK)
        logEvent(BTN_CLICK, bundle)
        val intent = Intent(activity, WebViewActivity::class.java)
        startActivity(intent)
    }

    private fun openPlayStore(uri: String) {
        try {
            val intent = Intent(Intent.ACTION_VIEW).apply {
                data = Uri.parse(uri)
                setPackage("com.android.vending")
            }
            startActivity(intent)
        } catch (exc: android.content.ActivityNotFoundException) {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(uri)))
        }
    }
    override fun getLayoutId() = R.layout.about_view

    override fun getToolbarTitleId() = R.string.main_screen_About

    override fun injectDependencies() {
        DaggerAboutComponent.builder()
                .lCAppComponent(LCApplication.component)
                .aboutModule(AboutModule())
                .build()
                .inject(this)
    }

    override fun getPresenter(): MVPPresenter = presenter
}