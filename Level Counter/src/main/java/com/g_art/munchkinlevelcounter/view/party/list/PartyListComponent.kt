package com.g_art.munchkinlevelcounter.view.party.list

import com.g_art.munchkinlevelcounter.di.LCAppComponent
import com.g_art.munchkinlevelcounter.di.PerScreen
import dagger.Component

/**
 * Created by agulia on 3/20/18.
 */

@PerScreen
@Component(modules = [(PartyListModule::class)],
        dependencies = [(LCAppComponent::class)])
interface PartyListComponent {
    fun inject(view: PartyListView)
}
