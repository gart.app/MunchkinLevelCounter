package com.g_art.munchkinlevelcounter.view.about

import com.g_art.munchkinlevelcounter.di.PerScreen
import dagger.Module
import dagger.Provides

@Module
class AboutModule {

    @PerScreen
    @Provides
    fun providePresenter() = AboutPresenter()

}