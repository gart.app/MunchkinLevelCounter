package com.g_art.munchkinlevelcounter.view.party.details.adapter

import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.OvalShape
import android.os.Bundle
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.g_art.munchkinlevelcounter.R
import com.g_art.munchkinlevelcounter.databinding.PlayerInPartyRowBinding
import com.g_art.munchkinlevelcounter.databinding.PlayerRowBinding
import com.g_art.munchkinlevelcounter.entity.Gender
import com.g_art.munchkinlevelcounter.ext.getFirst
import com.g_art.munchkinlevelcounter.model.PlayerModel
import com.g_art.munchkinlevelcounter.util.LCDiffUtil
import com.g_art.munchkinlevelcounter.view.helper.move.ItemTouchHelperAdapter
//import kotlinx.android.synthetic.main.player_in_party_row.view.*
import java.util.*
import kotlin.collections.ArrayList

class PlayersInPartyAdapter : RecyclerView.Adapter<PlayersInPartyAdapter.ViewHolder>(), ItemTouchHelperAdapter {

    private var players: List<PlayerModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = PlayerInPartyRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount() = players.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        players[position].apply {
            holder.playerName.text = this.name
            val iconColor = ShapeDrawable(OvalShape())
            iconColor.intrinsicHeight = 24
            iconColor.intrinsicWidth = 24
            iconColor.paint.color = this.color
            holder.playerColor.background = iconColor
            when (this.gender) {
                Gender.MALE -> holder.playerGender.setImageResource(R.drawable.ic_gender_man)
                Gender.FEMALE -> holder.playerGender.setImageResource((R.drawable.ic_gender_woman))
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads)
        } else {
            val diffBundle = payloads.getFirst() as Bundle
            diffBundle.keySet().forEach {
                when (it) {
                    LCDiffUtil.PLAYER_COLOR_KEY -> {
                        val iconColor = ShapeDrawable(OvalShape())
                        iconColor.intrinsicHeight = 24
                        iconColor.intrinsicWidth = 24
                        iconColor.paint.color = diffBundle.getInt(it)
                        holder.playerColor.background = iconColor
                    }
                    LCDiffUtil.PLAYER_NAME_KEY -> holder.playerName.text = diffBundle.getString(it)
                    LCDiffUtil.HIGHLIGHT_KEY -> {
                        when (Gender.getGenderByString(it.toUpperCase())) {
                            Gender.MALE -> holder.playerGender.setImageResource(R.drawable.ic_gender_man)
                            Gender.FEMALE -> holder.playerGender.setImageResource((R.drawable.ic_gender_woman))
                            else -> holder.playerGender.setImageResource(R.drawable.ic_gender_man)
                        }
                    }
                }
            }
        }
    }

    override fun onItemMove(fromPosition: Int, toPosition: Int): Boolean {
        if (fromPosition < toPosition) {
            for (i in fromPosition until toPosition) {
                Collections.swap(players, i, i + 1)
            }
        } else {
            for (i in fromPosition downTo toPosition + 1) {
                Collections.swap(players, i, i - 1)
            }
        }
        notifyItemMoved(fromPosition, toPosition)
        return true
    }

    override fun onItemDismiss(position: Int) {
        removePlayer(position)
    }

    fun getPlayerAt(position: Int): PlayerModel {
        return players[position]
    }

    fun updatePlayers(players: List<PlayerModel>) {
        val diffResult = DiffUtil.calculateDiff(PlayersInPartyDiffUtil(this.players, players), true)
        diffResult.dispatchUpdatesTo(this)
        (this.players as ArrayList).clear()
        (this.players as ArrayList).addAll(players)
    }

    fun removePlayer(position: Int) {
        (players as ArrayList).removeAt(position)
        notifyItemRemoved(position)
    }

    class ViewHolder(view: PlayerInPartyRowBinding) : RecyclerView.ViewHolder(view.root) {
        val playerColor = view.playerColor
        val playerName = view.txtPlayerName
        val playerGender = view.imbSingleGender
    }
}