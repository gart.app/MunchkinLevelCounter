package com.g_art.munchkinlevelcounter.view.party.details

import com.g_art.munchkinlevelcounter.model.PartyAndPlayersModel
import com.g_art.munchkinlevelcounter.model.PartyModel
import com.g_art.munchkinlevelcounter.model.PlayerModel
import com.g_art.munchkinlevelcounter.mvp.BasePresenter
import com.g_art.munchkinlevelcounter.usecases.PartyDetailsUseCase
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

/**
 * Created by agulia on 4/3/18.
 */
class PartyDetailsPresenter
@Inject constructor(private val useCase: PartyDetailsUseCase):
        BasePresenter<PartyDetailsContract.View> (),
        PartyDetailsContract.Presenter {

    private var partyAndPlayers: PartyAndPlayersModel? = null

    override fun getPartyAndPlayersById(id: Long) {

        val idToLoad: Long = if (id != 0L && partyAndPlayers == null) {
            id
        } else {
            partyAndPlayers?.partyModel?.id ?: 0L
        }

        if (idToLoad == 0L) {
            disposables.add(
                    Completable
                            .fromAction { partyAndPlayers = PartyAndPlayersModel(PartyModel(), ArrayList()) }
                            .subscribeOn(Schedulers.computation())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe( {
                                if (partyAndPlayers != null) {
                                    view?.onLoadPartySuccess(partyAndPlayers!!)
                                } else view?.onError()}, { view?.onLoadPartyError(it) } )
            )
        } else {
            disposables.add(useCase.loadPartyAndPlayersById(idToLoad)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe( {
                        partyAndPlayers = it
                        view?.onLoadPartySuccess(it) }, { view?.onLoadPartyError(it) } ))
        }
    }

    override fun removePlayerFromParty(playerId: Long, position: Int) {
        partyAndPlayers?.let { party->
            if (party.players.size > 2) {
                disposables.add(useCase.removePlayerFromParty(partyAndPlayers!!.partyModel.id, playerId)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe( {
                            party.players.removeAt(position)
                            view?.onRemovePlayerSuccess(position)
                        }, { view?.onRemovePlayerError(it) } ))
            } else {
                view?.onNotEnoughPlayersError()
            }
        }
    }

    override fun saveParty() {
        savePlayersOrder()
        disposables.add(
                if (partyAndPlayers!!.partyModel.id == 0L) {
                    useCase.insertParty(partyAndPlayers!!)
                } else {
                    useCase.updateParty(partyAndPlayers!!)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( {
                    nullifyCurrentParty()
                    view?.onSavePartySuccess()
                }, { onSavePartyError(it) } ))
    }

    override fun savePartyAndOpenPlayersView() {
        disposables.add(useCase.insertParty(partyAndPlayers!!.partyModel)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( {
                    useCase.loadPartyAndPlayersById(it)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe {model ->
                                partyAndPlayers = model
                                view?.onSavePartyAndOpenPlayersSuccess(model.partyModel.id)
                            }

                }, { onSavePartyError(it) } ))
    }

    override fun savePartyAndOpenPlayersView(partyName: String) {
        if (partyName != partyAndPlayers!!.partyModel.name || partyAndPlayers!!.partyModel.id == 0L) {
            partyAndPlayers!!.partyModel.name = partyName
            disposables.add(useCase.insertParty(partyAndPlayers!!.partyModel)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe( {
                        savePlayersOrder()
                        disposables.add(
                                useCase.loadPartyAndPlayersById(it)
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe {model ->
                                            partyAndPlayers = model
                                            view?.onSavePartyAndOpenPlayersSuccess(model.partyModel.id)
                                        }
                        )
                    }, { onSavePartyError(it) } ))
        } else {
            view?.onSavePartyAndOpenPlayersSuccess(partyAndPlayers!!.partyModel.id)
        }
    }

    override fun deleteParty() {
        disposables.add(useCase.removePartyById(partyAndPlayers!!.partyModel.id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( {
                    nullifyCurrentParty()
                    view?.onDeletePartySuccess() },
                        { view?.onDeletePartyError(it) } ))
    }

    override fun changePartyName(name: String) {
        disposables.add(
                Completable
                        .fromAction { partyAndPlayers?.partyModel?.name = name }
                        .subscribeOn(Schedulers.computation())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe( { },
                                { onSavePartyError(it) } )
        )
    }

    override fun switchPlayersOrder(fromPosition: Int, toPosition: Int) {
        partyAndPlayers?.let {
            val players = it.players
            Collections.swap(players, fromPosition, toPosition)
            players.forEachIndexed { index, player ->
                player.position = index
            }
            it.players = players
        }
    }

    override fun savePlayersOrder() {
        disposables.add(useCase.savePlayersOrder(partyAndPlayers!!.players)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( { view?.onReorderPlayerSuccess() }, { view?.onReorderPlayerError(it) } ))

    }

    private fun nullifyCurrentParty() {
        partyAndPlayers = null
    }

    private fun onSavePartyError(throwable: Throwable) {
        when (throwable) {
            is IllegalArgumentException -> view?.onPartyValidationFailed(throwable)
            is IllegalStateException -> view?.onNotEnoughPlayersError()
            else -> view?.onSavePartyError(throwable)
        }
    }

    override fun handleBack() {
        partyAndPlayers?.let {
            if (it.players.size >= 2) {
                view?.onHandleBack()
            } else {
                view?.onNotEnoughPlayersError()
            }
        }
    }
}