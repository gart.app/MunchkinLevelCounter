package com.g_art.munchkinlevelcounter.view.stats

import com.g_art.munchkinlevelcounter.entity.ActionType
import com.g_art.munchkinlevelcounter.model.PlayerInGameActionsModel
import com.g_art.munchkinlevelcounter.mvp.BasePresenter
import com.g_art.munchkinlevelcounter.usecases.ActionsUseCase
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class StatsPresenter
@Inject constructor(private val useCase: ActionsUseCase) :
        BasePresenter<StatsContract.View>(), StatsContract.Presenter {

    private val LINE_WIDTH = 3f
    private val CUBIC_INTENSITY= 0.1f

    private var cachedGameId: Long = -1L
    private var cachedRecords: Map<ActionType, List<ILineDataSet>> = HashMap()

    override fun cacheStatsForGame(gameId: Long) {
        if (cachedGameId == gameId && cachedRecords.isNotEmpty()) {
            view?.onStatLoadSuccess(cachedRecords)
        } else {
            disposables.add(useCase.getGameActions(gameId)
                    .subscribeOn(Schedulers.computation())
                    .observeOn(Schedulers.computation())
                    .map { dbStatsRecords ->
                        return@map mapRecords(dbStatsRecords)
                    }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                cachedGameId = gameId
                                cachedRecords = it!!
                            },
                            { view?.onStatLoadError(it) }
                    )
            )
        }
    }

    override fun cacheStatsForPlayer(gameId: Long, playerId: Long) {
        if (cachedGameId == gameId && cachedRecords.isNotEmpty()) {
            view?.onStatLoadSuccess(cachedRecords)
        } else {
            disposables.add(useCase.getGameActionsForPlayer(gameId, playerId)
                    .subscribeOn(Schedulers.computation())
                    .observeOn(Schedulers.computation())
                    .map { dbStatsRecords ->
                        return@map mapRecords(dbStatsRecords)
                    }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                cachedGameId = gameId
                                cachedRecords = it
                            },
                            { view?.onStatLoadError(it) }
                    )
            )
        }
    }

    override fun loadStatsForType(gameId: Long, statsType: ActionType, childView: StatsContract.ChildView) {
        if (!cachePresent(gameId, statsType)) {
            disposables.add(useCase.getGameActions(gameId)
                    .subscribeOn(Schedulers.computation())
                    .observeOn(Schedulers.computation())
                    .map { dbStatsRecords ->
                        return@map mapRecords(dbStatsRecords)
                    }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                cachedGameId = gameId
                                cachedRecords = it
                                childView.onStatLoadSuccess(cachedRecords[statsType])
                            },
                            { childView.onStatLoadError(it) }
                    )
            )
        } else {
            childView.onStatLoadSuccess(cachedRecords[statsType]!!)
        }

    }

    private fun cachePresent(gameId: Long, statsType: ActionType): Boolean {
        return cachedGameId == gameId && cachedRecords.isNotEmpty() && cachedRecords.containsKey(statsType)
    }

    private fun mapRecords(dbStatsRecords: List<PlayerInGameActionsModel>): HashMap<ActionType, ArrayList<ILineDataSet>> {
        val statsPerType: HashMap<ActionType, ArrayList<ILineDataSet>> = HashMap()

        dbStatsRecords.forEach { playerRecord ->
            playerRecord.playerActions.entries.map { entry ->
                val dataSets = if (statsPerType.containsKey(entry.key)) {
                    statsPerType[entry.key]!!
                } else {
                    ArrayList()
                }
                val playerEntries: ArrayList<Entry> = ArrayList()
                playerEntries.add(Entry(0f, 0f))
                entry.value
                        .sortedBy { action -> action.actionDate }
                        .forEachIndexed { index, data ->
                            playerEntries.add(Entry(1+index.toFloat(), data.amount.toFloat()))
                        }
                val set = LineDataSet(playerEntries, playerRecord.player.name)
                set.setDrawCircles(false)
                set.color = playerRecord.player.color
                set.lineWidth = LINE_WIDTH
                set.cubicIntensity = CUBIC_INTENSITY
                set.mode = LineDataSet.Mode.CUBIC_BEZIER
                set.setDrawValues(false)
                dataSets.add(set)
                statsPerType.put(entry.key, dataSets)
            }

        }
        return statsPerType
    }

}