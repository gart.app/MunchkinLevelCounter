package com.g_art.munchkinlevelcounter.view.player.details

import com.g_art.munchkinlevelcounter.di.PerScreen
import com.g_art.munchkinlevelcounter.repository.PlayerRepository
import com.g_art.munchkinlevelcounter.usecases.PlayerDetailsUseCase
import dagger.Module
import dagger.Provides

/**
 * Created by agulia on 3/28/18.
 */
@Module
class PlayerDetailsModule {

    @PerScreen
    @Provides
    fun providePlayerUseCase(playerRepository: PlayerRepository) = PlayerDetailsUseCase(playerRepository)

    @PerScreen
    @Provides
    fun providePresenter(playerUseCase: PlayerDetailsUseCase) = PlayerDetailsPresenter(playerUseCase)
}