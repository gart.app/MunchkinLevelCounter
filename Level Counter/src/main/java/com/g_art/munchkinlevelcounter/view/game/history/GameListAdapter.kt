package com.g_art.munchkinlevelcounter.view.game.history

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.g_art.munchkinlevelcounter.R
import com.g_art.munchkinlevelcounter.databinding.GameRowBinding
import com.g_art.munchkinlevelcounter.model.GameModel
import java.text.DateFormat
import java.text.SimpleDateFormat

class GameListAdapter(val onGameClick: (Long, Int) -> Unit) : RecyclerView.Adapter<GameListAdapter.ViewHolder>()  {

    private var games = ArrayList<GameModel>()
    private val dateFormat: DateFormat = SimpleDateFormat.getDateInstance()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameListAdapter.ViewHolder {
        val binding = GameRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = games.size

    override fun onBindViewHolder(holder: GameListAdapter.ViewHolder, position: Int) {
        games[position].apply {

            holder.itemView.setOnClickListener { onGameClick(this.id, 1) }
            holder.removeGame.setOnClickListener { onGameClick(this.id, 3) }

            holder.gameName.text = String.format(holder.gameName.context.resources.getString(R.string.game_name_holder), this.id)
            this.endDate?.let {
                holder.gameDate.text = dateFormat.format(it)
            }
        }
    }

    fun fullGamesUpdate(newGames: List<GameModel>) {
        this.games.clear()
        this.games.addAll(newGames)
        notifyDataSetChanged()
    }

    fun removeGame(position: Int) {
        games.removeAt(position)
        notifyItemRemoved(position)
    }

    class ViewHolder(binding: GameRowBinding) : RecyclerView.ViewHolder(binding.root) {
        val gameName = binding.txtGameName
        val gameDate = binding.gameDate
        val removeGame = binding.imRemoveGame
    }
}