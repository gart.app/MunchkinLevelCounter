package com.g_art.munchkinlevelcounter.view.party.list

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.OnClick
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler
import com.g_art.munchkinlevelcounter.LCApplication
import com.g_art.munchkinlevelcounter.R
import com.g_art.munchkinlevelcounter.ext.gone
import com.g_art.munchkinlevelcounter.ext.initRecyclerView
import com.g_art.munchkinlevelcounter.ext.visible
import com.g_art.munchkinlevelcounter.model.PartyModel
import com.g_art.munchkinlevelcounter.mvp.BaseView
import com.g_art.munchkinlevelcounter.mvp.MVPPresenter
import com.g_art.munchkinlevelcounter.util.*
import com.g_art.munchkinlevelcounter.view.game.active.master.MasterGameView
import com.g_art.munchkinlevelcounter.view.party.details.PartyDetailsView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.analytics.FirebaseAnalytics
import javax.inject.Inject

/**
 * Created by agulia on 3/12/18.
 */
class PartyListView : BaseView(), PartyListContract.View {

    @Inject
    lateinit var presenter: PartyListPresenter

    @BindView(R.id.rv_party_list)
    lateinit var recyclerView: RecyclerView

    @BindView(R.id.fab_add_party)
    lateinit var addPartyFab: FloatingActionButton

    private val clickListener: (Long, Int) -> Unit = this::onPartyClicked

    private val partyAdapter = PartyListAdapter(clickListener)

    override fun onAttach(view: View) {
        super.onAttach(view)
        recyclerView.initRecyclerView(LinearLayoutManager(view.context), partyAdapter)
        with(presenter) {
            start(this@PartyListView)
            loadParties()
        }

        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, PARTY_LIST)
        logEvent(VIEW_OPEN, bundle)

    }

    override fun makeButtonsVisible() {
        addPartyFab.visible()
    }
    override fun makeButtonsGone() {
        addPartyFab.gone()
    }

    private fun onPartyClicked(partyId: Long, clickType: Int) {
        //Depending on click type invoke proper method (ugly solution)
        when(clickType) {
            1 -> onPartySelectClick(partyId)
            2 -> onPartyEditClick(partyId)
            3 -> onPartyRemoveClick(partyId)
        }
    }

    override fun onLoadPartiesSuccess(parties: List<PartyModel>) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, PARTY_DATA)
        bundle.putInt(FirebaseAnalytics.Param.VALUE, parties.size)
        logEvent(DATA_LOADED, bundle)

        partyAdapter.fullPartiesUpdate(parties)
    }

    override fun onLoadPartiesError(throwable: Throwable) {
        showMessage(R.string.error_parties_loading)
    }

    @OnClick(R.id.fab_add_party)
    override fun onCreatePartyClicked() {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, CREATE_PARTY)
        logEvent(BTN_CLICK, bundle)

        router.pushController(RouterTransaction.with(PartyDetailsView())
                .pushChangeHandler(FadeChangeHandler())
                .popChangeHandler(FadeChangeHandler()))
    }

    private fun onPartyEditClick(partyId: Long) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, EDIT_PARTY)
        logEvent(BTN_CLICK, bundle)

        val partyDetailsView = PartyDetailsView().apply {
            args.putLong(PARTY_ID, partyId)
        }
        router.pushController(RouterTransaction.with(partyDetailsView)
                .pushChangeHandler(FadeChangeHandler())
                .popChangeHandler(FadeChangeHandler()))
    }

    private fun onPartyRemoveClick(partyId: Long) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, REMOVE_PARTY)
        logEvent(BTN_CLICK, bundle)

        presenter.removePartyById(partyId)
    }

    private fun onPartySelectClick(partyId: Long) {
        presenter.startTheGame( partyId)
    }

    override fun onDeletePartySuccess(position: Int) {
        partyAdapter.removeParty(position)
    }

    override fun onDeletePartyError(throwable: Throwable) {
        showMessage(R.string.error_party_delete)
    }

    override fun onStartTheGameSuccess(gameId: Long) {
        startGame(gameId)
    }

    private fun startGame(gameId: Long) {
        val masterGame = MasterGameView().apply {
            args.putLong(GAME_ID, gameId)
            args.putString(TAG_KEY, gameId.toString())
        }
        router.pushController(RouterTransaction.with(masterGame)
                .pushChangeHandler(FadeChangeHandler())
                .popChangeHandler(FadeChangeHandler())
                .tag(gameId.toString()))
    }

    override fun onStartTheGameError(throwable: Throwable) {
        showMessage(R.string.error_game_start)
    }

    override fun getLayoutId() = R.layout.party_list_screen

    override fun getToolbarTitleId() = R.string.party_list_title

    override fun getPresenter(): MVPPresenter = presenter

    override fun injectDependencies() {
        DaggerPartyListComponent.builder()
                .lCAppComponent(LCApplication.component)
                .partyListModule(PartyListModule())
                .build()
                .inject(this)
    }
}