package com.g_art.munchkinlevelcounter.view.party.details

import com.g_art.munchkinlevelcounter.model.PartyAndPlayersModel
import com.g_art.munchkinlevelcounter.view.helper.move.ItemTouchHelperViewCallback

/**
 * Created by agulia on 4/3/18.
 */
interface PartyDetailsContract {

    interface View : ItemTouchHelperViewCallback {
        fun onLoadPartySuccess(party: PartyAndPlayersModel)
        fun onLoadPartyError(throwable: Throwable)

        fun onSavePartySuccess()
        fun onPartyValidationFailed(throwable: Throwable)
        fun onNotEnoughPlayersError()
        fun onSavePartyError(throwable: Throwable)
        fun onSavePartyAndOpenPlayersSuccess(partyId: Long)

        fun onReorderPlayerSuccess()
        fun onReorderPlayerError(throwable: Throwable)

        fun onDeletePartySuccess()
        fun onDeletePartyError(throwable: Throwable)
        fun onRemovePlayerSuccess(position: Int)
        fun onRemovePlayerError(it: Throwable)

        fun onHandleBack()

        fun onError()
    }

    interface Presenter {
        fun getPartyAndPlayersById(id: Long)

        fun removePlayerFromParty(playerId: Long, position: Int)

        fun saveParty()

        fun savePartyAndOpenPlayersView()

        fun savePartyAndOpenPlayersView(partyName: String)

        fun deleteParty()

        fun changePartyName(name: String)

        fun switchPlayersOrder(fromPosition: Int, toPosition: Int)

        fun savePlayersOrder()

        fun handleBack()
    }
}