package com.g_art.munchkinlevelcounter.view.start

import android.content.Intent
import android.net.Uri
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.RadioGroup
import android.os.Bundle
import butterknife.OnClick
import com.afollestad.materialdialogs.MaterialDialog
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler
import com.g_art.munchkinlevelcounter.LCApplication
import com.g_art.munchkinlevelcounter.R
import com.g_art.munchkinlevelcounter.R.id.*
import com.g_art.munchkinlevelcounter.mvp.BaseView
import com.g_art.munchkinlevelcounter.mvp.MVPPresenter
import com.g_art.munchkinlevelcounter.util.*
import com.g_art.munchkinlevelcounter.view.WebViewActivity
import com.g_art.munchkinlevelcounter.view.about.AboutView
import com.g_art.munchkinlevelcounter.view.game.active.master.MasterGameView
import com.g_art.munchkinlevelcounter.view.game.active.single.SingleGameView
import com.g_art.munchkinlevelcounter.view.game.history.GameListView
import com.g_art.munchkinlevelcounter.view.party.list.PartyListView
import com.g_art.munchkinlevelcounter.view.player.list.PlayerListView
import com.g_art.munchkinlevelcounter.view.settings.PreferencesActivity
import com.google.firebase.analytics.FirebaseAnalytics
import javax.inject.Inject

/**
 * Created by agulia on 3/3/18.
 */
class StartView : BaseView(), StartContract.View {
    private val PAID_URI = "https://play.google.com/store/apps/details?id=com.g_art.munchkinlevelcounter.paid"

    @Inject
    lateinit var presenter: StartPresenter

    override fun onAttach(view: View) {
        super.onAttach(view)

        if (shouldShowAboutInfo()) {
            showAboutInfo()
        } else {
            requestUnfinishedGame()
        }
    }

    private fun requestUnfinishedGame() {
        if (!getStartActivity().wasGameShown()) {
            with(presenter) {
                start(this@StartView)
                presenter.checkUnfinishedGame()
            }
        }
    }

    private fun shouldShowAboutInfo(): Boolean {
        return !getStartActivity().wasInfoShown() && getGameState() != AppType.PAID
    }

    private fun showAboutInfo() {
        activity?.layoutInflater?.inflate(R.layout.info_screen, null)?.let {custom->
            val paidBtn = custom.findViewById<Button>(R.id.btn_paid)
            val policyBtn = custom.findViewById<Button>(R.id.p_policy)
            val radioGroup = custom.findViewById<RadioGroup>(R.id.rg_versions)
            radioGroup?.check(rb_ads_v)

            val dialog = MaterialDialog.Builder(getStartActivity())
                    .customView(custom, true)
                    .title(R.string.about_info_title)
                    .positiveText(R.string.info_select)
                    .onPositive { _, _ ->
                        saveVersion(radioGroup?.checkedRadioButtonId)
                    }
                    .autoDismiss(true)
                    .canceledOnTouchOutside(false)
                    .build()

            paidBtn?.setOnClickListener {
                dialog.dismiss()
                openPlayStoreForPaidVersion()
            }
            policyBtn?.setOnClickListener {
                dialog.dismiss()
                openPrivacyPolicyView()
            }
            dialog.show()
        }
    }

    private fun saveVersion(checkedRadioButtonId: Int?) {
        when (checkedRadioButtonId) {
            R.id.rb_ads_free_v ->
                getStartActivity().saveVersion(false)
            R.id.rb_ads_v ->
                getStartActivity().saveVersion(true)
            else -> showMessage(R.string.info_option_is_required)
        }
    }

    private fun openPlayStoreForPaidVersion() {
        try {
            val intent = Intent(Intent.ACTION_VIEW).apply {
                data = Uri.parse(
                        PAID_URI)
                setPackage("com.android.vending")
            }
            startActivity(intent)
        } catch (exc: android.content.ActivityNotFoundException) {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(PAID_URI)))
        }
    }

    private fun openPrivacyPolicyView() {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, POLICY_CLICK)
        logEvent(BTN_CLICK, bundle)
        val intent = Intent(activity, WebViewActivity::class.java)
        startActivity(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.start_screen_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            action_settings -> onSettingsClick()
            action_history -> onGameHistoryClick()
            action_about -> onAboutClick()
        }
        return true
    }

    @OnClick(R.id.imb_single_mode, R.id.btn_single_mode)
    override fun onLoneWolfClick() {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, LONE_WOLF_CLICK)
        logEvent(BTN_CLICK, bundle)

        router.pushController(RouterTransaction.with(PlayerListView())
                .pushChangeHandler(FadeChangeHandler())
                .popChangeHandler(FadeChangeHandler()))
    }

    @OnClick(R.id.imb_game_master_mode, R.id.btn_game_master_mode)
    override fun onGameModeClick() {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, GAME_MASTER_CLICK)
        logEvent(BTN_CLICK, bundle)

        router.pushController(RouterTransaction.with(PartyListView())
                .pushChangeHandler(FadeChangeHandler())
                .popChangeHandler(FadeChangeHandler()))
    }

    override fun onSettingsClick() {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, SETTINGS_CLICK)
        logEvent(BTN_CLICK, bundle)
        val intent = Intent(activity, PreferencesActivity::class.java)
        startActivity(intent)
    }

    override fun onGameHistoryClick() {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, GAME_HISTORY_CLICK)
        logEvent(BTN_CLICK, bundle)

        router.pushController(RouterTransaction.with(GameListView())
                .pushChangeHandler(FadeChangeHandler())
                .popChangeHandler(FadeChangeHandler()))
    }

    override fun onAboutClick() {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, ABOUT_CLICK)
        logEvent(BTN_CLICK, bundle)

        router.pushController(RouterTransaction.with(AboutView())
                .pushChangeHandler(FadeChangeHandler())
                .popChangeHandler(FadeChangeHandler()))
    }

    override fun showUnfinishedGameMessage(gameId: Long, single: Boolean) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, UNFINISHED_GAME)
        logEvent(INFO_DIALOG, bundle)

        MaterialDialog.Builder(getStartActivity())
                .title(R.string.game_play_title)
                .content(R.string.unfinished_game)
                .positiveText(R.string.unfinished_game_continue)
                .onPositive { _, _ ->
                    continueGame(gameId, single)
                }
                .negativeText(R.string.unfinished_game_finish)
                .onNegative { _, _ ->
                    finishGame(gameId)
                }
                .canceledOnTouchOutside(false)
                .show()

        getStartActivity().gameShown()
    }

    private fun continueGame(gameId: Long, single: Boolean) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, CONTINUE_CLICK)
        logEvent(UNFINISHED_GAME, bundle)

        if (single) {
            val singleGameView = SingleGameView().apply {
                args.putLong(GAME_ID, gameId)
                args.putString(TAG_KEY, gameId.toString())
            }
            router.pushController(RouterTransaction.with(singleGameView)
                    .pushChangeHandler(FadeChangeHandler())
                    .popChangeHandler(FadeChangeHandler())
                    .tag(gameId.toString()))
        } else {
            val masterGameView = MasterGameView().apply {
                args.putLong(GAME_ID, gameId)
                args.putString(TAG_KEY, gameId.toString())
            }
            router.pushController(RouterTransaction.with(masterGameView)
                    .pushChangeHandler(FadeChangeHandler())
                    .popChangeHandler(FadeChangeHandler())
                    .tag(gameId.toString()))
        }
    }

    private fun finishGame(gameId: Long) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, FINISH_CLICK)
        logEvent(UNFINISHED_GAME, bundle)

        presenter.finishTheGame(gameId)
    }

    override fun onError(throwable: Throwable) {
        showMessage(R.string.error_load_last_game)
    }

    override fun getLayoutId() = R.layout.main_screen

    override fun getToolbarTitleId() = R.string.app_name

    override fun getPresenter(): MVPPresenter = presenter

    override fun injectDependencies() {
        DaggerStartComponent.builder()
                .lCAppComponent(LCApplication.component)
                .startModule(StartModule())
                .build()
                .inject(this)
    }

}