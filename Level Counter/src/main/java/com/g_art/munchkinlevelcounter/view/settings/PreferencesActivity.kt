package com.g_art.munchkinlevelcounter.view.settings

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import com.g_art.munchkinlevelcounter.R
import io.github.inflationx.viewpump.ViewPumpContextWrapper


class PreferencesActivity: AppCompatPreferenceActivity(),
        SharedPreferences.OnSharedPreferenceChangeListener {
    val ads_enabled = "ads_enabled"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_prefs)
        setSupportActionBar(findViewById(R.id.prefs_toolbar))
        getSupportActionBar()?.apply {
            setDisplayHomeAsUpEnabled(true)
        }
        addPreferencesFromResource(R.xml.preferences)
    }

    override fun onResume() {
        super.onResume()
        // Set up a listener whenever a key changes
        preferenceScreen.sharedPreferences
                .registerOnSharedPreferenceChangeListener(this)
        val pref = findPreference(ads_enabled)
        if (pref != null) {
            val currValue = preferenceScreen.sharedPreferences.getBoolean(ads_enabled, false)
            val summaryID = if (currValue) {
                R.string.prefs_ads_enabled
            } else {
                R.string.prefs_ads_disabled
            }
            pref.setSummary(summaryID)
        }

    }

    override fun onPause() {
        super.onPause()
        // Unregister the listener whenever a key changes
        preferenceScreen.sharedPreferences
                .unregisterOnSharedPreferenceChangeListener(this)
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        sharedPreferences?.let { shPrefs ->
            val pref = findPreference(key)
            if (key!! == ads_enabled) {
                val currValue = shPrefs.getBoolean(key, false)
                val summaryID = if (currValue) {
                    R.string.prefs_ads_enabled
                } else {
                    R.string.prefs_ads_disabled
                }
                pref.setSummary(summaryID)
            }
        }
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase))
    }

}