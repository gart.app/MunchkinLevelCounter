package com.g_art.munchkinlevelcounter.view.player.details

import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.OvalShape
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageView
import butterknife.BindView
import butterknife.OnClick
import butterknife.OnEditorAction
import com.afollestad.materialdialogs.color.ColorChooserDialog
import com.g_art.munchkinlevelcounter.LCApplication
import com.g_art.munchkinlevelcounter.R
import com.g_art.munchkinlevelcounter.entity.Gender
import com.g_art.munchkinlevelcounter.model.PlayerModel
import com.g_art.munchkinlevelcounter.mvp.BaseView
import com.g_art.munchkinlevelcounter.mvp.MVPPresenter
import com.g_art.munchkinlevelcounter.util.GAME_HISTORY
import com.g_art.munchkinlevelcounter.util.PLAYER_DETAILS
import com.g_art.munchkinlevelcounter.util.VIEW_OPEN
import com.google.firebase.analytics.FirebaseAnalytics
import javax.inject.Inject

/**
 * Created by agulia on 3/13/18.
 */
class PlayerDetailsView : BaseView(), PlayerDetailsContract.View {

    @Inject
    lateinit var presenter: PlayerDetailsPresenter

    @BindView(R.id.ed_player_name)
    lateinit var playerName: EditText

    @BindView(R.id.single_player_color)
    lateinit var playerColor: View

    @BindView(R.id.imb_single_gender)
    lateinit var playerGender: ImageView

    private var iconColor = ShapeDrawable(OvalShape())

    override fun onAttach(view: View) {
        super.onAttach(view)

        iconColor.intrinsicHeight = 24
        iconColor.intrinsicWidth = 24
        val requestedId = args.getLong(PLAYER_ID)

        initExistingPlayer(requestedId)

        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, PLAYER_DETAILS)
        logEvent(VIEW_OPEN, bundle)
    }

    private fun initExistingPlayer(playerId: Long) {
        with(presenter) {
            start(this@PlayerDetailsView)
            getPlayerById(playerId)
        }
    }

    override fun onLoadPlayerSuccess(player: PlayerModel) {
        playerName.setText(player.name)

        when (player.gender) {
            Gender.MALE -> playerGender.setImageResource(R.drawable.ic_gender_man)
            Gender.FEMALE -> playerGender.setImageResource((R.drawable.ic_gender_woman))
        }

//        iconColor = ShapeDrawable(OvalShape())
//        iconColor.intrinsicHeight = 24
//        iconColor.intrinsicWidth = 24
        iconColor.paint.color = player.color
        playerColor.background = iconColor
        playerColor.invalidate()
    }

    override fun onLoadPlayerError(throwable: Throwable) {
        showMessage(R.string.error_player_load)
        router.popCurrentController()
    }

    override fun onSavePlayerSuccess() {
        router.popCurrentController()
    }

    override fun onPlayerValidationFailed(throwable: Throwable) {
        showMessage(R.string.error_player_validation)
    }

    override fun onSavePlayerError(throwable: Throwable) {
        showMessage(R.string.error_player_save)
    }

    override fun onDeletePlayerSuccess() {
        router.popCurrentController()
    }

    override fun onDeletePlayerError(throwable: Throwable) {
        showMessage(R.string.error_player_delete)
    }

    @OnEditorAction(R.id.ed_player_name)
    fun onEditorAction(code: Int): Boolean {
        if (code == EditorInfo.IME_ACTION_DONE) {
            this.view?.hideKeyboard()
            presenter.setName(playerName.text.toString())
            return true
        }
        return false
    }

    @OnClick(R.id.single_player_color)
    fun onPlayerColorClick() {
        getStartActivity().setViewForColorCallback(this@PlayerDetailsView)
        ColorChooserDialog.Builder(getStartActivity(), R.string.choose_player_color)
                .allowUserColorInputAlpha(false)
                .allowUserColorInput(false)
                .cancelButton(R.string.dialog_cancel_btn)
                .backButton(R.string.dialog_back_btn)
                .doneButton(R.string.dialog_ok_btn)
                .dynamicButtonColor(true)
                .show()
    }

    @OnClick(R.id.imb_single_gender)
    fun onGenderChangeClick() {
        presenter.toggleGender()
    }

    override fun onColorChoose(dialog: ColorChooserDialog, selectedColor: Int) {
        presenter.setColor(selectedColor)
    }

    @OnClick(R.id.fab_save_player)
    fun savePlayer() {
        hideKeyboardAndTakeName()
        with(presenter) {
            start(this@PlayerDetailsView)
            savePlayer()
        }
    }

    private fun hideKeyboardAndTakeName() {
        this.view?.hideKeyboard()
        presenter.setName(playerName.text.toString())
    }

    override fun getLayoutId() = R.layout.player_details_view

    override fun getToolbarTitleId() = R.string.title_player_view

    override fun injectDependencies() {
        DaggerPlayerDetailsComponent.builder()
                .lCAppComponent(LCApplication.component)
                .playerDetailsModule(PlayerDetailsModule())
                .build()
                .inject(this)
    }

    override fun getPresenter(): MVPPresenter = presenter
}