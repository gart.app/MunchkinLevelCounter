package com.g_art.munchkinlevelcounter.view.start

import com.g_art.munchkinlevelcounter.di.PerScreen
import com.g_art.munchkinlevelcounter.repository.GameRepository
import com.g_art.munchkinlevelcounter.repository.PlayerRepository
import com.g_art.munchkinlevelcounter.usecases.GameUseCase
import dagger.Module
import dagger.Provides

/**
 * Created by agulia on 3/19/18.
 */
@Module
class StartModule {

    @PerScreen
    @Provides
    fun provideUseCase(gameRepository: GameRepository, playerRepository: PlayerRepository) =
            GameUseCase(gameRepository, playerRepository)

    @PerScreen
    @Provides
    fun providePresenter(gameUseCase: GameUseCase) = StartPresenter(gameUseCase)
}