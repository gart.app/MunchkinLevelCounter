package com.g_art.munchkinlevelcounter.view.player.details

import com.g_art.munchkinlevelcounter.model.PlayerModel

/**
 * Created by agulia on 3/28/18.
 */
interface PlayerDetailsContract {
    interface View {
        fun onLoadPlayerSuccess(player: PlayerModel)
        fun onLoadPlayerError(throwable: Throwable)

        fun onSavePlayerSuccess()
        fun onPlayerValidationFailed(throwable: Throwable)
        fun onSavePlayerError(throwable: Throwable)

        fun onDeletePlayerSuccess()
        fun onDeletePlayerError(throwable: Throwable)
    }

    interface Presenter {
        fun getPlayerById(id: Long)
        fun toggleGender()
        fun setColor(newColor: Int)

        fun setName(name: String)

        fun savePlayer()

        fun deletePlayer()
    }
}