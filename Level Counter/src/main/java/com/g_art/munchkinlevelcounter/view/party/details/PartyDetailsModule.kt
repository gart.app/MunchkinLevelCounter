package com.g_art.munchkinlevelcounter.view.party.details

import com.g_art.munchkinlevelcounter.di.PerScreen
import com.g_art.munchkinlevelcounter.repository.PartyRepository
import com.g_art.munchkinlevelcounter.repository.PlayerRepository
import com.g_art.munchkinlevelcounter.usecases.PartyDetailsUseCase
import dagger.Module
import dagger.Provides

/**
 * Created by agulia on 4/3/18.
 */
@Module
class PartyDetailsModule {

    @PerScreen
    @Provides
    fun providePartyUseCase(partyRepository: PartyRepository,
                            playerRepository: PlayerRepository) =
            PartyDetailsUseCase(partyRepository, playerRepository)

    @PerScreen
    @Provides
    fun providePresenter(partyDetailsUseCase: PartyDetailsUseCase) = PartyDetailsPresenter(partyDetailsUseCase)
}