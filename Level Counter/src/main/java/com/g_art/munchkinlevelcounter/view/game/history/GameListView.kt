package com.g_art.munchkinlevelcounter.view.game.history

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import butterknife.BindView
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler
import com.g_art.munchkinlevelcounter.LCApplication
import com.g_art.munchkinlevelcounter.R
import com.g_art.munchkinlevelcounter.ext.initRecyclerView
import com.g_art.munchkinlevelcounter.model.GameModel
import com.g_art.munchkinlevelcounter.mvp.BaseView
import com.g_art.munchkinlevelcounter.mvp.MVPPresenter
import com.g_art.munchkinlevelcounter.util.*
import com.g_art.munchkinlevelcounter.view.game.details.GameDetailsView
import com.google.firebase.analytics.FirebaseAnalytics
import javax.inject.Inject

/**
 * Created by agulia on 3/12/18.
 */
class GameListView: BaseView(), GameListContract.View {

    @Inject
    lateinit var presenter: GameListPresenter

    @BindView(R.id.rv_games_list)
    lateinit var recyclerView: RecyclerView

    private val clickListener: (Long, Int) -> Unit = this::onGameClicked

    private val gameAdapter = GameListAdapter(clickListener)

    override fun onAttach(view: View) {
        super.onAttach(view)
        recyclerView.initRecyclerView(LinearLayoutManager(view.context), gameAdapter)
        with(presenter) {
            start(this@GameListView)
            loadGames()
        }
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, GAME_HISTORY)
        logEvent(VIEW_OPEN, bundle)
    }

    private fun onGameClicked(gameId: Long, clickType: Int) {
        when(clickType) {
            1 -> onGameSelectClick(gameId)
            3 -> onGameRemoveClick(gameId)
        }
    }

    override fun onLoadGamesSuccess(games: List<GameModel>) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, GAME_DATA)
        bundle.putInt(FirebaseAnalytics.Param.VALUE, games.size)
        logEvent(DATA_LOADED, bundle)

        gameAdapter.fullGamesUpdate(games)
    }

    override fun onLoadGamesError(throwable: Throwable) {
        showMessage(R.string.error_game_loading)
    }

    override fun onGameRemoveSuccess(position: Int) {
        gameAdapter.removeGame(position)
    }

    override fun onGameRemoveError(throwable: Throwable) {
        showMessage(R.string.error_generic)
    }

    private fun onGameRemoveClick(gameId: Long) {
        presenter.removeGameById(gameId = gameId)
    }

    private fun onGameSelectClick(gameId: Long) {
        val detailsView = GameDetailsView().apply {
            args.putLong(GAME_ID, gameId)
        }

        router.pushController(RouterTransaction.with(detailsView)
                .pushChangeHandler(FadeChangeHandler())
                .popChangeHandler(FadeChangeHandler()))
    }

    override fun getLayoutId(): Int = R.layout.game_history

    override fun getToolbarTitleId(): Int = R.string.game_history_title

    override fun injectDependencies() {
        DaggerGameListComponent.builder()
                .lCAppComponent(LCApplication.component)
                .gameListModule(GameListModule())
                .build()
                .inject(this)
    }

    override fun getPresenter(): MVPPresenter = presenter
}