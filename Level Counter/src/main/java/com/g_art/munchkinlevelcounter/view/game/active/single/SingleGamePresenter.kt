package com.g_art.munchkinlevelcounter.view.game.active.single

import com.g_art.munchkinlevelcounter.model.ActivePlayerAndGameModel
import com.g_art.munchkinlevelcounter.model.ActivePlayerModel
import com.g_art.munchkinlevelcounter.mvp.BasePresenter
import com.g_art.munchkinlevelcounter.usecases.GameUseCase
import com.g_art.munchkinlevelcounter.view.game.active.ActiveGameContract
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class SingleGamePresenter
    @Inject constructor(private val useCase: GameUseCase):
        BasePresenter<ActiveGameContract.SinglePlayerView>(),
        ActiveGameContract.PlayerGamePresenter {

    private var playerAndGame: ActivePlayerAndGameModel? = null
    private var continueAfterWin: Boolean = false

    override fun loadGame(gameId: Long) {
        disposables.add(useCase.loadActivePlayerAndGame(gameId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( {
                    playerAndGame = it
                    playerAndGame?.let { gameModel ->
                        view?.onLoadGameSuccess(gameModel.game)
                        view?.onPlayerUpdateSuccess(gameModel.activePlayer)
                    }
                }, { view?.onLoadGameError(it) } )
        )
    }

    override fun lvlUp() {
        playerAndGame?.let {
            disposables.add(useCase.lvlUpPlayer(it.activePlayer)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                val maxGameLevel = it.game.maxLevel
                                if (it.activePlayer.playerInGame.level >= maxGameLevel && !continueAfterWin) {
                                    continueAfterWin = true
                                    view?.onPlayerWinTheGame(it.activePlayer)
                                } else {
                                    view?.onStatsChangeSuccess(it.activePlayer.playerInGame.level,
                                            it.activePlayer.playerInGame.gear)
                                }
                            },
                            { error->onPlayerUpdateError(error) } )
            )
        }
    }

    override fun lvlDwn() {
        playerAndGame?.let {
            disposables.add(useCase.lvlDwnPlayer(it.activePlayer)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                view?.onStatsChangeSuccess(it.activePlayer.playerInGame.level,
                                        it.activePlayer.playerInGame.gear)
                            },
                            { error->onPlayerUpdateError(error) } )
            )
        }
    }

    override fun gearUp(amount: Int) {
        playerAndGame?.let {
            disposables.add(useCase.gearUpPlayer(it.activePlayer, amount)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { view?.onPlayerUpdateSuccess(it.activePlayer) },
                            { error -> onPlayerUpdateError(error) } )
            )
        }
    }

    override fun gearDwn(amount: Int) {
        playerAndGame?.let {
            disposables.add(useCase.gearDwnPlayer(it.activePlayer, amount)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { view?.onPlayerUpdateSuccess(it.activePlayer) },
                            { error->onPlayerUpdateError(error) } )
            )
        }
    }

    override fun toggleGender() {
        playerAndGame?.let {
            disposables.add(useCase.toggleGender(it.activePlayer)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { view?.onToggleGenderSuccess(it.activePlayer) },
                            { error-> onPlayerUpdateError(error) } )
            )
        }
    }

    override fun changeColor(selectedColor: Int) {
        playerAndGame?.let {
            disposables.add(useCase.changeColor(selectedColor, it.activePlayer)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { view?.onColorChangeSuccess(selectedColor) },
                            { error -> onPlayerUpdateError(error) } )
            )
        }
    }

    override fun markTheWinner(player: ActivePlayerModel) {
        disposables.add(
                useCase.markTheWinner(player)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                {  },
                                { onPlayerUpdateError(it) }
                        )
        )
    }

    override fun setGameMaxLevel(maxLevel: Int) {
        playerAndGame?.let {
            disposables.add(
                    useCase.setGameMaxLevel(it.game, maxLevel)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    {
                                        continueAfterWin = false
                                        view?.onUpdateGameSuccess(it.game)
                                    },
                                    {error ->
                                        continueAfterWin = false
                                        view?.onUpdateGameError(error)

                                    }
                            )
            )
        }
    }

    override fun editPlayers() {
        playerAndGame?.let {
            view?.editPlayers(playerAndGame!!.game.id, -1)
        }
    }

    override fun finishTheGame() {
        playerAndGame?.let {
            disposables.add(
                    useCase.finishTheGame(it.game.id)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    {},
                                    { error -> view?.onFinishGameError(error) }
                            )
            )
        }
    }

    override fun finishTheGameAndOpenStats() {
        playerAndGame?.let {
            disposables.add(
                    useCase.finishTheGame(it.game.id)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    {
                                        view?.openStatsView(it.game.id)
                                    },
                                    { error-> view?.onFinishGameError(error) }
                            )
            )
        }
    }

    override fun finishTheGame(activePlayer: ActivePlayerModel) {
        playerAndGame?.let {
            disposables.add(
                    useCase.finishTheGame(activePlayer, it.game.id)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    {
                                        view?.openStatsView(it.game.id)
                                    },
                                    {error->
                                        view?.onFinishGameError(error)
                                    }
                            )
            )
        }
    }

    override fun openGameMaxLevelChange() {
        playerAndGame?.let {
            view?.showMaxLevelChangeDialog(it.game.maxLevel)
        }
    }

    override fun startBattle() {
        playerAndGame?.let {
            view?.startBattleForPlayer(it.game.id, it.activePlayer.player.id)
        }
    }

    private fun onPlayerUpdateError(throwable: Throwable) {
        when (throwable) {
            is IllegalArgumentException -> view?.onPlayerUpdateFailed(throwable)
            else -> view?.onPlayerUpdateError(throwable)
        }
    }
}