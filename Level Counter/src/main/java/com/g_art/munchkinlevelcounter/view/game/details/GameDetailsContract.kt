package com.g_art.munchkinlevelcounter.view.game.details

import com.g_art.munchkinlevelcounter.model.ActivePlayerModel
import com.g_art.munchkinlevelcounter.model.GameModel
import com.g_art.munchkinlevelcounter.mvp.MPVView

interface GameDetailsContract {

    interface View : MPVView {
        fun onLoadGameSuccess(game: GameModel, players: List<ActivePlayerModel>)
        fun onLoadGameError(throwable: Throwable)
        fun onStartGameError(throwable: Throwable)

        fun openTheGame(gameId: Long, isSingleGame: Boolean)
        fun openStats(gameId: Long)
    }

    interface Presenter {
        fun loadGame(gameId: Long)
        fun openStats()

        fun replayTheGame()
        fun continueTheGame()
    }
}