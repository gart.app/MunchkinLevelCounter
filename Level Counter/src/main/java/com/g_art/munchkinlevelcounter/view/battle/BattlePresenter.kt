package com.g_art.munchkinlevelcounter.view.battle

import com.g_art.munchkinlevelcounter.model.ActivePlayerModel
import com.g_art.munchkinlevelcounter.model.GameModel
import com.g_art.munchkinlevelcounter.model.MonsterModel
import com.g_art.munchkinlevelcounter.model.PlayersAndGameModel
import com.g_art.munchkinlevelcounter.mvp.BasePresenter
import com.g_art.munchkinlevelcounter.usecases.GameUseCase
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class BattlePresenter
@Inject constructor(private val useCase: GameUseCase) :
        BasePresenter<BattleContract.View>(),
        BattleContract.Presenter {

    private var gameModel: GameModel? = null
    private var activePlayer: ActivePlayerModel? = null
    private var playersAndGame: PlayersAndGameModel? = null
    private var monster: MonsterModel? = MonsterModel(gameId = 0, level = 1, mods = 0, treasures = 0)
    private var companions: ArrayList<ActivePlayerModel>? = null
    private var companion: ActivePlayerModel? = null
    private var companionPosition: Int? = null

    override fun loadCompanions() {
        companions?.let {
            view?.displayCompanions(it, companionPosition)
        }
    }

    override fun selectCompanion(positionId: Int) {
        if (positionId != -1) {
            companionPosition = positionId
            companion = companions?.get(positionId)
            companion?.let { playerModel ->
                companionPosition?.let {
                    view?.onSelectedCompanion(playerModel, it)
                    updatePlayerPower()
                }
            }
        } else {
            companion = null
            companionPosition = null
            view?.onRemoveCompanion()
            updatePlayerPower()
        }
    }

    override fun loadBattleGame(gameId: Long, attackerId: Long) {
        disposables.add(useCase.loadGameAndPlayers(gameId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { playersAndGameModel ->
                            playersAndGame = playersAndGameModel
                            companions = ArrayList()

                            playersAndGame?.players?.forEach { player ->
                                if (player.player.id == attackerId) {
                                    activePlayer = player
                                } else {
                                    companions?.add(player)
                                }
                            }
                            gameModel = playersAndGame?.gameModel
                            gameModel?.let { game->
                                monster?.let { it.gameId = game.id }
                            }

                            playersAndGame?.let {
                                view?.onLoadGameSuccess(it.gameModel)
                            }
                            activePlayer?.let {
                                view?.onPlayerUpdateSuccess(it)
                                updatePlayerPower()
                            }
                            monster?.let {
                                view?.onMonsterUpdateSuccess(it)
                            }

                            if (companions == null || companions!!.isEmpty()) {
                                view?.disableCompanionBtn()
                            }
                        },
                        { })

        )
    }

    override fun lvlUp(isPlayer: Boolean) {
        activePlayer?.let { player ->
            gameModel?.let { game ->
                disposables.add(
                        if (isPlayer) {
                            useCase.lvlUpPlayer(player, game.maxLevel)
                        } else {
                            Completable.fromAction {
                                monster?.let { it.gameId = game.id }
                                monster?.let { useCase.lvlUpMonster(it) }
                            }
                        }
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(
                                        {
                                            if (isPlayer) {
                                                view?.onPlayerUpdateSuccess(player)
                                                updatePlayerPower()
                                            } else {
                                                monster?.let {
                                                    view?.onMonsterUpdateSuccess(it)
                                                }
                                            }
                                        },
                                        {
                                            if (isPlayer) {
                                                view?.onPlayerUpdateError(it)
                                            }
                                        })
                )
            }

        }
    }

    override fun lvlDwn(isPlayer: Boolean) {
        activePlayer?.let { player ->
            gameModel?.let { game ->
                disposables.add(
                        if (isPlayer) {
                            useCase.lvlDwnPlayer(player)
                        } else {
                            Completable.fromAction {
                                monster?.let { it.gameId = game.id }
                                monster?.let { useCase.lvlDwnMonster(it) }
                            }
                        }

                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(
                                        {
                                            if (isPlayer) {
                                                view?.onPlayerUpdateSuccess(player)
                                                updatePlayerPower()
                                            } else {
                                                view?.onMonsterUpdateSuccess(monster!!)
                                            }
                                        },
                                        {
                                            if (isPlayer) {
                                                view?.onPlayerUpdateError(it)
                                            }
                                        })
                )
            }
        }
    }

    override fun gearUp(amount: Int) {
        activePlayer?.let {player->
            disposables.add(
                    useCase.gearUpPlayer(player, 1)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    {
                                        view?.onPlayerUpdateSuccess(player)
                                        updatePlayerPower()
                                    },
                                    {
                                        view?.onPlayerUpdateError(it)
                                    })
            )

        }
    }

    override fun gearDwn(amount: Int) {
        activePlayer?.let {
            disposables.add(
                    useCase.gearDwnPlayer(it, 1)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    {
                                        view?.onPlayerUpdateSuccess(it)
                                        updatePlayerPower()
                                    },
                                    {error->
                                        view?.onPlayerUpdateError(error)
                                    })
            )
        }
    }

    override fun modifierUp(amount: Int, isPlayer: Boolean) {
        activePlayer?.let {player->
            gameModel?.let {game->
                disposables.add(
                        if (isPlayer) {
                            useCase.modUpPlayer(player)
                        } else {
                            Completable.fromAction {
                                monster?.let { it.gameId = game.id }
                                monster?.let { useCase.modUpMonster(it) }
                            }
                        }

                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(
                                        {
                                            if (isPlayer) {
                                                view?.onPlayerUpdateSuccess(player)
                                                updatePlayerPower()
                                            } else {
                                                monster?.let {
                                                    view?.onMonsterUpdateSuccess(monster!!)
                                                }
                                            }
                                        },
                                        {
                                            if (isPlayer) {
                                                view?.onPlayerUpdateError(it)
                                            }
                                        })
                )
            }
        }
    }

    override fun modifierDwn(amount: Int, isPlayer: Boolean) {
        gameModel?.let {game->
            activePlayer?.let {player ->
                disposables.add(
                        if (isPlayer) {
                            useCase.modDwnPlayer(player)
                        } else {
                            Completable.fromAction {
                                monster?.let { it.gameId = game.id }
                                monster?.let { useCase.modDwnMonster(monster!!) }
                            }
                        }

                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(
                                        {
                                            if (isPlayer) {
                                                view?.onPlayerUpdateSuccess(player)
                                                updatePlayerPower()
                                            } else {
                                                monster?.let { view?.onMonsterUpdateSuccess(it) }
                                            }
                                        },
                                        {
                                            if (isPlayer) {
                                                view?.onPlayerUpdateError(it)
                                            }
                                        })
                )
            }
        }
    }

    override fun treasuresUp() {
        gameModel?.let { gameModel->
            monster?.let { it.gameId = gameModel.id }
        }
        monster?.let {
            disposables.add(
                    useCase.treasuresUp(it)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({
                                view?.onMonsterUpdateSuccess(it)
                            }, {})
            )
        }
    }

    override fun treasuresDwn() {
        gameModel?.let { gameModel->
            monster?.let { it.gameId = gameModel.id }
        }
        monster?.let {
            disposables.add(
                    useCase.treasuresDwn(it)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe( {
                                view?.onMonsterUpdateSuccess(it)
                            }, {})
            )
        }
    }

    override fun resetMods() {
        activePlayer?.let {
            disposables.add(
                    useCase.modResetPlayer(it)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({ monster = MonsterModel(gameId = 0, level = 1, mods = 0, treasures = 0) }, {})
            )
        }
    }

    override fun toggleWarrior(checked: Boolean) {
        activePlayer?.let {
            disposables.add(
                    useCase.toggleWarrior(checked, it)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe( {
                                view?.onPlayerUpdateSuccess(it)
                                updatePlayerPower()
                            }, {})
            )
        }
    }

    override fun fight() {
        activePlayer?.let {player->
            gameModel?.let { gameModel->
                monster?.let { it.gameId = gameModel.id }
            }
            val mPower = monster?.getPower() ?: 0
            val pPower = player.playerInGame.getTotalPower()
                    .plus(companion?.playerInGame?.getTotalPower() ?: 0)

            //Save fight result to stats
            if (pPower > mPower || player.playerInGame.warrior && pPower == mPower) {
                monster?.let {
                    disposables.add(
                            useCase.monsterKilled(player, monster!!)
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe( {
                                        resetMods()
                                        view?.winBattleMessage()
                                    }, {})

                    )
                }
            } else {
                resetMods()
                view?.loseBattleMessage()
            }
        }
    }

    override fun run(diceResult: Int) {
        activePlayer?.let { player->
            disposables.add(
                    useCase.runAway(player, diceResult > 4)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe( {
                                resetMods()
                                view?.closeBattle()
                            }, {
                                view?.closeBattle()
                            } )

            )
        }
    }

    private fun updatePlayerPower() {
        activePlayer?.let {
            view?.onPowerUpdate(it.playerInGame.getTotalPower()
                            .plus(companion?.playerInGame?.getTotalPower() ?: 0)
            )
        }
    }

    override fun destroy() {
        monster = MonsterModel(gameId = 0, level = 1, mods = 0, treasures = 0)
        super.destroy()
    }
}