package com.g_art.munchkinlevelcounter.view.about

import com.g_art.munchkinlevelcounter.mvp.BasePresenter

class AboutPresenter :
        BasePresenter<AboutContract.View>(), AboutContract.Presenter  {
}