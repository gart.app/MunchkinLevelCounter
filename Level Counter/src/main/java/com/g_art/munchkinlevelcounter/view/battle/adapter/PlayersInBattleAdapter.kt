package com.g_art.munchkinlevelcounter.view.battle.adapter

import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.OvalShape
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.g_art.munchkinlevelcounter.R
import com.g_art.munchkinlevelcounter.databinding.InGamePlayersRow2Binding
import com.g_art.munchkinlevelcounter.model.ActivePlayerModel

class PlayersInBattleAdapter : RecyclerView.Adapter<PlayersInBattleAdapter.ViewHolder>() {
    private var players: List<ActivePlayerModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = InGamePlayersRow2Binding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount() = players.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        players[position].apply {
            holder.playerName.text = this.player.name
            val iconColor = ShapeDrawable(OvalShape())
            iconColor.intrinsicHeight = 24
            iconColor.intrinsicWidth = 24
            iconColor.paint.color = this.player.color
            holder.playerLevel.text = this.playerInGame.level.toString()
            holder.playerTotal.text = String.format(holder.itemView.context.resources.getString(R.string.total_value_container),
                    this.playerInGame.getTotalPower())
        }
    }

    fun updatePlayers(players: List<ActivePlayerModel>) {
        (this.players as ArrayList).clear()
        (this.players as ArrayList).addAll(players)
        notifyDataSetChanged()
    }

    class ViewHolder(view: InGamePlayersRow2Binding) : RecyclerView.ViewHolder(view.root) {
        val playerName = view.inGamePlayerName
        val playerLevel = view.inGamePlayerLvlValue
        val playerTotal = view.inGamePlayerTotalValue
    }
}