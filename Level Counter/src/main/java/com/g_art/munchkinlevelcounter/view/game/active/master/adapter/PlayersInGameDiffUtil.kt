package com.g_art.munchkinlevelcounter.view.game.active.master.adapter

import android.os.Bundle
import androidx.recyclerview.widget.DiffUtil
import com.g_art.munchkinlevelcounter.model.ActivePlayerModel
import com.g_art.munchkinlevelcounter.util.LCDiffUtil.Companion.HIGHLIGHT_KEY
import com.g_art.munchkinlevelcounter.util.LCDiffUtil.Companion.PLAYER_COLOR_KEY
import com.g_art.munchkinlevelcounter.util.LCDiffUtil.Companion.PLAYER_LEVEL_KEY
import com.g_art.munchkinlevelcounter.util.LCDiffUtil.Companion.PLAYER_NAME_KEY
import com.g_art.munchkinlevelcounter.util.LCDiffUtil.Companion.PLAYER_TOTAL_KEY

class PlayersInGameDiffUtil(
        var oldList: List<ActivePlayerModel>,
        var newList: List<ActivePlayerModel>) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].player.id == newList[newItemPosition].player.id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
//        return oldList[oldItemPosition].equals(newList[newItemPosition])
        return false
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        if (oldList.size <= oldItemPosition) {
            return null
        }

        if (newList.size <= newItemPosition) {
            return null
        }

        val oldPlayer = oldList[oldItemPosition]
        val newPlayer = newList[newItemPosition]
        val diffBundle = Bundle()

        if (oldPlayer.player.id != newPlayer.player.id) {
            return null
        }

        if (oldPlayer.player.name != newPlayer.player.name) {
            diffBundle.putString(PLAYER_NAME_KEY, newPlayer.player.name)
        }

        if (oldPlayer.player.color != newPlayer.player.color) {
            diffBundle.putInt(PLAYER_COLOR_KEY, newPlayer.player.color)
        }

        if (oldPlayer.playerInGame.level != newPlayer.playerInGame.level) {
            diffBundle.putInt(PLAYER_LEVEL_KEY, newPlayer.playerInGame.level)
        }

        if (oldPlayer.playerInGame.getTotalPower()
                != newPlayer.playerInGame.getTotalPower()) {
            diffBundle.putInt(PLAYER_TOTAL_KEY, newPlayer.playerInGame.getTotalPower())
        }

        diffBundle.putBoolean(HIGHLIGHT_KEY, newPlayer.player.selected)

        return diffBundle
    }
}