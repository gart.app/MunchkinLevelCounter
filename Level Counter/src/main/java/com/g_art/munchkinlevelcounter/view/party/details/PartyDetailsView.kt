package com.g_art.munchkinlevelcounter.view.party.details

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import butterknife.BindView
import butterknife.OnClick
import butterknife.OnEditorAction
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler
import com.g_art.munchkinlevelcounter.LCApplication
import com.g_art.munchkinlevelcounter.R
import com.g_art.munchkinlevelcounter.ext.initRecyclerView
import com.g_art.munchkinlevelcounter.model.PartyAndPlayersModel
import com.g_art.munchkinlevelcounter.mvp.BaseView
import com.g_art.munchkinlevelcounter.mvp.MVPPresenter
import com.g_art.munchkinlevelcounter.view.party.details.adapter.PlayersInPartyAdapter
import com.g_art.munchkinlevelcounter.view.player.list.PlayerListView
import javax.inject.Inject
import androidx.recyclerview.widget.ItemTouchHelper
import com.g_art.munchkinlevelcounter.util.GAME_HISTORY
import com.g_art.munchkinlevelcounter.util.PARTY_DETAILS
import com.g_art.munchkinlevelcounter.util.VIEW_OPEN
import com.g_art.munchkinlevelcounter.view.helper.move.SimpleItemTouchHelperCallback
import com.google.firebase.analytics.FirebaseAnalytics


/**
 * Created by agulia on 3/12/18.
 */
class PartyDetailsView: BaseView(), PartyDetailsContract.View {

    @Inject
    lateinit var presenter: PartyDetailsPresenter

    @BindView(R.id.et_party_name)
    lateinit var partyName: EditText

    @BindView(R.id.rv_players_in_party)
    lateinit var recyclerView: RecyclerView

    private val partyAdapter = PlayersInPartyAdapter()
    private val callback = SimpleItemTouchHelperCallback(this)
    private val touchHelper = ItemTouchHelper(callback)

    override fun onAttach(view: View) {
        super.onAttach(view)

        recyclerView.initRecyclerView(LinearLayoutManager(view.context), partyAdapter)
        touchHelper.attachToRecyclerView(recyclerView)

        val requestedId = args.getLong(PARTY_ID)
        with(presenter) {
            start(this@PartyDetailsView)
            presenter.getPartyAndPlayersById(requestedId)
        }

        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, PARTY_DETAILS)
        logEvent(VIEW_OPEN, bundle)
    }

    @OnClick(R.id.fab_save_party)
    fun onSaveClick() {
        onEditorAction(EditorInfo.IME_ACTION_DONE)
        presenter.saveParty()
    }

    @OnClick(R.id.fab_delete_party)
    fun onDeleteClick() {
        presenter.deleteParty()
    }

    @OnClick(R.id.fab_add_player_to_party)
    fun onAddPlayerClick() {
        this.view?.hideKeyboard()
        presenter.savePartyAndOpenPlayersView(partyName.text.toString())
    }

    @OnEditorAction(R.id.et_party_name)
    fun onEditorAction(code: Int): Boolean {
        if (code == EditorInfo.IME_ACTION_DONE) {
            this.view?.hideKeyboard()
            presenter.changePartyName(partyName.text.toString())
            return true
        }
        return false
    }

    override fun onItemMove(fromPosition: Int, toPosition: Int) {
        partyAdapter.onItemMove(fromPosition, toPosition)
        presenter.switchPlayersOrder(fromPosition, toPosition)
    }

    override fun onItemDismiss(position: Int) {
        val playerToRemove = partyAdapter.getPlayerAt(position)
        presenter.removePlayerFromParty(playerToRemove.id, position)
    }

    override fun onLoadPartySuccess(party: PartyAndPlayersModel) {
        partyName.setText(party.partyModel.name)
        partyAdapter.updatePlayers(party.players)
    }

    override fun onLoadPartyError(throwable: Throwable) {
        showMessage(R.string.error_party_load)
    }

    override fun onSavePartySuccess() {
        router.popCurrentController()
    }

    override fun onPartyValidationFailed(throwable: Throwable) {
        showMessage(R.string.error_party_validation)
    }

    override fun onNotEnoughPlayersError() {
        showMessage(R.string.empty_party_error)
    }

    override fun onSavePartyError(throwable: Throwable) {
        showMessage(R.string.error_party_save)
    }

    override fun onSavePartyAndOpenPlayersSuccess(partyId: Long) {
        val playerListView = PlayerListView().apply {
            args.putLong(ACTION_ID, FROM_THE_PARTY)
            args.putLong(PARTY_ID, partyId)
        }

        router.pushController(RouterTransaction.with(playerListView)
                .pushChangeHandler(FadeChangeHandler())
                .popChangeHandler(FadeChangeHandler()))
    }

    override fun onReorderPlayerSuccess() {
    }

    override fun onReorderPlayerError(throwable: Throwable) {
        showMessage(R.string.error_party_player_reordering)
    }

    override fun onDeletePartySuccess() {
        router.popCurrentController()
    }

    override fun onDeletePartyError(throwable: Throwable) {
        showMessage(R.string.error_party_delete)
    }

    override fun onRemovePlayerSuccess(position: Int) {
        partyAdapter.removePlayer(position)
    }

    override fun onRemovePlayerError(it: Throwable) {
        showMessage(R.string.error_party_player_removing)
    }

    override fun onError() {
        showMessage(R.string.error_party_load)
    }

    override fun handleBack(): Boolean {
        presenter.handleBack()
        return true
    }

    override fun onHandleBack() {
        router.popCurrentController()
    }

    override fun getLayoutId() = R.layout.party_details_screen

    override fun getToolbarTitleId() = R.string.title_view_party

    override fun injectDependencies() {
        DaggerPartyDetailsComponent.builder()
                .lCAppComponent(LCApplication.component)
                .partyDetailsModule(PartyDetailsModule())
                .build()
                .inject(this)
    }

    override fun getPresenter(): MVPPresenter = presenter
}