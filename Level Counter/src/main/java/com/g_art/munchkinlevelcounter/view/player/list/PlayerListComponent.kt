package com.g_art.munchkinlevelcounter.view.player.list

import com.g_art.munchkinlevelcounter.di.LCAppComponent
import com.g_art.munchkinlevelcounter.di.PerScreen
import dagger.Component

/**
 * Created by G_Artem on 3/24/18.
 */

@PerScreen
@Component(modules = [(PlayerListModule::class)],
        dependencies = [(LCAppComponent::class)])
interface PlayerListComponent {
    fun inject(view: PlayerListView)
}