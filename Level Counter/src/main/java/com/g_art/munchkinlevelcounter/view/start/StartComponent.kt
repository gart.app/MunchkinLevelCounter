package com.g_art.munchkinlevelcounter.view.start

import com.g_art.munchkinlevelcounter.di.LCAppComponent
import com.g_art.munchkinlevelcounter.di.PerScreen
import dagger.Component

/**
 * Created by agulia on 3/19/18.
 */

@PerScreen
@Component(modules = [(StartModule::class)],
        dependencies = [(LCAppComponent::class)])
interface StartComponent {
    fun inject(view: StartView)
}