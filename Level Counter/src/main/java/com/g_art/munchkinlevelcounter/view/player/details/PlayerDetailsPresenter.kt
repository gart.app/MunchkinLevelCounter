package com.g_art.munchkinlevelcounter.view.player.details

import com.g_art.munchkinlevelcounter.entity.Gender
import com.g_art.munchkinlevelcounter.model.PlayerModel
import com.g_art.munchkinlevelcounter.mvp.BasePresenter
import com.g_art.munchkinlevelcounter.usecases.PlayerDetailsUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

/**
 * Created by agulia on 3/13/18.
 */
class PlayerDetailsPresenter
@Inject constructor(private val useCase: PlayerDetailsUseCase) :
        BasePresenter<PlayerDetailsContract.View>(),
        PlayerDetailsContract.Presenter {

    private var player: PlayerModel? = null

    override fun getPlayerById(id: Long) {
        if (id == 0L) {
            player = PlayerModel()
            player?.let {
                it.generateAndSignColor()
                view?.onLoadPlayerSuccess(it)
            }
        } else {
            disposables.add(useCase.loadPlayerById(id).observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        player = it
                        view?.onLoadPlayerSuccess(it)
                    }, { view?.onLoadPlayerError(it) })
            )
        }

    }

    override fun setName(name: String) {
        player?.let {
            it.name = name
            view?.onLoadPlayerSuccess(it)
        }
    }

    override fun toggleGender() {
        player?.let {
            when (it.gender) {
                Gender.MALE -> {
                    it.gender = Gender.FEMALE
                }
                Gender.FEMALE -> {
                    it.gender = Gender.MALE
                }
            }
            view?.onLoadPlayerSuccess(it)
        }
    }

    override fun setColor(newColor: Int) {
        player?.let {
            it.color = newColor
            view?.onLoadPlayerSuccess(it)
        }
    }

    override fun savePlayer() {
        player?.let {
            if (it.id == 0L) {
                insertPlayer(it)
            } else {
                updatePlayer(it)
            }
        }
    }

    private fun updatePlayer(player: PlayerModel) {
        disposables.add(useCase.updatePlayer(player)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( { view?.onSavePlayerSuccess() }, this::onSavePlayerError)
        )
    }

    private fun insertPlayer(player: PlayerModel) {
        disposables.add(useCase.insertPlayer(player)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( { view?.onSavePlayerSuccess() }, this::onSavePlayerError)
        )
    }

    private fun onSavePlayerError(throwable: Throwable) {
        when (throwable) {
            is IllegalArgumentException -> view?.onPlayerValidationFailed(throwable)
            else -> view?.onSavePlayerError(throwable)
        }
    }

    override fun deletePlayer() {
        player?.let {
            disposables.add(useCase.deletePlayer(it.id)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { view?.onDeletePlayerSuccess() },
                            { error -> view?.onDeletePlayerError(error) }
                    )
            )
        }
    }

}