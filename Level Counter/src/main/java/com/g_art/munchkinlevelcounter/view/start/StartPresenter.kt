package com.g_art.munchkinlevelcounter.view.start

import com.g_art.munchkinlevelcounter.mvp.BasePresenter
import com.g_art.munchkinlevelcounter.usecases.GameUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

/**
 * Created by agulia on 3/14/18.
 */
class StartPresenter
@Inject constructor(private val gameUseCase: GameUseCase) :
        BasePresenter<StartContract.View>(), StartContract.Presenter {

    override fun checkUnfinishedGame() {
        disposables.add(gameUseCase.getLastUnfinishedGame()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            if (it.endDate == null) {
                                view?.showUnfinishedGameMessage(it.id, it.partyId == 0L)
                            }
                        },
                        { view?.onError(it) })
        )
    }

    override fun finishTheGame(gameId: Long) {
        disposables.add(
                gameUseCase.finishTheGame(gameId).observeOn(AndroidSchedulers.mainThread())
                        .subscribe({}, { view?.onError(it) })
        )
    }
}