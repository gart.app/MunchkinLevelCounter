package com.g_art.munchkinlevelcounter.di

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by agulia on 3/19/18.
 */
@Module(includes = [(LCAppDbModule::class)])
class LCAppModule {

    @Singleton
    @Provides
    fun provideContext(application: Application): Context = application.applicationContext
}