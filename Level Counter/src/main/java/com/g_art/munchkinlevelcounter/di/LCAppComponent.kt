package com.g_art.munchkinlevelcounter.di

import android.app.Application
import com.g_art.munchkinlevelcounter.repository.ActionsRepository
import com.g_art.munchkinlevelcounter.repository.GameRepository
import com.g_art.munchkinlevelcounter.repository.PartyRepository
import com.g_art.munchkinlevelcounter.repository.PlayerRepository
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

/**
 * Created by agulia on 3/19/18.
 */

@Singleton
@Component(modules = [(LCAppModule::class)])
interface LCAppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): LCAppComponent
    }

    fun exposePartyListRepository(): PartyRepository

    fun exposePlayerListRepository(): PlayerRepository

    fun exposeGameRepository(): GameRepository

    fun exposeActionsRepository(): ActionsRepository
}