package com.g_art.munchkinlevelcounter.di

import com.g_art.munchkinlevelcounter.LCApplication
import com.g_art.munchkinlevelcounter.dao.GameDao
import com.g_art.munchkinlevelcounter.dao.PartyDao
import com.g_art.munchkinlevelcounter.dao.PlayerActionDao
import com.g_art.munchkinlevelcounter.dao.PlayerDao
import com.g_art.munchkinlevelcounter.entity.*
import com.g_art.munchkinlevelcounter.mapper.*
import com.g_art.munchkinlevelcounter.model.*
import com.g_art.munchkinlevelcounter.repository.*
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by agulia on 3/19/18.
 */
@Module
class LCAppDbModule {

    @Singleton
    @Provides
    fun providePartyRepository(partyDao: PartyDao,
                               playerDao: PlayerDao,
                               partyModelMapper: EntityModelMapper<Party, PartyModel>,
                               partyAndPlayersMapper: EntityModelMapper<PartyAndPlayersAmount, PartyModel>,
                               playerModelMapper: EntityModelMapper<Player, PlayerModel>,
                               playerInPartyModelMapper: EntityModelMapper<PlayerInParty, PlayerInPartyModel>):
            PartyRepository = PartyRepositoryImpl(partyDao, playerDao, partyModelMapper, partyAndPlayersMapper, playerModelMapper, playerInPartyModelMapper)

    @Singleton
    @Provides
    fun providePlayerRepository(playerDao: PlayerDao,
                                playerModelMapper: EntityModelMapper<Player, PlayerModel>,
                                selectionPlayerModelMapper: EntityModelMapper<Player, SelectionPlayerModel>):
            PlayerRepository = PlayerRepositoryImpl(playerDao, playerModelMapper, selectionPlayerModelMapper)

    @Singleton
    @Provides
    fun provideGameRepository(gameDao: GameDao,
                              playerDao: PlayerDao,
                              playerActionDao: PlayerActionDao,
                              gameMapper: EntityModelMapper<Game, GameModel>,
                              playerInGameMapper: EntityModelMapper<PlayerInGame, PlayerInGameModel>,
                              monsterMapper: EntityModelMapper<Monster, MonsterModel>,
                              activePlayerModelMapper: EntityModelMapper<ActivePlayer, ActivePlayerModel>,
                              playerActionModelMapper: EntityModelMapper<PlayerAction, PlayerActionModel>,
                              activePlayerAndGameModelMapper: EntityModelMapper<ActivePlayerAndGame, ActivePlayerAndGameModel>,
                              activePartyAndGameModelMapper: EntityModelMapper<ActivePartyAndGame, ActivePartyAndGameModel>):
            GameRepository = GameRepositoryImpl(gameDao, playerDao, playerActionDao, gameMapper, playerInGameMapper, monsterMapper,
            activePlayerModelMapper, playerActionModelMapper, activePlayerAndGameModelMapper, activePartyAndGameModelMapper)

    @Singleton
    @Provides
    fun provideActionsRepository(actionsDao: PlayerActionDao,
                                 playerInGameMapper: EntityModelMapper<PlayerInGame, PlayerInGameModel>,
                                 playerActionModelMapper: EntityModelMapper<PlayerAction, PlayerActionModel>,
                                 playerMapper: EntityModelMapper<Player, PlayerModel>):
            ActionsRepository = ActionsRepositoryImpl(actionsDao, playerInGameMapper, playerActionModelMapper, playerMapper)

    @Singleton
    @Provides
    fun providePartyMapper(): EntityModelMapper<Party, PartyModel> = PartyModelMapper()

    @Singleton
    @Provides
    fun provideMonsterMapper(): EntityModelMapper<Monster, MonsterModel> = MonsterModelMapper()

    @Singleton
    @Provides
    fun providePartyAndPlayerAmountMapper(): EntityModelMapper<PartyAndPlayersAmount, PartyModel> = PartyAndPlayersMapper()

    @Singleton
    @Provides
    fun providePlayerMapper(): EntityModelMapper<Player, PlayerModel> = PlayerModelMapper()

    @Singleton
    @Provides
    fun provideGameMapper(): EntityModelMapper<Game, GameModel> = GameModelMapper()

    @Singleton
    @Provides
    fun providePlayerActionMapper(): EntityModelMapper<PlayerAction, PlayerActionModel> = PlayerActionModelMapper()

    @Singleton
    @Provides
    fun provideActivePlayerModelMapper(selectionPlayerModelMapper: EntityModelMapper<Player, SelectionPlayerModel>,
                                       playerInGameMapper: EntityModelMapper<PlayerInGame, PlayerInGameModel>):
            EntityModelMapper<ActivePlayer, ActivePlayerModel> =
            ActivePlayerModelMapper(selectionPlayerModelMapper, playerInGameMapper)

    @Singleton
    @Provides
    fun provideActivePlayerAndGameModelMapper(gameMapper: EntityModelMapper<Game, GameModel>,
                                              activePlayerModelMapper: EntityModelMapper<ActivePlayer, ActivePlayerModel>):
            EntityModelMapper<ActivePlayerAndGame, ActivePlayerAndGameModel> =
            ActivePlayerAndGameModelMapper(gameMapper, activePlayerModelMapper)

    @Singleton
    @Provides
    fun provideActivePartyAndGameModelMapper(gameMapper: EntityModelMapper<Game, GameModel>,
                                              activePlayerModelMapper: EntityModelMapper<ActivePlayer, ActivePlayerModel>):
            EntityModelMapper<ActivePartyAndGame, ActivePartyAndGameModel> =
            ActivePartyAndGameModelMapper(gameMapper, activePlayerModelMapper)

    @Singleton
    @Provides
    fun provideSelectionPlayerMapper(): EntityModelMapper<Player, SelectionPlayerModel> = SelectionPlayerModelMapper()

    @Singleton
    @Provides
    fun providePlayerInPartyMapper(): EntityModelMapper<PlayerInParty, PlayerInPartyModel> = PlayerInPartyModelMapper()

    @Singleton
    @Provides
    fun providePlayerInGameMapper(): EntityModelMapper<PlayerInGame, PlayerInGameModel> = PlayerInGameModelMapper()

    @Singleton
    @Provides
    fun providePlayerDAO(): PlayerDao = LCApplication.database.playerDao()

    @Singleton
    @Provides
    fun providePartyDAO(): PartyDao = LCApplication.database.partyDao()

    @Singleton
    @Provides
    fun provideGameDAO(): GameDao = LCApplication.database.gameDao()

    @Singleton
    @Provides
    fun provideActionHistoryDAO(): PlayerActionDao = LCApplication.database.playerActionDao()
}