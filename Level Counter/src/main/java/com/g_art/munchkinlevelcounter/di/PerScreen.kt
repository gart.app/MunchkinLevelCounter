package com.g_art.munchkinlevelcounter.di

import javax.inject.Scope

/**
 * Custom Scope for each Screen
 * Created by agulia on 3/19/18.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerScreen