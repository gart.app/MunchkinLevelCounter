package com.g_art.munchkinlevelcounter.mapper

import com.g_art.munchkinlevelcounter.entity.ActivePartyAndGame
import com.g_art.munchkinlevelcounter.entity.ActivePlayer
import com.g_art.munchkinlevelcounter.entity.Game
import com.g_art.munchkinlevelcounter.model.ActivePartyAndGameModel
import com.g_art.munchkinlevelcounter.model.ActivePartyModel
import com.g_art.munchkinlevelcounter.model.ActivePlayerModel
import com.g_art.munchkinlevelcounter.model.GameModel

class ActivePartyAndGameModelMapper(private val gameModelMapper: EntityModelMapper<Game, GameModel>,
                                    private val activePlayerModelMapper: EntityModelMapper<ActivePlayer, ActivePlayerModel>) :
        EntityModelMapper<ActivePartyAndGame, ActivePartyAndGameModel> {

    override fun fromEntity(from: ActivePartyAndGame): ActivePartyAndGameModel {
        val partyAndGame = ActivePartyAndGameModel()
        partyAndGame.game = gameModelMapper.fromEntity(from.game)
        val party = ActivePartyModel()
        party.activePlayers = from.activePlayers.map { activePlayerModelMapper.fromEntity(it) }
        partyAndGame.activeParty = party
        return partyAndGame
    }

    override fun toEntity(from: ActivePartyAndGameModel): ActivePartyAndGame {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}