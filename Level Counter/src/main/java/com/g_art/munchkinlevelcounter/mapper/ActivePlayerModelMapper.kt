package com.g_art.munchkinlevelcounter.mapper

import com.g_art.munchkinlevelcounter.entity.ActivePlayer
import com.g_art.munchkinlevelcounter.entity.Player
import com.g_art.munchkinlevelcounter.entity.PlayerInGame
import com.g_art.munchkinlevelcounter.model.ActivePlayerModel
import com.g_art.munchkinlevelcounter.model.PlayerInGameModel
import com.g_art.munchkinlevelcounter.model.SelectionPlayerModel

class ActivePlayerModelMapper(private var playerModelMapper: EntityModelMapper<Player, SelectionPlayerModel>,
                              private var playerInGameModelMapper: EntityModelMapper<PlayerInGame, PlayerInGameModel>):
        EntityModelMapper<ActivePlayer, ActivePlayerModel> {

    override fun fromEntity(from: ActivePlayer) = ActivePlayerModel(
            player = playerModelMapper.fromEntity(from.player),
            playerInGame = playerInGameModelMapper.fromEntity(from.playerInGame)
    )

    override fun toEntity(from: ActivePlayerModel): ActivePlayer {
        val entity = ActivePlayer()
        entity.player = playerModelMapper.toEntity(from.player)
        entity.playerInGame = playerInGameModelMapper.toEntity(from.playerInGame)
        return entity
    }
}