package com.g_art.munchkinlevelcounter.mapper

/**
 * Created by agulia on 3/20/18.
 */
interface EntityModelMapper<E, M> {
    fun fromEntity(from: E): M
    fun toEntity(from: M): E
}