package com.g_art.munchkinlevelcounter.mapper

import com.g_art.munchkinlevelcounter.entity.PlayerAction
import com.g_art.munchkinlevelcounter.model.PlayerActionModel

/**
 * Created by agulia on 3/22/18.
 */
class PlayerActionModelMapper: EntityModelMapper<PlayerAction, PlayerActionModel> {
    override fun fromEntity(from: PlayerAction) =
            PlayerActionModel(from.id, from.gameId, from.playerId, from.amount, from.valueType, from.actionDate)

    override fun toEntity(from: PlayerActionModel) =
            PlayerAction(from.id, from.gameId, from.playerId, from.amount, from.valueType, from.actionDate)

}