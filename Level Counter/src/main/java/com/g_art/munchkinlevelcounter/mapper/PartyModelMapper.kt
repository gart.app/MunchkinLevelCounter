package com.g_art.munchkinlevelcounter.mapper

import com.g_art.munchkinlevelcounter.entity.Party
import com.g_art.munchkinlevelcounter.model.PartyModel

/**
 * Created by agulia on 3/20/18.
 */
class PartyModelMapper: EntityModelMapper<Party, PartyModel> {
    override fun fromEntity(from: Party) = PartyModel(from.id, from.partyName, 0)

    override fun toEntity(from: PartyModel) = Party(from.id, from.name)
}