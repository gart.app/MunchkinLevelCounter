package com.g_art.munchkinlevelcounter.mapper

import com.g_art.munchkinlevelcounter.entity.Monster
import com.g_art.munchkinlevelcounter.model.MonsterModel

class MonsterModelMapper: EntityModelMapper<Monster, MonsterModel> {
    override fun fromEntity(from: Monster) = MonsterModel(from.id, from.gameId, from.level, from.mods, from.treasures)

    override fun toEntity(from: MonsterModel) = Monster(from.id, from.gameId, from.level, from.mods, from.treasures)
}