package com.g_art.munchkinlevelcounter.mapper

import com.g_art.munchkinlevelcounter.entity.PlayerInParty
import com.g_art.munchkinlevelcounter.model.PlayerInPartyModel

/**
 * Created by agulia on 3/21/18.
 */
class PlayerInPartyModelMapper: EntityModelMapper<PlayerInParty, PlayerInPartyModel> {
    override fun fromEntity(from: PlayerInParty) = PlayerInPartyModel(from.playerId, from.partyId)

    override fun toEntity(from: PlayerInPartyModel) = PlayerInParty(from.playerId, from.partyId)
}