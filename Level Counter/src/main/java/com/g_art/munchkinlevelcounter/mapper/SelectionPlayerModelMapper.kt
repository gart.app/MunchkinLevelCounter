package com.g_art.munchkinlevelcounter.mapper

import com.g_art.munchkinlevelcounter.entity.Player
import com.g_art.munchkinlevelcounter.model.SelectionPlayerModel

class SelectionPlayerModelMapper: EntityModelMapper<Player, SelectionPlayerModel> {
    override fun fromEntity(from: Player) = SelectionPlayerModel(from.id, from.playerName, from.gender, from.color, from.owner, from.position, false)

    override fun toEntity(from: SelectionPlayerModel) = Player(from.id, from.name, from.gender, from.color, from.owner, from.position)

}