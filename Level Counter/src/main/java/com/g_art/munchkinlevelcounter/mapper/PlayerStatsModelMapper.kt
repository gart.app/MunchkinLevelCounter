package com.g_art.munchkinlevelcounter.mapper

import com.g_art.munchkinlevelcounter.entity.PlayerStats
import com.g_art.munchkinlevelcounter.model.PlayerStatsModel

/**
 * Created by agulia on 3/21/18.
 */
class PlayerStatsModelMapper: EntityModelMapper<PlayerStats, PlayerStatsModel> {

    override fun fromEntity(from: PlayerStats) =
            PlayerStatsModel(from.id, from.gamesPlayed, from.gamesWon,
                    from.treasures, from.monsters, from.died,
                    from.maxLevel, from.maxPower, from.playerId)

    override fun toEntity(from: PlayerStatsModel) =
            PlayerStats(from.id, from.gamesPlayed, from.gamesWon,
                    from.treasures, from.monsters, from.died,
                    from.maxLevel, from.maxPower, from.playerId)

}