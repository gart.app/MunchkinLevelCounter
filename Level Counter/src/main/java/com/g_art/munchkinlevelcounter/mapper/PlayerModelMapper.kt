package com.g_art.munchkinlevelcounter.mapper

import com.g_art.munchkinlevelcounter.entity.Player
import com.g_art.munchkinlevelcounter.model.PlayerModel

/**
 * Created by agulia on 3/20/18.
 */
open class PlayerModelMapper: EntityModelMapper<Player, PlayerModel> {
    override fun fromEntity(from: Player) = PlayerModel(from.id, from.playerName, from.gender, from.color, from.owner, from.position)

    override fun toEntity(from: PlayerModel) = Player(from.id, from.name, from.gender, from.color, from.owner, from.position)
}