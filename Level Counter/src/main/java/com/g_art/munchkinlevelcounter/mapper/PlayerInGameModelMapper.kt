package com.g_art.munchkinlevelcounter.mapper

import com.g_art.munchkinlevelcounter.entity.PlayerInGame
import com.g_art.munchkinlevelcounter.model.PlayerInGameModel

/**
 * Created by agulia on 3/22/18.
 */
class PlayerInGameModelMapper: EntityModelMapper<PlayerInGame, PlayerInGameModel> {
    override fun fromEntity(from: PlayerInGame) =
            PlayerInGameModel(from.id, from.gameId, from.playerId, from.level, from.gear, from.mods, from.winner, from.warrior, from.treasures)

    override fun toEntity(from: PlayerInGameModel) =
            PlayerInGame(from.id, from.gameId, from.playerId, from.level, from.gear, from.mods, from.winner, from.warrior, from.treasures)

}