package com.g_art.munchkinlevelcounter.mapper

import com.g_art.munchkinlevelcounter.entity.Party
import com.g_art.munchkinlevelcounter.entity.PartyAndPlayersAmount
import com.g_art.munchkinlevelcounter.model.PartyModel

class PartyAndPlayersMapper: EntityModelMapper<PartyAndPlayersAmount, PartyModel> {
    override fun fromEntity(from: PartyAndPlayersAmount) = PartyModel(from.party.id, from.party.partyName, from.playersAmount)

    override fun toEntity(from: PartyModel) = PartyAndPlayersAmount(Party(from.id, from.name), from.playersAmount)
}