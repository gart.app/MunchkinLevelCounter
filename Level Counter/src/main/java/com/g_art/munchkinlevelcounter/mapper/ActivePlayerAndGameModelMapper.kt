package com.g_art.munchkinlevelcounter.mapper

import com.g_art.munchkinlevelcounter.entity.ActivePlayer
import com.g_art.munchkinlevelcounter.entity.ActivePlayerAndGame
import com.g_art.munchkinlevelcounter.entity.Game
import com.g_art.munchkinlevelcounter.model.ActivePlayerAndGameModel
import com.g_art.munchkinlevelcounter.model.ActivePlayerModel
import com.g_art.munchkinlevelcounter.model.GameModel

class ActivePlayerAndGameModelMapper(private val gameModelMapper: EntityModelMapper<Game, GameModel>,
                                     private val activePlayerModelMapper: EntityModelMapper<ActivePlayer, ActivePlayerModel>):
        EntityModelMapper<ActivePlayerAndGame, ActivePlayerAndGameModel> {

    override fun fromEntity(from: ActivePlayerAndGame): ActivePlayerAndGameModel {
        val activePlayerAndGameModel = ActivePlayerAndGameModel()
        activePlayerAndGameModel.game = gameModelMapper.fromEntity(from.game)
        activePlayerAndGameModel.activePlayer = activePlayerModelMapper.fromEntity(from.activePlayer)
        return activePlayerAndGameModel
    }

    override fun toEntity(from: ActivePlayerAndGameModel): ActivePlayerAndGame {
        return ActivePlayerAndGame(gameModelMapper.toEntity(from.game),
                activePlayerModelMapper.toEntity(from.activePlayer))
    }
}