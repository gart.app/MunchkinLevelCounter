package com.g_art.munchkinlevelcounter.mapper

import com.g_art.munchkinlevelcounter.entity.Game
import com.g_art.munchkinlevelcounter.model.GameModel

/**
 * Created by agulia on 3/20/18.
 */
class GameModelMapper: EntityModelMapper<Game, GameModel> {
    override fun fromEntity(from: Game) = GameModel(id = from.id,
            playerId = from.playerId, partyId = from.partyId,
            startDate = from.startDate, endDate = from.endDate,
            maxLevel = from.maxLevel)

    override fun toEntity(from: GameModel) = Game(id = from.id,
            playerId = from.playerId, partyId = from.partyId,
            startDate = from.startDate, endDate = from.endDate,
            maxLevel = from.maxLevel)
}