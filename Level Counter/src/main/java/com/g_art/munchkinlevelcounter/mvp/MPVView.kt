package com.g_art.munchkinlevelcounter.mvp

interface MPVView {
    fun getGameState(): Int
}