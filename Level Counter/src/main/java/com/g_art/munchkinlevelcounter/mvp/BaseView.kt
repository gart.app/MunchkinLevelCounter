package com.g_art.munchkinlevelcounter.mvp

import android.content.Context
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import butterknife.ButterKnife
import com.afollestad.materialdialogs.color.ColorChooserDialog
import com.bluelinelabs.conductor.Controller
import com.g_art.munchkinlevelcounter.view.start.activity.StartActivity

/**
 * Created by agulia on 3/13/18.
 */
abstract class BaseView : Controller(), MPVView {

    companion object {
        //TYPE
        const val PLAYER_ID = "playerId"
        const val PARTY_ID = "partyId"
        const val GAME_ID = "gameId"
        const val ACTION_ID = "actionId"
        const val BUNDLE = "bundle"
        const val TAG_KEY = "tag_key"
        const val STATS_TYPE = "stats_type"

        //ACTIONS
        const val DEFAULT = 0L
        const val FROM_THE_PARTY = 100L
        const val FROM_THE_GAME = 101L
        const val MENU_CLICK = 102L
        const val LVL_STATS = 500
        const val GEAR_STATS = 501
        const val POWER_STATS = 502
        const val TREASURES_STATS = 503
    }

    // Inject dependencies once per life of Controller
    val inject by lazy { injectDependencies() }


    override fun onContextAvailable(context: Context) {
        super.onContextAvailable(context)
        inject
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        val view = inflater.inflate(getLayoutId(), container, false)
        ButterKnife.bind(this, view)
        setHasOptionsMenu(true)
        return view
    }

    override fun onAttach(view: View) {
        super.onAttach(view)
        setToolbarTitle()
    }

    override fun onDetach(view: View) {
        super.onDetach(view)
        getPresenter().stop()
    }

    override fun onDestroy() {
        getPresenter().destroy()
        super.onDestroy()
    }

    protected fun View.hideKeyboard() {
        val inputMethodManager = applicationContext?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(this.windowToken, 0)
    }

    protected fun showMessage(@StringRes msgResId: Int) {
        Toast.makeText(this.applicationContext, msgResId, Toast.LENGTH_SHORT).show()
    }

    private fun setToolbarTitle() {
        // if the Activity happens to be non-AppCompatActivity or it does not have ActionBar, simply do not set the title
        (activity as? AppCompatActivity)?.supportActionBar?.apply {
            getToolbarTitleId()?.apply {
                title = resources?.getString(this)
                setDisplayHomeAsUpEnabled(router.backstackSize > 1)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                router.popCurrentController()
            }
        }
        return true
    }

    protected fun getStartActivity(): StartActivity {
        return activity as StartActivity
    }

    protected fun logEvent(event: String, bundle: Bundle) {
        (activity as? StartActivity)?.getAnalytics()?.apply {
            this.logEvent(event, bundle)
        }
    }

    override fun getGameState(): Int = getStartActivity().getGameType()

    open fun onColorChoose(dialog: ColorChooserDialog, selectedColor: Int) {}

    @LayoutRes
    protected abstract fun getLayoutId(): Int

    @StringRes
    protected abstract fun getToolbarTitleId(): Int?

    protected abstract fun injectDependencies()

    protected abstract fun getPresenter(): MVPPresenter

    fun showAdIfPossible() {
        getStartActivity().showAdIfPossible()
    }

}