package com.g_art.munchkinlevelcounter.mvp

import io.reactivex.disposables.CompositeDisposable

/**
 * Created by agulia on 3/13/18.
 */

abstract class BasePresenter<V>: MVPPresenter {

    protected val disposables: CompositeDisposable = CompositeDisposable()
    protected var view: V? = null
        private set

    open fun start(view: V) {
        this.view = view
    }

    override fun stop() {
        this.view = null
    }

    override fun destroy() {
        disposables.clear()
    }
}