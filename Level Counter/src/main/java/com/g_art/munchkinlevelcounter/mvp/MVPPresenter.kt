package com.g_art.munchkinlevelcounter.mvp

/**
 * Created by agulia on 3/16/18.
 */
interface MVPPresenter {
    fun stop()
    fun destroy()
}