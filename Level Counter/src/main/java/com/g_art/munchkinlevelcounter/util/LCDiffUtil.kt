package com.g_art.munchkinlevelcounter.util


class LCDiffUtil {
    companion object {
        const val PLAYER_COLOR_KEY = "playerColor"
        const val PLAYER_NAME_KEY = "playerName"
        const val PLAYER_LEVEL_KEY = "playerLevel"
        const val PLAYER_TOTAL_KEY = "playerTotal"
        const val HIGHLIGHT_KEY = "highlight"
        const val GENDER_KEY = "playerGender"
    }
}