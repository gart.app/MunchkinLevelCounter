package com.g_art.munchkinlevelcounter.util.ads

import android.os.Bundle
import com.g_art.munchkinlevelcounter.R
import com.g_art.munchkinlevelcounter.util.*
import com.g_art.munchkinlevelcounter.view.start.activity.StartActivity
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.ads.MobileAds
import java.util.*


class AdsUtility(activity: StartActivity) : AdListener() {

    private var mInterstitialAd: InterstitialAd? = null
    private var mActivity: StartActivity = activity
    private var mLastShowDate: Date? = null

    init {
        MobileAds.initialize(mActivity, mActivity.getString(R.string.ads_app_id))
        MobileAds.setAppMuted(true)
        // Load ads into Interstitial Ads
        loadAd()
    }

    override fun onAdLoaded() {
        mActivity.getAnalytics()?.logEvent(AD_LOADED, Bundle.EMPTY)
    }

    override fun onAdFailedToLoad(errorCode: Int) {
        mActivity.getAnalytics()?.logEvent(AD_FAILED, Bundle.EMPTY)
    }

    override fun onAdOpened() {
        mActivity.getAnalytics()?.logEvent(AD_SHOWN, Bundle.EMPTY)
    }

    override fun onAdLeftApplication() {
        mActivity.getAnalytics()?.logEvent(AD_CANCELED, Bundle.EMPTY)
    }

    override fun onAdClosed() {
        loadAd()
        mActivity.getAnalytics()?.logEvent(AD_CLOSED, Bundle.EMPTY)
    }

    private fun loadAd() {
        if (mActivity.getGameType() == AppType.ADS) {
            mInterstitialAd = InterstitialAd(mActivity)
            mInterstitialAd?.adUnitId = mActivity.getString(R.string.interstitial_ads)
            mInterstitialAd?.loadAd(AdRequest.Builder()
                    .build())
        }
    }

    fun showAd() {
        mInterstitialAd?.let {
            if (it.isLoaded && canIShowAd()) {
                it.show()
            } else {
                mActivity.getAnalytics()?.logEvent(AD_NOT_AVAILABLE, Bundle.EMPTY)
                loadAd()
            }
        }
    }

    private fun canIShowAd() : Boolean {
        if (mActivity.getGameType() == AppType.ADS) {
            if (mLastShowDate == null || wasTimeOutEnded()) {
                mLastShowDate = Date()
                return true
            }
        }

        return false
    }

    private fun wasTimeOutEnded(): Boolean {
        if (mLastShowDate != null) {
            val cal = Calendar.getInstance()
            cal.time = Date()
            cal.add(Calendar.MINUTE, -10)
            val timeOutEnded = mLastShowDate!!.before(cal.time)
            if (!timeOutEnded) {
                mActivity.getAnalytics()?.logEvent(AD_NOT_AVAILABLE, Bundle.EMPTY)
            }
            return timeOutEnded
        }
        return true
    }

}