package com.g_art.munchkinlevelcounter.util.converter

import androidx.room.TypeConverter
import com.g_art.munchkinlevelcounter.entity.ActionType
import com.g_art.munchkinlevelcounter.entity.Gender
import java.util.*

/**
 * Created by agulia on 3/15/18.
 */
object LCTypeConverters {

    @TypeConverter
    @JvmStatic
    fun toDateTime(value: Long?): Date? {
        return value?.let {
            return Date(it)
        }
    }

    @TypeConverter
    @JvmStatic
    fun fromDateTime(date: Date?): Long? {
        return date?.let {
            return it.time
        }
    }

    @TypeConverter
    @JvmStatic
    fun toActionType(value: String?): ActionType? {
        return ActionType.getActionByType(value)
    }

    @TypeConverter
    @JvmStatic
    fun fromActionType(actionType: ActionType?): String? {
        return actionType?.let { it.type }
    }

    @TypeConverter
    @JvmStatic
    fun toGender(value: String?): Gender? {
        return Gender.getGenderByString(value)
    }

    @TypeConverter
    @JvmStatic
    fun fromGender(gender: Gender?): String? {
        return gender?.let { it.gender }
    }
}