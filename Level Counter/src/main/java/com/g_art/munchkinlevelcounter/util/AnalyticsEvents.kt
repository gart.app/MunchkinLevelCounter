package com.g_art.munchkinlevelcounter.util

const val BTN_CLICK = "BTN_CLICK"
const val INFO_DIALOG = "INFO_DIALOG"
const val LONE_WOLF_CLICK = "LONE_WOLF_CLICK"
const val LONE_WOLF_GAME = "LONE_WOLF_GAME"
const val GAME_MASTER_CLICK = "GAME_MASTER_CLICK"
const val GAME_MASTER_GAME = "GAME_MASTER_GAME"
const val SETTINGS_CLICK = "SETTINGS_CLICK"
const val POLICY_CLICK = "POLICY_CLICK"
const val GAME_HISTORY_CLICK = "GAME_HISTORY_CLICK"
const val ABOUT_CLICK = "ABOUT_CLICK"
const val UNFINISHED_GAME = "UNFINISHED_GAME"
const val CONTINUE_CLICK = "CONTINUE_CLICK"
const val FINISH_CLICK = "FINISH_CLICK"
const val GAME_STARTED = "GAME_STARTED"
const val VIEW_OPEN = "VIEW_OPEN"
const val GAME_ENDED = "GAME_ENDED"
const val DICE_CLICK = "DICE_CLICK"
const val MAX_LVL_CLICK = "MAX_LVL_CLICK"
const val GAME_ERROR = "GAME_ERROR"
const val EDIT_PLAYERS_CLICK = "EDIT_PLAYERS_CLICK"
const val PLAYER_LIST_VIEW = "PLAYER_LIST_VIEW"
const val DEFAULT_USE = "DEFAULT_USE"
const val FROM_THE_PARTY_USE = "FROM_THE_PARTY_USE"
const val FROM_THE_GAME_USE = "FROM_THE_GAME_USE"
const val GAME_DETAILS = "GAME_DETAILS"
const val BATTLE_SCREEN = "BATTLE_SCREEN"
const val RUN_AWAY = "RUN_AWAY"
const val FIGHT = "FIGHT"
const val GAME_HISTORY = "GAME_HISTORY"
const val PARTY_DETAILS = "PARTY_DETAILS"
const val PARTY_LIST = "PARTY_LIST"
const val CREATE_PARTY = "PARTY_LIST"
const val EDIT_PARTY = "EDIT_PARTY"
const val REMOVE_PARTY = "REMOVE_PARTY"
const val PARTY_DATA = "PARTY_DATA"
const val DATA_LOADED = "DATA_LOADED"
const val GAME_DATA = "GAME_DATA"
const val PLAYER_DETAILS = "PLAYER_DETAILS"
const val STATS_SCREEN = "STATS_SCREEN"

/** ===== ADS ===== */
const val AD_LOADED = "AD_LOADED"
const val AD_SHOWN = "AD_SHOWN"
const val AD_CANCELED = "AD_CANCELED"
const val AD_NOT_AVAILABLE = "AD_NOT_AVAILABLE"
const val AD_FAILED = "AD_FAILED"
const val AD_CLOSED = "AD_CLOSED"
const val AD_TIMEOUT = "AD_TIMEOUT" +
        ""