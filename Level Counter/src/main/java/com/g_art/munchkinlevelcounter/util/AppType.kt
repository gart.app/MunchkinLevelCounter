package com.g_art.munchkinlevelcounter.util

class AppType {
    companion object {
        const val ADS_FREE = 0
        const val ADS = 1
        const val PAID = 2
    }
}