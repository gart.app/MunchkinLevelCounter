package com.g_art.munchkinlevelcounter.util

import com.g_art.munchkinlevelcounter.model.MultiEdit

class UtilityData {

    companion object {
        val multiEditValues: List<MultiEdit> = listOf(
                MultiEdit(5, "+5"),
                MultiEdit(4, "+4"),
                MultiEdit(3, "+3"),
                MultiEdit(-3, "-3"),
                MultiEdit(-4, "-4"),
                MultiEdit(-5, "-5")
        )
    }

}