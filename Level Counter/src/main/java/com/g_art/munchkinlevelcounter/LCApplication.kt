package com.g_art.munchkinlevelcounter

import android.R
import android.app.Application
import android.graphics.Color
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.g_art.munchkinlevelcounter.database.LCDatabase
import com.g_art.munchkinlevelcounter.di.DaggerLCAppComponent
import com.g_art.munchkinlevelcounter.di.LCAppComponent
import com.g_art.munchkinlevelcounter.entity.*
import com.g_art.munchkinlevelcounter.ext.ioThread
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump
import java.util.*


/**
 * Created by agulia on 2/26/18.
 */

class LCApplication : Application() {

    companion object {
        lateinit var database: LCDatabase
        var db_name: String = "level-counter-db"

        lateinit var component: LCAppComponent
            private set

        private val rnd = Random()

        val PLAYER_DATA = listOf(
                Player(color = rndColor(), gender = Gender.MALE, owner = false, position = 10, playerName = "PlayerName 1"),
                Player(color = rndColor(), gender = Gender.FEMALE, owner = false, position = 2, playerName = "PlayerName 2"),
                Player(color = rndColor(), gender = Gender.MALE, owner = false, position = 3, playerName = "PlayerName 3"),
                Player(color = rndColor(), gender = Gender.FEMALE, owner = false, position = 1, playerName = "PlayerName 4"),
                Player(color = rndColor(), gender = Gender.MALE, owner = false, position = 0, playerName = "PlayerName 5"),
                Player(color = rndColor(), gender = Gender.FEMALE, owner = false, position = 7, playerName = "PlayerName 6"),
                Player(color = rndColor(), gender = Gender.MALE, owner = false, position = 5, playerName = "PlayerName 7"),
                Player(color = rndColor(), gender = Gender.FEMALE, owner = false, position = 3, playerName = "PlayerName 8"),
                Player(color = rndColor(), gender = Gender.MALE, owner = false, position = 8, playerName = "PlayerName 9")
        )

        val PARTY_DATA = listOf(
                Party(partyName = "Party 1"),
                Party(partyName = "Party 2"),
                Party(partyName = "Party 3"),
                Party(partyName = "Party 4")
        )

        val PARTY_PLAYERS_DATA = listOf(
                PlayerInParty(partyId = 1, playerId = 1),
                PlayerInParty(partyId = 1, playerId = 2),
                PlayerInParty(partyId = 1, playerId = 3),
                PlayerInParty(partyId = 1, playerId = 4),
                PlayerInParty(partyId = 2, playerId = 1),
                PlayerInParty(partyId = 2, playerId = 2),
                PlayerInParty(partyId = 2, playerId = 6),
                PlayerInParty(partyId = 3, playerId = 8),
                PlayerInParty(partyId = 3, playerId = 9),
                PlayerInParty(partyId = 3, playerId = 3),
                PlayerInParty(partyId = 4, playerId = 4),
                PlayerInParty(partyId = 4, playerId = 5),
                PlayerInParty(partyId = 4, playerId = 6)
        )

        fun rndColor(): Int {
            return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))

        }
    }

    override fun onCreate() {
        super.onCreate()

//        LeakCanary.install(this)

        database = Room
                .databaseBuilder(this, LCDatabase::class.java, db_name)
                .addCallback(object: RoomDatabase.Callback() {
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        super.onCreate(db)
                        ioThread {
                            database.playerDao().insertAll(PLAYER_DATA)
                            database.partyDao().insertAll(PARTY_DATA)
                            database.partyDao().addPlayersToParty(PARTY_PLAYERS_DATA)
                        }
                    }
                })
                .build()
        ViewPump.init(
            ViewPump.builder().addInterceptor(
                    CalligraphyInterceptor(
                        CalligraphyConfig.Builder()
                            .setDefaultFontPath("fonts/Quasimodo_Original.ttf")
                            .build()
                    )
                )
                .build()
        )

        component = DaggerLCAppComponent.builder()
                .application(this)
                .build()
    }

    val MIGRATION_1_2 = object : Migration(1, 2) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("")
        }
    }
}