package com.g_art.munchkinlevelcounter.usecases

import com.g_art.munchkinlevelcounter.model.*
import com.g_art.munchkinlevelcounter.repository.GameRepository
import com.g_art.munchkinlevelcounter.repository.PartyRepository
import com.g_art.munchkinlevelcounter.repository.PlayerRepository
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by G_Artem on 3/24/18.
 */
class PlayersUseCase(private val playerRepository: PlayerRepository,
                     private val partyRepository: PartyRepository,
                     private val gameRepository: GameRepository) {
    private val dateFormat: DateFormat = SimpleDateFormat.getDateInstance()


    fun loadAllPlayers(): Single<List<SelectionPlayerModel>> = playerRepository.getAllPlayers()

    fun loadPlayersFromParty(partyId: Long): Maybe<List<SelectionPlayerModel>> = playerRepository.findPlayersByPartyId(partyId)

    fun loadPlayersFromGame(gameId: Long): Maybe<List<SelectionPlayerModel>> = playerRepository.findPlayersByGameId(gameId)

    fun removePlayerById(id: Long): Completable = playerRepository.delete(id)

    fun editPlayersInParty(playersToAdd: List<PlayerInPartyModel>, playersToRemove: List<PlayerInPartyModel>): Completable =
            partyRepository.editPlayersInTheParty(playersToAdd, playersToRemove)

    fun editPlayersInTheGame(gameId: Long, partyId: Long,
                             playersToAdd: List<PlayerInGameModel>,
                             playersToRemove: List<PlayerInGameModel>): Completable =
            gameRepository.editPlayersInTheGame(gameId, partyId, playersToAdd, playersToRemove)

    fun startTheGame(player: SelectionPlayerModel): Maybe<Long> =
        gameRepository.createSingleGame(GameModel(startDate = Date(), playerId = player.id))

    fun createPartyAndInsertPlayers(toInsert: List<PlayerInGameModel>): Maybe<Long> =
        partyRepository.createPartyAndAddPlayers(PartyModel(name = dateFormat.format(Date())), toInsert.map { PlayerModel(id = it.playerId) })

}