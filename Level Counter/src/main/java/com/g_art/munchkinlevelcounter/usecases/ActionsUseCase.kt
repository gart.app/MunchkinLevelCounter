package com.g_art.munchkinlevelcounter.usecases

import com.g_art.munchkinlevelcounter.model.PlayerInGameActionsModel
import com.g_art.munchkinlevelcounter.repository.ActionsRepository
import io.reactivex.Single

class ActionsUseCase(private val actionsRepository: ActionsRepository) {

    fun getGameActions(gameId: Long):
            Single<List<PlayerInGameActionsModel>> {
        return actionsRepository.getGameActions(gameId)
    }

    fun getGameActionsForPlayer(gameId: Long, playerId: Long):
            Single<List<PlayerInGameActionsModel>> =
            actionsRepository.getGameActionsForPlayer(gameId, playerId)
}