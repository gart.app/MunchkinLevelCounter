package com.g_art.munchkinlevelcounter.usecases

import com.g_art.munchkinlevelcounter.model.GameModel
import com.g_art.munchkinlevelcounter.model.PartyModel
import com.g_art.munchkinlevelcounter.repository.GameRepository
import com.g_art.munchkinlevelcounter.repository.PartyRepository
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import java.util.*

/**
 * Created by agulia on 3/20/18.
 */
class PartiesUseCase(private val repository: PartyRepository,
                     private val gameRepository: GameRepository) {
    fun loadAllParties(): Single<List<PartyModel>> = repository.getAllParties()
    fun loadAllParties(limit: Int): Single<List<PartyModel>> = repository.getAllParties(limit)

    fun removePartyById(id: Long): Completable = repository.delete(id)

    fun startTheGame(partyId: Long): Maybe<Long> = gameRepository.createPartyGame(
            game = GameModel(
                    partyId = partyId,
                    startDate = Date()
            )
    )

}