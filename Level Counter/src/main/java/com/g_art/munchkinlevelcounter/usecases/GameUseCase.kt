package com.g_art.munchkinlevelcounter.usecases

import com.g_art.munchkinlevelcounter.entity.ActionType
import com.g_art.munchkinlevelcounter.entity.Gender
import com.g_art.munchkinlevelcounter.model.*
import com.g_art.munchkinlevelcounter.repository.GameRepository
import com.g_art.munchkinlevelcounter.repository.PlayerRepository
import io.reactivex.Completable
import io.reactivex.Maybe
import java.util.*
import kotlin.collections.ArrayList

class GameUseCase(private val gameRepository: GameRepository,
                  private val playerRepository: PlayerRepository) {

    fun loadGames() = gameRepository.loadPlayedGames()

    fun loadGames(limit: Int) = gameRepository.loadPlayedGames(limit)

    fun removeGameById(gameId: Long) = gameRepository.removeGameById(gameId)

    fun loadGameAndParty(gameId: Long) = gameRepository.loadGameAndParty(gameId)

    fun loadGame(id: Long) = gameRepository.loadGame(id)

    fun loadActivePlayer(gameId: Long, playerId: Long) = gameRepository.loadActivePlayer(gameId, playerId)

    fun loadActivePlayerAndGame(gameId: Long, playerId: Long) = gameRepository.loadGameAndActivePlayer(gameId, playerId)

    fun loadActivePlayerAndGame(gameId: Long) = gameRepository.loadGameAndActivePlayer(gameId)

    fun loadPlayersForGame(gameId: Long) = gameRepository.loadPlayersForGame(gameId)

    fun loadPlayerForGame(gameId: Long, playerId: Long) = gameRepository.loadPlayerForGame(gameId, playerId)

    fun updatePlayerInGame(player: PlayerInGameModel) = gameRepository.updatePlayerInGame(player)

    fun loadGameAndPlayers(gameId: Long): Maybe<PlayersAndGameModel> = gameRepository.loadGameAndPlayers(gameId)

    fun lvlUpPlayer(player: ActivePlayerModel): Completable {
        val inGamePlayer = player.playerInGame
        val newLevel = inGamePlayer.level.plus(1)
        inGamePlayer.level = newLevel

        val lvlAction = PlayerActionModel(playerId = inGamePlayer.playerId,
                gameId = inGamePlayer.gameId,
                amount = newLevel,
                valueType = ActionType.LVL,
                actionDate = Date())

        val powerAction = PlayerActionModel(playerId = inGamePlayer.playerId,
                gameId = inGamePlayer.gameId,
                amount = inGamePlayer.getTotalPower(),
                valueType = ActionType.POWER,
                actionDate = Date())

        return gameRepository.updatePlayerInGame(inGamePlayer, lvlAction, powerAction)
    }

    fun lvlUpPlayer(activePlayers: List<ActivePlayerModel>, playerPosition: Int): Completable {
        return activePlayers.elementAtOrNull(playerPosition)?.playerInGame?.let {
            val newLevel = it.level.plus(1)
            it.level = newLevel
            return gameRepository.updatePlayerInGame(it).andThen(saveBasicGameStats(activePlayers))
        } ?: Completable.error(IllegalStateException())
    }

    fun lvlUpPlayer(player: ActivePlayerModel, maxLevel: Int): Completable {
        val inGamePlayer = player.playerInGame
        val newLevel = inGamePlayer.level.plus(1)

        if (newLevel >= maxLevel) {
            return Completable.error(IllegalStateException())
        }

        inGamePlayer.level = newLevel

        val lvlAction = PlayerActionModel(playerId = inGamePlayer.playerId,
                gameId = inGamePlayer.gameId,
                amount = newLevel,
                valueType = ActionType.LVL,
                actionDate = Date())

        val powerAction = PlayerActionModel(playerId = inGamePlayer.playerId,
                gameId = inGamePlayer.gameId,
                amount = inGamePlayer.getTotalPower(),
                valueType = ActionType.POWER,
                actionDate = Date())

        return gameRepository.updatePlayerInGame(inGamePlayer, lvlAction, powerAction)
    }

    fun lvlUpMonster(monster: MonsterModel): Completable {
        val newLevel = monster.level.plus(1)
        monster.level = newLevel
        return gameRepository.updateMonsterInGame(monster)
    }

    fun lvlDwnPlayer(player: ActivePlayerModel): Completable {
        val inGamePlayer = player.playerInGame
        val newLevel = inGamePlayer.level.minus(1)
        if (newLevel < 1) {
            return Completable.complete()
        }
        inGamePlayer.level = newLevel

        val lvlAction = PlayerActionModel(playerId = inGamePlayer.playerId,
                gameId = inGamePlayer.gameId, amount = newLevel, valueType = ActionType.LVL, actionDate = Date())

        val powerAction = PlayerActionModel(playerId = inGamePlayer.playerId,
                gameId = inGamePlayer.gameId, amount = inGamePlayer.getTotalPower(), valueType = ActionType.POWER, actionDate = Date())

        return gameRepository.updatePlayerInGame(inGamePlayer, lvlAction, powerAction)
    }

    fun lvlDwnPlayer(activePlayers: List<ActivePlayerModel>, playerPosition: Int): Completable {
        return activePlayers.elementAtOrNull(playerPosition)?.playerInGame?.let {
            val newLevel = it.level.minus(1)
            if (newLevel < 1) {
                return Completable.complete()
            }
            it.level = newLevel

            return gameRepository.updatePlayerInGame(it).andThen(saveBasicGameStats(activePlayers))
        } ?: Completable.error(IllegalStateException())
    }

    fun lvlDwnMonster(monster: MonsterModel): Completable {
        val newLevel = monster.level.minus(1)
        if (newLevel < 1) {
            return Completable.complete()
        }
        monster.level = newLevel
        return gameRepository.updateMonsterInGame(monster)
    }

    fun gearUpPlayer(player: ActivePlayerModel, amount: Int): Completable {
        val inGamePlayer = player.playerInGame
        val newGear = inGamePlayer.gear.plus(amount)
        inGamePlayer.gear = newGear
        val action = PlayerActionModel(playerId = inGamePlayer.playerId,
                gameId = inGamePlayer.gameId, amount = newGear, valueType = ActionType.GEAR, actionDate = Date())

        val powerAction = PlayerActionModel(playerId = inGamePlayer.playerId,
                gameId = inGamePlayer.gameId,
                amount = inGamePlayer.getTotalPower(),
                valueType = ActionType.POWER,
                actionDate = Date())
        return gameRepository.updatePlayerInGame(inGamePlayer, action, powerAction)
    }

    fun gearUpPlayer(activePlayers: List<ActivePlayerModel>, playerPosition: Int, amount: Int): Completable {
        return activePlayers.elementAtOrNull(playerPosition)?.playerInGame?.let {
            val newGear = it.gear.plus(amount)
            it.gear = newGear

            return gameRepository.updatePlayerInGame(it).andThen(saveBasicGameStats(activePlayers))
        } ?: Completable.error(IllegalStateException())
    }

    fun gearDwnPlayer(player: ActivePlayerModel, amount: Int): Completable {
        val inGamePlayer = player.playerInGame
        val newGear = inGamePlayer.gear.minus(amount)
        inGamePlayer.gear = newGear

        val action = PlayerActionModel(playerId = inGamePlayer.playerId,
                gameId = inGamePlayer.gameId, amount = newGear, valueType = ActionType.GEAR, actionDate = Date())

        val powerAction = PlayerActionModel(playerId = inGamePlayer.playerId,
                gameId = inGamePlayer.gameId,
                amount = inGamePlayer.getTotalPower(),
                valueType = ActionType.POWER,
                actionDate = Date())
        return gameRepository.updatePlayerInGame(inGamePlayer, action, powerAction)
    }

    fun gearDwnPlayer(activePlayers: List<ActivePlayerModel>, playerPosition: Int, amount: Int): Completable {
        return activePlayers.elementAtOrNull(playerPosition)?.playerInGame?.let {
            val newGear = it.gear.minus(amount)
            it.gear = newGear

            return gameRepository.updatePlayerInGame(it).andThen(saveBasicGameStats(activePlayers))
        } ?: Completable.error(IllegalStateException())
    }

    fun modUpPlayer(player: ActivePlayerModel): Completable {
        val inGamePlayer = player.playerInGame
        val newMod = inGamePlayer.mods.plus(1)
        inGamePlayer.mods = newMod
        return gameRepository.updatePlayerInGame(inGamePlayer)
    }

    fun modUpMonster(monster: MonsterModel): Completable {
        val newMod = monster.mods.plus(1)
        monster.mods = newMod
        return gameRepository.updateMonsterInGame(monster)
    }

    fun modDwnPlayer(player: ActivePlayerModel): Completable {
        val inGamePlayer = player.playerInGame
        val newMod = inGamePlayer.mods.minus(1)
        inGamePlayer.mods = newMod
        return gameRepository.updatePlayerInGame(inGamePlayer)
    }

    fun modDwnMonster(monster: MonsterModel): Completable {
        val newMod = monster.mods.minus(1)
        monster.mods = newMod
        return gameRepository.updateMonsterInGame(monster)
    }

    fun modResetPlayer(player: ActivePlayerModel): Completable {
        player.playerInGame.mods = 0
        return gameRepository.updatePlayerInGame(player.playerInGame)
    }

    fun toggleWarrior(checked: Boolean, player: ActivePlayerModel): Completable {
        player.playerInGame.warrior = checked
        return gameRepository.updatePlayerInGame(player.playerInGame)
    }

    fun monsterKilled(player: ActivePlayerModel, monster: MonsterModel): Completable {
        val monsterAction = PlayerActionModel(playerId = player.player.id,
                gameId = player.playerInGame.gameId, amount = 1, valueType = ActionType.MONSTER, actionDate = Date())
        val treasuresAction = PlayerActionModel(playerId = player.player.id,
                gameId = player.playerInGame.gameId, amount = monster.treasures, valueType = ActionType.TREASURE, actionDate = Date())
        return gameRepository.createPlayerActions(monsterAction, treasuresAction)
    }

    fun runAway(player: ActivePlayerModel, isSuccess: Boolean): Completable {
        val actionType : ActionType = if (isSuccess) {
            ActionType.RUN_SUCCESS
        } else {
            ActionType.RUN_FAIL
        }
        val action = PlayerActionModel(playerId = player.player.id,
                gameId = player.playerInGame.gameId, amount = 1, valueType = actionType, actionDate = Date())
        return gameRepository.createPlayerAction(action)
    }

    fun toggleGender(player: ActivePlayerModel): Completable {
        val playerModel = player.player
        when (playerModel.gender) {
            Gender.MALE -> {
                playerModel.gender = Gender.FEMALE
            }
            Gender.FEMALE -> {
                playerModel.gender = Gender.MALE
            }
        }
        return playerRepository.update(playerModel)
    }

    fun changeColor(newColor: Int, player:ActivePlayerModel): Completable {
        val playerModel = player.player
        playerModel.color = newColor
        return playerRepository.update(playerModel)
    }

    fun markTheWinner(player: ActivePlayerModel): Completable {
        player.playerInGame.winner = true
        return gameRepository.updatePlayerInGame(player.playerInGame)
    }

    fun unmarkTheWinner(players: List<ActivePlayerModel>): Completable {
        var changedPlayer: ActivePlayerModel? = null
        players.forEach {
            if (it.playerInGame.winner) {
                it.playerInGame.winner = false
                changedPlayer = it
            }
        }
        return changedPlayer?.let { player ->
            gameRepository.updatePlayerInGame(player.playerInGame) } ?: Completable.complete()
    }

    fun setGameMaxLevel(game: GameModel, newMaxLevel: Int): Completable {
        game.maxLevel = newMaxLevel
        return gameRepository.updateTheGame(game)
    }

    fun savePlayersOrder(players: List<ActivePlayerModel>): Completable {
        return playerRepository.updateSelectionPlayers(players.map { it.player })
    }

    fun finishTheGame(winner: ActivePlayerModel, gameId: Long): Completable = markTheWinner(winner)
                .andThen(
                        gameRepository.finishTheGame(gameId)
                )

    fun finishTheGame(gameId: Long): Completable = gameRepository.finishTheGame(gameId)

    fun replayTheGame(oldGame: PlayersAndGameModel): Maybe<Long> {

        if (oldGame.players.size == 1) {
            return gameRepository.createSingleGame(
                    GameModel(playerId = oldGame.players[0].player.id,
                            maxLevel = oldGame.gameModel.maxLevel,
                            startDate = Date()))
        }
        return gameRepository.createPartyGame(
                GameModel(partyId = oldGame.gameModel.partyId,
                        maxLevel = oldGame.gameModel.maxLevel,
                        startDate = Date())
        )
    }

    fun treasuresUp(monster: MonsterModel): Completable {
        val newTreasures = monster.treasures.plus(1)
        monster.treasures = newTreasures
        return gameRepository.updateMonsterInGame(monster)
    }

    fun treasuresDwn(monster: MonsterModel): Completable {
        val newTreasures = monster.treasures.minus(1)
        if (newTreasures < 1) {
            return Completable.complete()
        }
        monster.treasures = newTreasures
        return gameRepository.updateMonsterInGame(monster)
    }

    private fun saveBasicGameStats(activePlayers: List<ActivePlayerModel>): Completable {
        val actionsToSave = ArrayList<PlayerActionModel>()
        val actionDate = Date()

        activePlayers.forEach { player ->
            actionsToSave.add(PlayerActionModel(playerId = player.playerInGame.playerId,
                    gameId = player.playerInGame.gameId,
                    amount = player.playerInGame.level,
                    valueType = ActionType.LVL,
                    actionDate = actionDate))
            actionsToSave.add(PlayerActionModel(playerId = player.playerInGame.playerId,
                    gameId = player.playerInGame.gameId,
                    amount = player.playerInGame.gear,
                    valueType = ActionType.GEAR,
                    actionDate = actionDate))
            actionsToSave.add(PlayerActionModel(playerId = player.playerInGame.playerId,
                    gameId = player.playerInGame.gameId,
                    amount = player.playerInGame.getTotalPower(),
                    valueType = ActionType.POWER,
                    actionDate = actionDate))
        }

        return gameRepository.createPlayerActions(actionsToSave)
    }

    fun getLastUnfinishedGame(): Maybe<GameModel> =
        gameRepository.lastGame()

}