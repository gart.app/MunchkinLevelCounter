package com.g_art.munchkinlevelcounter.usecases

import com.g_art.munchkinlevelcounter.model.PartyAndPlayersModel
import com.g_art.munchkinlevelcounter.model.PartyModel
import com.g_art.munchkinlevelcounter.model.PlayerModel
import com.g_art.munchkinlevelcounter.repository.PartyRepository
import com.g_art.munchkinlevelcounter.repository.PlayerRepository
import io.reactivex.Completable
import io.reactivex.Maybe

class PartyDetailsUseCase(private val partyRepository: PartyRepository,
                          private val playerRepository: PlayerRepository) {

    fun loadPartyById(id: Long): Maybe<PartyModel> = partyRepository.findPartyById(id)

    fun loadPartyAndPlayersById(id: Long): Maybe<PartyAndPlayersModel> = partyRepository.findPartyAndPlayersById(id)

    fun insertParty(partyModel: PartyModel) = validate(partyModel).andThen(partyRepository.insertOrUpdate(partyModel))

    fun insertOrUpdate(partyAndPlayersModel: PartyAndPlayersModel) = validate(partyAndPlayersModel)
            .andThen(partyRepository.insertOrUpdate(partyAndPlayersModel.partyModel))

    fun insertParty(partyAndPlayersModel: PartyAndPlayersModel): Completable = validate(partyAndPlayersModel)
            .andThen(partyRepository.insert(partyAndPlayersModel.partyModel))

    fun updateParty(partyAndPlayersModel: PartyAndPlayersModel): Completable = validate(partyAndPlayersModel)
            .andThen(partyRepository.update(partyAndPlayersModel.partyModel))

    fun addPlayersToParty(party: PartyModel, players: List<PlayerModel>) = validate(party).andThen(partyRepository.editPlayersInTheParty(party, players))

    fun removePlayerFromParty(partyId: Long, playerId: Long) = partyRepository.removePlayerFromParty(partyId, playerId)

    fun removePartyById(id: Long) = partyRepository.delete(id)

    fun savePlayerOrder(playerMode: PlayerModel) = playerRepository.update(playerMode)

    fun savePlayersOrder(players: List<PlayerModel>) = playerRepository.update(players)

    private fun validate(party: PartyModel): Completable {
        return if (!party.isValidForAdd()) {
            Completable.error(IllegalArgumentException("Party failed validation before add"))
        } else {
            Completable.complete()
        }
    }

    private fun validate(partyAndPlayersModel: PartyAndPlayersModel): Completable {
        return if (!partyAndPlayersModel.partyModel.isValidForAdd()) {
            Completable.error(IllegalArgumentException("Party failed validation before add"))
        } else if (partyAndPlayersModel.players.size < 2) {
            Completable.error(IllegalStateException("Not enough players in party"))
        } else {
            Completable.complete()
        }
    }
}