package com.g_art.munchkinlevelcounter.usecases

import com.g_art.munchkinlevelcounter.model.PlayerModel
import com.g_art.munchkinlevelcounter.repository.PlayerRepository
import io.reactivex.Completable
import io.reactivex.Maybe

/**
 * Created by agulia on 3/29/18.
 */
class PlayerDetailsUseCase(private val repository: PlayerRepository) {

    fun loadPlayerById(id: Long): Maybe<PlayerModel> = repository.findPlayerById(id)

    fun insertPlayer(playerModel: PlayerModel) = validate(playerModel).andThen(repository.insert(playerModel))

    fun updatePlayer(playerModel: PlayerModel) = validate(playerModel).andThen(repository.update(playerModel))

    fun deletePlayer(id: Long) = repository.delete(id)

    private fun validate(player: PlayerModel): Completable {
        return if (!player.isValidForAdd()) {
            Completable.error(IllegalArgumentException("Player failed validation before add"))
        } else {
            Completable.complete()
        }
    }
}