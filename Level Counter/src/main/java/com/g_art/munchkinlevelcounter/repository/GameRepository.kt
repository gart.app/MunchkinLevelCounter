package com.g_art.munchkinlevelcounter.repository

import com.g_art.munchkinlevelcounter.model.*
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

interface GameRepository {

    fun loadPlayedGames(): Single<List<GameModel>>

    fun loadPlayedGames(limit: Int): Single<List<GameModel>>

    fun removeGameById(gameId: Long): Completable

    fun loadGame(id: Long): Maybe<GameModel>

    fun loadGameAndParty(gameId: Long): Maybe<ActivePartyAndGameModel>

    fun loadActivePlayer(gameId: Long, playerId: Long): Maybe<ActivePlayerModel>

    fun loadGameAndActivePlayer(gameId: Long, playerId: Long): Maybe<ActivePlayerAndGameModel>

    fun loadGameAndActivePlayer(gameId: Long): Maybe<ActivePlayerAndGameModel>

    fun createSingleGame(game: GameModel): Maybe<Long>

    fun createPartyGame(game: GameModel): Maybe<Long>

    fun createPartyGame(game: GameModel, players: List<ActivePlayerModel>): Maybe<Long>

    fun updateTheGame(game: GameModel): Completable

    fun loadPlayerForGame(gameId: Long, playerId: Long): Maybe<PlayerInGameModel>

    fun loadPlayersForGame(gameId: Long): Single<List<PlayerInGameModel>>

    fun loadGameAndPlayers(gameId: Long): Maybe<PlayersAndGameModel>

    fun updatePlayerInGame(player: PlayerInGameModel, vararg actions: PlayerActionModel): Completable

    fun updateMonsterInGame(monster: MonsterModel): Completable

    fun updatePlayerInGame(player: PlayerInGameModel): Completable

    fun createPlayerAction(action: PlayerActionModel): Completable

    fun createPlayerActions(vararg action: PlayerActionModel): Completable

    fun createPlayerActions(actions: List<PlayerActionModel>): Completable

    fun editPlayersInTheGame(gameId: Long, partyId: Long,
                             playersToAdd: List<PlayerInGameModel>,
                             playersToRemove: List<PlayerInGameModel>): Completable

    fun finishTheGame(gameId: Long): Completable

    fun lastGame(): Maybe<GameModel>
}