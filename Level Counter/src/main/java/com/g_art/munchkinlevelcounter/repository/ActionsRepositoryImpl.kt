package com.g_art.munchkinlevelcounter.repository

import com.g_art.munchkinlevelcounter.dao.PlayerActionDao
import com.g_art.munchkinlevelcounter.entity.*
import com.g_art.munchkinlevelcounter.mapper.EntityModelMapper
import com.g_art.munchkinlevelcounter.model.*
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers

class ActionsRepositoryImpl(private val actionsDao: PlayerActionDao,
                            private val playerInGameMapper: EntityModelMapper<PlayerInGame, PlayerInGameModel>,
                            private val playerActionModelMapper: EntityModelMapper<PlayerAction, PlayerActionModel>,
                            private val playerMapper: EntityModelMapper<Player, PlayerModel>) : ActionsRepository {

    override fun getGameActions(gameId: Long): Single<List<PlayerInGameActionsModel>> =
            Single.zip(
                    actionsDao.getPlayerInGame(gameId).subscribeOn(Schedulers.io()),
                    actionsDao.getGameActions(gameId).subscribeOn(Schedulers.io()),

                    BiFunction<List<PlayerInGameAction>, List<PlayerAction>, List<PlayerInGameActionsModel>>
                    { players, actions ->

                        val actionsMap: Map<Long, List<PlayerAction>> = actions
                                .groupBy { action -> action.playerId }

                        players.forEach {
                            if (actionsMap.containsKey(it.player!!.id)) {
                                it.playerActions = actionsMap[it.player!!.id]!!
                            }
                        }

                        return@BiFunction actionsPerPlayers(players)
                    }
            ).subscribeOn(Schedulers.computation())

    override fun getGameActionsForPlayer(gameId: Long, playerId: Long): Single<List<PlayerInGameActionsModel>> =
            actionsDao.getGameActionsForPlayer(gameId, playerId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.computation())
                    .map { playerActions ->
                        actionsPerPlayers(playerActions)
                    }

    private fun actionsPerPlayers(playerActions: List<PlayerInGameAction>): List<PlayerInGameActionsModel> {
        return playerActions.map {
            val actionsMap: Map<ActionType, List<PlayerActionModel>> = it.playerActions
                    .map { action -> playerActionModelMapper.fromEntity(action) }
                    .groupBy { action -> action.valueType }

            PlayerInGameActionsModel(playerMapper.fromEntity(it.player!!),
                    playerInGameMapper.fromEntity(it.playerInGame!!),
                    actionsMap)
        }
    }
}