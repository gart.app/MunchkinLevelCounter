package com.g_art.munchkinlevelcounter.repository

import com.g_art.munchkinlevelcounter.dao.PartyDao
import com.g_art.munchkinlevelcounter.dao.PlayerDao
import com.g_art.munchkinlevelcounter.entity.Party
import com.g_art.munchkinlevelcounter.entity.PartyAndPlayersAmount
import com.g_art.munchkinlevelcounter.entity.Player
import com.g_art.munchkinlevelcounter.entity.PlayerInParty
import com.g_art.munchkinlevelcounter.mapper.EntityModelMapper
import com.g_art.munchkinlevelcounter.model.PartyAndPlayersModel
import com.g_art.munchkinlevelcounter.model.PartyModel
import com.g_art.munchkinlevelcounter.model.PlayerInPartyModel
import com.g_art.munchkinlevelcounter.model.PlayerModel
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers

/**
 * Created by agulia on 3/20/18.
 */
class PartyRepositoryImpl(private val partyDao: PartyDao,
                          private val playerDao: PlayerDao,
                          private val partyMapper: EntityModelMapper<Party, PartyModel>,
                          private val partyAndPlayersMapper: EntityModelMapper<PartyAndPlayersAmount, PartyModel>,
                          private val playerMapper: EntityModelMapper<Player, PlayerModel>,
                          private val partyPlayerMapper: EntityModelMapper<PlayerInParty, PlayerInPartyModel>) : PartyRepository {

    override fun insertOrUpdate(party: PartyModel): Maybe<Long> =
            Maybe.fromCallable {
                partyDao.insertParty(partyMapper.toEntity(party))
            }.subscribeOn(Schedulers.io())

    override fun insert(party: PartyModel): Completable =
            Completable.fromCallable {
                partyDao.insertParty(partyMapper.toEntity(party))
            }.subscribeOn(Schedulers.io())

    override fun update(party: PartyModel): Completable =
        Completable.fromCallable {
            partyDao.updateParty(partyMapper.toEntity(party))
        }.subscribeOn(Schedulers.io())


    override fun createPartyAndAddPlayers(party: PartyModel, players: List<PlayerModel>): Maybe<Long> =
        Maybe.fromCallable {
            partyDao.insertParty(partyMapper.toEntity(party))
                .also { partyId ->
                    val playersInParty = mutableListOf<PlayerInPartyModel>()
                    players.forEach {player ->
                        playersInParty.add(PlayerInPartyModel(player.id, partyId))
                    }
                    playerDao.addPlayersToParty(playersInParty.map { partyPlayerMapper.toEntity(it) })
                }
        }
    .subscribeOn(Schedulers.io())

    override fun editPlayersInTheParty(party: PartyModel, players: List<PlayerModel>): Completable = Completable.fromAction {
        partyDao.insertParty(partyMapper.toEntity(party))
                .also { partyId ->
                    val playersInParty = mutableListOf<PlayerInPartyModel>()
                    players.forEach { player ->
                        playersInParty.add(PlayerInPartyModel(player.id, partyId))
                    }
                    playerDao.addPlayersToParty(playersInParty.map { partyPlayerMapper.toEntity(it) })
                }
    }.subscribeOn(Schedulers.io())

    override fun editPlayersInTheParty(playersToAdd: List<PlayerInPartyModel>,
                                       playersToRemove: List<PlayerInPartyModel>): Completable = Completable.fromAction {
        partyDao.editPartyWithPlayers(
                playersToAdd.map { partyPlayerMapper.toEntity(it) },
                playersToRemove.map { partyPlayerMapper.toEntity(it) }
        )
    }.subscribeOn(Schedulers.io())

    override fun removePlayerFromParty(partyId: Long, playerId: Long): Completable =
            Completable.fromAction {
                partyDao.removePlayerFromParty(partyId, playerId)
            }.subscribeOn(Schedulers.io())

    override fun delete(party: PartyModel): Completable = Completable.fromAction {
        partyDao.removeParty(partyMapper.toEntity(party))
    }.subscribeOn(Schedulers.io())

    override fun delete(id: Long) = Completable.fromAction {
        partyDao.removePartyById(id)
    }.subscribeOn(Schedulers.io())

    override fun findPartyById(id: Long): Maybe<PartyModel> {
        return partyDao.getPartyById(id)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .map { partyMapper.fromEntity(it) }
    }

    override fun findPartyAndPlayersById(id: Long): Maybe<PartyAndPlayersModel> {
        return Maybe.zip(
                findPartyById(id).subscribeOn(Schedulers.io()),
                findPlayersByPartyId(id).subscribeOn(Schedulers.io()),

                BiFunction<PartyModel, List<PlayerModel>, PartyAndPlayersModel>
                { party, players ->
                    val sorted = ArrayList(players.sortedBy { playerModel -> playerModel.position })
                    PartyAndPlayersModel(party, sorted)
                }
        ).subscribeOn(Schedulers.computation())
    }

    override fun findPlayersByPartyId(id: Long): Maybe<List<PlayerModel>> {
        return playerDao.getPlayersByPartyId(id)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .map { it.map(playerMapper::fromEntity) }
    }

    override fun getAllParties(): Single<List<PartyModel>> {
        return partyDao.getPartiesAndPlayersCount()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .map { it.map(partyAndPlayersMapper::fromEntity) }
    }

    override fun getAllParties(limit: Int): Single<List<PartyModel>> {
        return partyDao.getPartiesAndPlayersCount(limit)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .map { it.map(partyAndPlayersMapper::fromEntity) }
    }
}