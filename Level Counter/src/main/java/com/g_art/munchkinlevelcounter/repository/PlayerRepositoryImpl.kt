package com.g_art.munchkinlevelcounter.repository

import com.g_art.munchkinlevelcounter.dao.PlayerDao
import com.g_art.munchkinlevelcounter.entity.Player
import com.g_art.munchkinlevelcounter.mapper.EntityModelMapper
import com.g_art.munchkinlevelcounter.model.PlayerModel
import com.g_art.munchkinlevelcounter.model.SelectionPlayerModel
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers

class PlayerRepositoryImpl(private val playerDao: PlayerDao,
                           private val mapper: EntityModelMapper<Player, PlayerModel>,
                           private val selectionMapper: EntityModelMapper<Player, SelectionPlayerModel>) : PlayerRepository {

    override fun insert(player: PlayerModel) = Completable
            .fromCallable {
                playerDao.insert(mapper.toEntity(player))
            }
            .subscribeOn(Schedulers.io())

    override fun update(player: PlayerModel) =
            Completable.fromCallable {
                playerDao.update(mapper.toEntity(player))
            }
                    .subscribeOn(Schedulers.io())

    override fun update(players: List<PlayerModel>): Completable =
            Completable.fromCallable {
                playerDao.updateAll(players.map { mapper.toEntity(it) })
            }.subscribeOn(Schedulers.io())

    override fun updateSelectionPlayers(players: List<SelectionPlayerModel>): Completable =
            Completable.fromAction {
                playerDao.updatePlayersPositions(players)
            }.subscribeOn(Schedulers.io())

    override fun delete(id: Long) = Completable.fromAction { playerDao.deletePlayer(id) }
            .subscribeOn(Schedulers.io())

    override fun findPlayersByPartyId(id: Long): Maybe<List<SelectionPlayerModel>> {
        return Maybe.zip(
                playerDao.getAllPlayersMaybe().subscribeOn(Schedulers.io()),
                playerDao.getPlayersByPartyId(id).subscribeOn(Schedulers.io()),

                BiFunction<List<Player>,
                        List<Player>,
                        List<SelectionPlayerModel>> { allPlayers, selectedPlayers ->
                    val playerModels = allPlayers.map(selectionMapper::fromEntity)
                    playerModels.forEach {
                        selectedPlayers.forEach { selectedPlayer ->
                            if (selectedPlayer.id == it.id) {
                                it.selected = true
                            }
                        }
                    }
                    return@BiFunction playerModels
                }).subscribeOn(Schedulers.computation())
    }

    override fun findPlayersByGameId(id: Long): Maybe<List<SelectionPlayerModel>> {
        return Maybe.zip(
                playerDao.getAllPlayersMaybe().subscribeOn(Schedulers.io()),
                playerDao.getPlayersByGameId(id).subscribeOn(Schedulers.io()),

                BiFunction<List<Player>,
                        List<Player>,
                        List<SelectionPlayerModel>> { allPlayers, selectedPlayers ->
                    val playerModels = allPlayers.map(selectionMapper::fromEntity)
                    playerModels.forEach {
                        selectedPlayers.forEach { selectedPlayer ->
                            if (selectedPlayer.id == it.id) {
                                it.selected = true
                            }
                        }
                    }
                    return@BiFunction playerModels
                }).subscribeOn(Schedulers.computation())
    }

    override fun findPlayerById(id: Long): Maybe<PlayerModel> {
        return playerDao.getPlayerById(id)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .map { mapper.fromEntity(it) }
    }

    override fun getAllPlayers(): Single<List<SelectionPlayerModel>> {
        return playerDao.getAllPlayers()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .map { it.map(selectionMapper::fromEntity) }
    }
}
