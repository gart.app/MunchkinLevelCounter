package com.g_art.munchkinlevelcounter.repository

import com.g_art.munchkinlevelcounter.model.PlayerModel
import com.g_art.munchkinlevelcounter.model.SelectionPlayerModel
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

/**
 * Created by agulia on 3/19/18.
 */
interface PlayerRepository {
    fun insert(player: PlayerModel): Completable

    fun update(player: PlayerModel): Completable

    fun update(players: List<PlayerModel>): Completable

    fun updateSelectionPlayers(players: List<SelectionPlayerModel>): Completable

    fun delete(id: Long): Completable

    fun findPlayersByPartyId(id: Long): Maybe<List<SelectionPlayerModel>>

    fun findPlayersByGameId(id: Long): Maybe<List<SelectionPlayerModel>>

    fun findPlayerById(id: Long): Maybe<PlayerModel>

    fun getAllPlayers(): Single<List<SelectionPlayerModel>>
}