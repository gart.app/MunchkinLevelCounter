package com.g_art.munchkinlevelcounter.repository

import com.g_art.munchkinlevelcounter.model.PartyAndPlayersModel
import com.g_art.munchkinlevelcounter.model.PartyModel
import com.g_art.munchkinlevelcounter.model.PlayerInPartyModel
import com.g_art.munchkinlevelcounter.model.PlayerModel
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

/**
 * Created by agulia on 3/20/18.
 */
interface PartyRepository {
    fun insertOrUpdate(party: PartyModel): Maybe<Long>

    fun insert(party: PartyModel): Completable

    fun update(party: PartyModel): Completable

    fun editPlayersInTheParty(party: PartyModel, players: List<PlayerModel>): Completable

    fun createPartyAndAddPlayers(party: PartyModel, players: List<PlayerModel>): Maybe<Long>

    fun editPlayersInTheParty(playersToAdd: List<PlayerInPartyModel>, playersToRemove: List<PlayerInPartyModel>): Completable

    fun removePlayerFromParty(partyId: Long, playerId: Long): Completable

    fun delete(party: PartyModel): Completable

    fun delete(id: Long): Completable

    fun findPartyById(id: Long): Maybe<PartyModel>

    fun findPartyAndPlayersById(id: Long): Maybe<PartyAndPlayersModel>

    fun findPlayersByPartyId(id: Long): Maybe<List<PlayerModel>>

    fun getAllParties(): Single<List<PartyModel>>
    fun getAllParties(limit: Int): Single<List<PartyModel>>
}
