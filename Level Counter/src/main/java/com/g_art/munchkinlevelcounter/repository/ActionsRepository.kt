package com.g_art.munchkinlevelcounter.repository

import com.g_art.munchkinlevelcounter.model.PlayerInGameActionsModel
import io.reactivex.Single

interface ActionsRepository {

    fun getGameActions(gameId: Long): Single<List<PlayerInGameActionsModel>>

    fun getGameActionsForPlayer(gameId: Long, playerId: Long): Single<List<PlayerInGameActionsModel>>
}