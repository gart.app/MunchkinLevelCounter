package com.g_art.munchkinlevelcounter.repository

import com.g_art.munchkinlevelcounter.dao.GameDao
import com.g_art.munchkinlevelcounter.dao.PlayerActionDao
import com.g_art.munchkinlevelcounter.dao.PlayerDao
import com.g_art.munchkinlevelcounter.entity.*
import com.g_art.munchkinlevelcounter.mapper.EntityModelMapper
import com.g_art.munchkinlevelcounter.model.*
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import java.util.*

class GameRepositoryImpl(private val gameDao: GameDao,
                         private val playerDao: PlayerDao,
                         private val playerActionDao: PlayerActionDao,
                         private val gameMapper: EntityModelMapper<Game, GameModel>,
                         private val playerInGameMapper: EntityModelMapper<PlayerInGame, PlayerInGameModel>,
                         private val monsterMapper: EntityModelMapper<Monster, MonsterModel>,
                         private val activePlayerModelMapper: EntityModelMapper<ActivePlayer, ActivePlayerModel>,
                         private val playerActionModelMapper: EntityModelMapper<PlayerAction, PlayerActionModel>,
                         private val activePlayerAndGameModelMapper: EntityModelMapper<ActivePlayerAndGame, ActivePlayerAndGameModel>,
                         private val activePartyAndGameModelMapper: EntityModelMapper<ActivePartyAndGame, ActivePartyAndGameModel>) :
        GameRepository {

    override fun lastGame(): Maybe<GameModel> =
        gameDao.getLastGame()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .map (gameMapper::fromEntity)

    override fun loadPlayedGames(): Single<List<GameModel>> =
            gameDao.getPlayedGames()
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.computation())
                    .map {
                        it.asSequence().sortedByDescending { game -> game.id }
                                .map(gameMapper::fromEntity).toList()
                    }

    override fun loadPlayedGames(limit: Int): Single<List<GameModel>> =
        gameDao.getPlayedGames(limit)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .map {
                    it.asSequence().sortedByDescending { game -> game.id }
                            .map(gameMapper::fromEntity).toList()
                }

    override fun removeGameById(gameId: Long): Completable =
            Completable.fromAction {
                gameDao.removeGameAndActionsById(gameId)
            }.subscribeOn(Schedulers.io())

    override fun loadGameAndParty(gameId: Long): Maybe<ActivePartyAndGameModel> {
        return Maybe.zip(
                gameDao.getGameById(gameId).subscribeOn(Schedulers.io()),
                gameDao.getActivePlayers(gameId).subscribeOn(Schedulers.io()),

                BiFunction<Game, List<ActivePlayer>, ActivePartyAndGameModel>
                { game, activePlayers ->
                    val sortedPlayers = ArrayList(activePlayers.sortedBy { playerModel -> playerModel.player.position })
                    activePartyAndGameModelMapper.fromEntity(ActivePartyAndGame(game, sortedPlayers))
                }
        ).subscribeOn(Schedulers.computation())
    }

    override fun loadGame(id: Long): Maybe<GameModel> = Maybe.fromAction {
        gameDao.getGameById(id)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .map { gameMapper.fromEntity(it) }
    }

    override fun loadActivePlayer(gameId: Long, playerId: Long): Maybe<ActivePlayerModel> =
            gameDao.getActivePlayer(gameId, playerId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.computation())
                    .map { activePlayerModelMapper.fromEntity(it) }

    override fun loadGameAndActivePlayer(gameId: Long, playerId: Long): Maybe<ActivePlayerAndGameModel> {
        return Maybe.zip(
                gameDao.getGameById(gameId).subscribeOn(Schedulers.io()),
                gameDao.getActivePlayer(gameId, playerId).subscribeOn(Schedulers.io()),

                BiFunction<Game, ActivePlayer, ActivePlayerAndGameModel>
                { game, activePlayer ->
                    activePlayerAndGameModelMapper.fromEntity(ActivePlayerAndGame(game, activePlayer))
                }

        ).subscribeOn(Schedulers.computation())
    }

    override fun loadGameAndActivePlayer(gameId: Long): Maybe<ActivePlayerAndGameModel> {
        return Maybe.zip(
                gameDao.getGameById(gameId).subscribeOn(Schedulers.io()),
                gameDao.getActivePlayer(gameId).subscribeOn(Schedulers.io()),

                BiFunction<Game, ActivePlayer, ActivePlayerAndGameModel>
                { game, activePlayer ->
                    activePlayerAndGameModelMapper.fromEntity(ActivePlayerAndGame(game, activePlayer))
                }

        ).subscribeOn(Schedulers.computation())
    }

    override fun loadPlayerForGame(gameId: Long, playerId: Long): Maybe<PlayerInGameModel> =
            gameDao.getPlayerForGame(gameId, playerId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.computation())
                    .map { playerInGameMapper.fromEntity(it) }

    override fun createSingleGame(game: GameModel): Maybe<Long> =
            Maybe.fromCallable { gameDao.createSingleGame(gameMapper.toEntity(game)) }
                    .subscribeOn(Schedulers.io())

    override fun createPartyGame(game: GameModel): Maybe<Long> {
        return Maybe.zip(
                Maybe.fromCallable { gameDao.insertWithResponse(gameMapper.toEntity(game)) }.subscribeOn(Schedulers.io()),
                playerDao.getPlayersByPartyId(game.partyId).subscribeOn(Schedulers.io()),

                BiFunction<Long, List<Player>, Long>
                { gameId, players ->

                    gameDao.insertPlayers(
                            players.map { PlayerInGame(gameId = gameId, playerId = it.id) }
                    )

                    return@BiFunction gameId
                }
        ).subscribeOn(Schedulers.io())
    }

    override fun createPartyGame(game: GameModel, players: List<ActivePlayerModel>): Maybe<Long> {
        return Maybe.fromAction<Long> {
            val gameId = gameDao.insertWithResponse(gameMapper.toEntity(game))
            gameDao.insertPlayers(
                    players.map { PlayerInGame(gameId = gameId, playerId = it.player.id) }
            )
        }.subscribeOn(Schedulers.io())
    }

    override fun updateTheGame(game: GameModel): Completable =
            Completable.fromAction {
                gameDao.update(gameMapper.toEntity(game))
            }.subscribeOn(Schedulers.io())

    override fun loadPlayersForGame(gameId: Long): Single<List<PlayerInGameModel>> =
            gameDao.getPlayersForGame(gameId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.computation())
                    .map { it.map(playerInGameMapper::fromEntity) }

    override fun loadGameAndPlayers(gameId: Long): Maybe<PlayersAndGameModel> =
            Maybe.zip(
                    gameDao.getGameById(gameId).subscribeOn(Schedulers.io()),
                    gameDao.getActivePlayers(gameId).subscribeOn(Schedulers.io()),

                    BiFunction<Game, List<ActivePlayer>, PlayersAndGameModel> { game, players ->

                        return@BiFunction PlayersAndGameModel(gameMapper.fromEntity(game),
                                players.asSequence()
                                        .sortedByDescending { it.playerInGame.level }
                                        .map { activePlayerModelMapper.fromEntity(it) }
                                        .toList()
                        )
                    }

            ).observeOn(Schedulers.computation())

    override fun updatePlayerInGame(player: PlayerInGameModel, vararg actions: PlayerActionModel): Completable =
            Completable.fromAction {
                gameDao.createActionAndUpdatePlayerInGame(playerInGameMapper.toEntity(player),
                        actions.map { playerActionModelMapper.toEntity(it) }
                )
            }.subscribeOn(Schedulers.io())

    override fun updatePlayerInGame(player: PlayerInGameModel): Completable =
            Completable.fromAction {
                gameDao.updatePlayerInGame(playerInGameMapper.toEntity(player))
            }.subscribeOn(Schedulers.io())

    override fun createPlayerAction(action: PlayerActionModel): Completable =
            Completable.fromAction {
                playerActionDao.insert()
            }.subscribeOn(Schedulers.io())

    override fun createPlayerActions(vararg action: PlayerActionModel): Completable =
            Completable.fromAction {
                playerActionDao.insert(action.map { playerActionModelMapper.toEntity(it) })
            }.subscribeOn(Schedulers.io())

    override fun createPlayerActions(actions: List<PlayerActionModel>): Completable =
        Completable.fromAction {
            playerActionDao.insert(actions.map { playerActionModelMapper.toEntity(it) })
        }.subscribeOn(Schedulers.io())

    override fun editPlayersInTheGame(gameId: Long, partyId: Long,
                                      playersToAdd: List<PlayerInGameModel>,
                                      playersToRemove: List<PlayerInGameModel>): Completable = Completable.fromAction {
        if (partyId != -1L) {
            gameDao.updateGameWithParty(gameId, partyId)
        } else {
            gameDao.updateGameWithParty(gameId, -1)
        }
        gameDao.editPlayers(
                playersToAdd.map { playerInGameMapper.toEntity(it) },
                playersToRemove.map { playerInGameMapper.toEntity(it) }
        )
    }.subscribeOn(Schedulers.io())

    override fun finishTheGame(gameId: Long): Completable =
            Completable.fromAction { gameDao.finishTheGame(gameId, Date()) }.subscribeOn(Schedulers.io())

    override fun updateMonsterInGame(monster: MonsterModel): Completable =
            Completable.fromAction {
                gameDao.updateMonsterInGame(monsterMapper.toEntity(monster))
            }.subscribeOn(Schedulers.io())
}